const recurringMeetingRepository = require('../respository/recurringMeetingsRepository');
const timeZoneDetailsRepo = require('../respository/timeZoneDetailsRepository');
const meetingRepository = require('../respository/MeetingRepository');
const meetingController = require('../controllers/MeetingController');
const WEEK_DAYS = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

/*******************************************************
 * Following method is called to create or upate an RM
 *******************************************************/

  module.exports.saveRecurringMeetingData = async (instance, changeType, userId, graph, query) => {

    /*************************************************************************************
     * Start of calling Graph APIs to get resource and subscription id to save in rm Table
     ************************************************************************************/

    let userDetails = await graph.api('/Users');
    let resource = 'Users/' + userDetails.id + '/Events/';

    // let subscriptionDetails = await graph.api('/subscriptions');
    // let subscriptionId = subscriptionDetails.find(x => x['notificationUrl'].includes('dev')).id;

    resource = resource + instance.seriesMasterId;
    let calendarResource = await graph(resource);
    
    /************************************************************************************
     * End of calling Graph APIs to get resource and subscription id to save in rm Table
     ************************************************************************************/

    let recurringMeetingDetail = {};
    let meetingEndTIme = calendarResource['end']['dateTime'].slice(10);
    let calendarResourceRecurrence = calendarResource['recurrence'];
    let currentDate = new Date();

    let rmCreatedDate = new Date(calendarResource.createdDateTime);
    let isMeetingForPreviousDay = false;

    /**
     * Preparing the object to be saved in RM table ( whether on creation or updation )
     */
    recurringMeetingDetail['subject'] = calendarResource['subject'];
    recurringMeetingDetail['is_all_day'] = calendarResource['isAllDay'] ? 'Y' : 'N';
    recurringMeetingDetail['start_date'] = new Date(calendarResource['start']['dateTime']);
    recurringMeetingDetail['end_date'] = new Date(calendarResourceRecurrence['range']['endDate'] + meetingEndTIme);
    recurringMeetingDetail['repeat_every'] = calendarResourceRecurrence['pattern']['interval'];
    if (calendarResourceRecurrence['pattern']['type'].includes("Yearly")) {
      if (calendarResourceRecurrence['range']['endDate'] === "0001-01-01") {
        recurringMeetingDetail['end_date'].setFullYear(recurringMeetingDetail.start_date.getFullYear() + 10);
        recurringMeetingDetail['end_date'].setMonth(recurringMeetingDetail.start_date.getMonth());
        recurringMeetingDetail['end_date'].setDate(recurringMeetingDetail.start_date.getDate() + 2);
      }
    }
    recurringMeetingDetail['frequency_type'] = calendarResourceRecurrence['pattern']['type'];
    recurringMeetingDetail['day_index'] = calendarResourceRecurrence['pattern']['index'];
    if (calendarResourceRecurrence['pattern']['daysOfWeek'] !== undefined)
      recurringMeetingDetail['weekdays_selected'] = calendarResourceRecurrence['pattern']['daysOfWeek'].toString();
    else
       recurringMeetingDetail['weekdays_selected'] = null;
    recurringMeetingDetail['organizer'] = calendarResource['organizer']['emailAddress'].address;
    recurringMeetingDetail['TimeZoneName'] = calendarResource['originalStartTimeZone'];
    recurringMeetingDetail['TimeZoneOffset'] = await getTimeZoneOffsetValue(calendarResource['originalStartTimeZone']);
    recurringMeetingDetail['countable'] = 1;
    

     //opt-out functionality starts here
     let incomingInviteeDetailed = [];
     let meetingInvites = [];
     for (let j = 0; j < calendarResource.attendees.length; j++) {
       if ((calendarResource.attendees[j].status.response != "declined") && (calendarResource.attendees[j].emailAddress.address != calendarResource.organizer.emailAddress.address)) {
         meetingInvites.push(calendarResource.attendees[j].emailAddress.address)
         incomingInviteeDetailed.push({ firstname: calendarResource.attendees[j].emailAddress.name, email: calendarResource.attendees[j].emailAddress.address });
       }
     }

    let optOutVal = [];
    if (incomingInviteeDetailed) {
        optOutVal = incomingInviteeDetailed.filter(function (listItem) {
            return listItem.email == 'opt-out@sustainably.run';
        });
    }
    
    if(calendarResource['bodyPreview'].includes("<---")) {
      recurringMeetingDetail['countable'] = 1;
    }
    if(optOutVal.length>0) {
      recurringMeetingDetail['countable'] = 0;
    }
    
    //opt-out functionality ends here



    let rmDetails;
    // if (changeType === "created") {

      console.log("------------------------Creating record--------------------------", calendarResource.iCalUId);

      recurringMeetingDetail['resource'] = resource;
      recurringMeetingDetail['resource_id'] = calendarResource.id;
      recurringMeetingDetail['outlook_meeting_id'] = calendarResource.iCalUId;
      // recurringMeetingDetail['subscription_id'] = subscriptionId;
      
      let meetingStartDateWithOffset;
    
      /**
       * If next meeting is today , then set meetingStartDateWithOffset with nextMeeting date 
       * otherwise with calendar startDate
       */

      let instanceStartDate = new Date(instance['start']['dateTime']);
      meetingStartDateWithOffset = calculateDateTimeWithOffset(instanceStartDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
      recurringMeetingDetail['next_meeting_date'] = meetingStartDateWithOffset;
      
      /**
       * Following condition will only execute when meeting start date is on the day of creation of RM and is 
       * after creation time.
       */
      if(meetingStartDateWithOffset.toDateString() == rmCreatedDate.toDateString()) {
    
        let rmCreatedDateWithOffset = calculateDateTimeWithOffset(rmCreatedDate.getTime(), recurringMeetingDetail.TimeZoneOffset);

        if(meetingStartDateWithOffset > rmCreatedDateWithOffset) {
            isMeetingForPreviousDay = true;
            calendarResource.start.dateTime = new String(instanceStartDate.toISOString());
            /** So next meeting date will be set considering today's instacne start date */
            recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);   

        }
      }
       
      rmDetails = await recurringMeetingRepository.saveRecurringMeetings(recurringMeetingDetail);
      
      if(rmDetails !== null && isMeetingForPreviousDay) {
        // new Date(calendarResource['start']['dateTime']);
        // let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
        console.log("Current Date With Offset : ", currentDateWithOffset, " Meeting Start Date With Offset : ", meetingStartDateWithOffset);
     
        // if( currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime() ) {
          //Create a meeting
        //   if(instances.length !== 0) {
        console.log("-----------------Creating with Instace Details --------------------", instance['start']);
        instance['rm_id'] = calendarResource.id;
        instance['iCalUId'] =calendarResource.iCalUId;
        meetingController.createMeeting(instance, userId);
        //   }
        // }
      }
    } 
//   }
   
  /**
   * Following method sends a boolean value whether any field related to dates, frequency is changed 
   * Those fields which play a role in deciding when is the next meeting date going to occur.
   * @param {*} existingRmData 
   * @param {*} incomingRmData 
   */
  function checkIsDateChanged(existingRmData, incomingRmData) {
    if(existingRmData !== null && incomingRmData !== null ) {
        if(existingRmData.start_date.getTime() !== incomingRmData.start_date.getTime() || 
        existingRmData.end_date.getTime() !== incomingRmData.end_date.getTime() || 
        existingRmData.repeat_every !== incomingRmData.repeat_every || 
        existingRmData.frequency_type !== incomingRmData.frequency_type ||
        existingRmData.day_index !== incomingRmData.day_index ||
        existingRmData.weekdays_selected !== incomingRmData.weekdays_selected ) {
        return true
      } else {
          return false;
      }
    } else {
      return false;
    }
  }
  
  /**
   * Following method accept these two params and provides an output of time with offset added
   * @param {*} nextMeetingDateTime 
   * @param {*} timeZoneOffset 
   */
  function calculateDateTimeWithOffset(nextMeetingDateTime, timeZoneOffset) {
    // console.log("--------------next meeting date time without offset in ms :----------", nextMeetingDateTime);
    // console.log(new Date(nextMeetingDateTime));
  
    let totalMinutes = 0;
    //+05:30
    let sign = timeZoneOffset.slice(0, 1); //This can be either + or -
    let timeToAddSubtract = new String(timeZoneOffset.slice(1));
    // console.log("----------time to add substract --------------", timeToAddSubtract);
  
    if (timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
      let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
      // console.log("----hour-----", hour);
      let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":") + 1));
      // console.log("----minutes-----", minutes);
      totalSeconds = ((hour * 60) + minutes) * 60; //Converting to seconds
    } else {
      totalSeconds = timeToAddSubtract * 60 * 60; //Converting Hours to seconds
    }
    if (sign == "?") {
      return new Date(nextMeetingDateTime - (totalSeconds * 1000));
    } else {
      return new Date(nextMeetingDateTime + (totalSeconds * 1000));
    }
  
  }
  
  /**
   * Following method calculates the next meeting date for that rm .
   * There are 6 cases : WEEKLY , DAILY , ABSOLUTE MONTHLY , RELATIVE MONTHLY , ABSOLUTE YEARLY AND RELATIVE YEARLY
   * Based on any of above frequency_type , we calculate the next meeting date with the offset added to it.
   * @param {*} calendarResource 
   * @param {*} timeZoneOffset 
   */
  function nextsetNextMeetingDate(calendarResource, timeZoneOffset) {
    let interval = calendarResource.recurrence.pattern.interval;
    let startDay = calendarResource.start.dateTime;
    let nextMeetingDate = new Date(startDay);
    let index = calendarResource.recurrence.pattern.index;
    
    if (calendarResource.recurrence.pattern.type == "weekly") {
  
      let weekdaysSelected = calendarResource.recurrence['pattern']['daysOfWeek'];
      
      if(weekdaysSelected.length == 1) {
                            
        nextMeetingDate.setDate(nextMeetingDate.getDate() + 7 * interval);
        return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);

      } else {
        
        let meetingDay = WEEK_DAYS[nextMeetingDate.getDay()];
        console.log(`--------meeting day : ${meetingDay}-----------------`);


        if(weekdaysSelected.indexOf(meetingDay) == (weekdaysSelected.length - 1) ) {
            //Set next meeting date after weeks from 1st day in array.
            let gapBwFirstnLastMeetingInWeek = WEEK_DAYS.indexOf(meetingDay) - WEEK_DAYS.indexOf(weekdaysSelected[0])
            let repeatMeetingAfter = (interval - 1 ) * 7 + (7 - gapBwFirstnLastMeetingInWeek);
            
            nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
            return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);

        } else {
            //Set next meeting day for next day in weekDaysSelected array.
            console.log("------Weekdays selected are : ------",weekdaysSelected );
            let meetingDayIndex = weekdaysSelected.indexOf(meetingDay); //This will give index of day in weekDaysSelected array in DB
            
            //Following code will give the week day number (b)
            let meetingWeekDay = WEEK_DAYS.indexOf(meetingDay);
            let nextMeetingWeekDay = WEEK_DAYS.indexOf(weekdaysSelected[meetingDayIndex + 1]); 
            
            let repeatMeetingAfter;
            ( nextMeetingWeekDay < meetingWeekDay ) ? 
            ( repeatMeetingAfter = 7 - meetingWeekDay + nextMeetingWeekDay ) : ( repeatMeetingAfter = nextMeetingWeekDay - meetingWeekDay);
            
            console.log(`--------nextMeetingWeekDay : ${nextMeetingWeekDay} repeatMeetingAfter : ${repeatMeetingAfter} -----------------`);
            
            nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
            return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
        }
    }

  
    } else if (calendarResource.recurrence.pattern.type == "absoluteMonthly") {
  
      nextMeetingDate.setMonth(nextMeetingDate.getMonth() + interval);
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
  
    } else if (calendarResource.recurrence.pattern.type == "relativeMonthly") {
  
      let dayIndex = nextMeetingDate.getDay();
      let monthIndex = nextMeetingDate.getMonth();
      let nextMonthIndex = monthIndex + interval;
  
      let firstDayOfNextMonth = new Date(nextMeetingDate.getFullYear(), nextMonthIndex, 1);
      firstDayOfNextMonth.setHours(nextMeetingDate.getHours());
      firstDayOfNextMonth.setMinutes(nextMeetingDate.getMinutes());
      // console.log("FIrst day of next month ", firstDayOfNextMonth);
  
      if (index == "first") {
        if (dayIndex == firstDayOfNextMonth.getDay()) {
          // return firstDayOfNextMonth;
          return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime() + timeZoneOffset);
        } else {
          while (firstDayOfNextMonth.getDay() !== dayIndex) {
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
          }
          // return firstDayOfNextMonth;
          return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
        }
      }
      if (index == "second") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7)
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "third") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 2);
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "fourth") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 3);
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "last") {
        let lastDayOfNextMonth = new Date(firstDayOfNextMonth.getFullYear(), firstDayOfNextMonth.getMonth() + 1, 0, firstDayOfNextMonth.getHours(), firstDayOfNextMonth.getMinutes(), firstDayOfNextMonth.getSeconds());
        // console.log("Last Day of Next Month is : ", lastDayOfNextMonth);
        while (lastDayOfNextMonth.getDay() !== dayIndex) {
          lastDayOfNextMonth.setDate(lastDayOfNextMonth.getDate() - 1);
        }
        // return lastDayOfNextMonth;
        return calculateDateTimeWithOffset(lastDayOfNextMonth.getTime(), timeZoneOffset);
      }
  
  
  
    } else if (calendarResource.recurrence.pattern.type == "daily") {
      nextMeetingDate.setDate(nextMeetingDate.getDate() + 1)
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
  
    } else if (calendarResource.recurrence.pattern.type == "relativeYearly") {
  
      let dayIndex = nextMeetingDate.getDay();
  
      let firsDayOfSameMonthNextYear = new Date(nextMeetingDate.getFullYear() + 1, nextMeetingDate.getMonth(), 1);
      firsDayOfSameMonthNextYear.setHours(nextMeetingDate.getHours());
      firsDayOfSameMonthNextYear.setMinutes(nextMeetingDate.getMinutes());
      firsDayOfSameMonthNextYear.setSeconds(nextMeetingDate.getSeconds());
  
      if (index == "first") {
        if (dayIndex == firsDayOfSameMonthNextYear.getDay()) {
          // return firsDayOfSameMonthNextYear;
          return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
        } else {
          while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
          }
          // return firsDayOfSameMonthNextYear;
          return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
        }
      }
      if (index == "second") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "third") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 2);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "fourth") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 3);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "last") {
        let lastDayOfSameMonthNextYear = new Date(firsDayOfSameMonthNextYear.getFullYear(), firsDayOfSameMonthNextYear.getMonth() + 1, 0, firsDayOfSameMonthNextYear.getHours(), firsDayOfSameMonthNextYear.getMinutes(), firsDayOfSameMonthNextYear.getSeconds());
        // console.log("Last Day of sam Month next year is : ", lastDayOfSameMonthNextYear);
        while (lastDayOfSameMonthNextYear.getDay() !== dayIndex) {
          lastDayOfSameMonthNextYear.setDate(lastDayOfSameMonthNextYear.getDate() - 1);
        }
        lastDayOfSameMonthNextYear;
        return calculateDateTimeWithOffset(lastDayOfSameMonthNextYear.getTime(), TimeZoneOffset);
      }
  
    } else if (calendarResource.recurrence.pattern.type == "absoluteYearly") {
      nextMeetingDate.setFullYear(nextMeetingDate.getFullYear() + interval);
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
    }
  }
  
  /**
   * Following method will be called to get the timezone offset value from time_zone_Details table.
   * It accepts the timezone name eg : India Standard TIme and provide offset wrt to URC
   * @param {*} timeZoneName 
   */
  async function getTimeZoneOffsetValue(timeZoneName) {
    // console.log("-------------Time Zone Name is : -----------------", timeZoneName);
    let timeZoneOffset = await timeZoneDetailsRepo.getTimeZoneOffsetValue(timeZoneName);
    return timeZoneOffset;
  }



  /**
   * Following function accepts resource_id ( rmId ) and cancels the recurring meeting 
   * by setting the cancellattion date and furthere deleting rm instances in meeting table related to it
   * Also in the end deleted meeting invitees for those instances deleted
   */
  module.exports.cancelRecurringMeeting = async (rmId) => {
    console.log("--------------------------Cancelling Recurring Meeting --------------------------");

    let condition = { where: { 'resource_id': rmId } };
    let removeData = await recurringMeetingRepository.cancelRecurringMeeting(condition);
    let removeInstances = await meetingRepository.cancelRmInstances(rmId);
    return true;

  }