'use strict';
const util = require('util');
const { pool, env } = require('./config');
const get_connection = util.promisify(pool.getConnection).bind(pool);
require('isomorphic-fetch');
const { Client, GraphError } = require('@microsoft/microsoft-graph-client');
const querystring = require('querystring');
const axios = require('axios');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();

exports.cronMeetCheck = async () => {
  const connection = await get_connection();
  try {
    const query = util.promisify(connection.query).bind(connection);
    
    let client_secret = env.CLIENT_SECRET;

    let startDate = new Date();
    startDate.setDate(startDate.getDate() - 1);

    let endDate   = new Date();
    endDate.setDate(endDate.getDate() + 1);

    let userSql = await query(`select t.*,u.id from user_tokens t left join users u on u.email = t.email`);

    console.log("userSql", endDate);

    let sDateTime = await getDates(startDate); 
    let eDateTime = await getDates(endDate); 

    // get all the meeting outlook ids from meetings 
    let sqlIds = `select outlook_meeting_id from meetings where start_date >= ? and end_date <= ?`;
    let allMeetIDs = await query(sqlIds, [sDateTime, eDateTime]);
    let finalArry = [];
    if(allMeetIDs.length > 0) {
      allMeetIDs.forEach(async c => {
        c.outlook_meeting_id != '' ?  finalArry.push(c.outlook_meeting_id): '';
      });
    }
    
    let sqlRMIds = `select resource_id from recurring_meetings where start_date >= ? and end_date <= ?`;
    let allRMMeetIDs = await query(sqlRMIds, [sDateTime, eDateTime]);
    let finalRMArray = [];
    if (allRMMeetIDs.length > 0) {
      allRMMeetIDs.forEach(async c => {
        c.resource_id != '' ? finalRMArray.push(c.resource_id) : '';
      });
    }
    
    // check user data exists
    if(userSql.length > 0) {

      let promises = userSql.map(async (d) => {
      
        // console.log(d);
        let userMainId = d.id ? d.id : '';
        let accessToken = d.accessToken; 

        let graph = Client.initWithMiddleware({
            authProvider: {
            getAccessToken: async () => accessToken
            },
        });

        try {
          
          // let eventDetails = await graph.api("/me/events?$top=15&$filter=isOrganizer eq true").get();
          let eventDetails = await graph.api('/me/calendar/calendarView?startDateTime='+startDate.toISOString()+'&endDateTime='+endDate.toISOString()+'&$top=15&$filter=isOrganizer eq true').get();

          if( eventDetails.value.length > 0 ) {
            console.log("event details1");
            await checkMeetings(eventDetails.value, finalArry, userMainId, query);
          }

        } catch (error) {
          console.log("error", error.code);
          if(error.code == "InvalidAuthenticationToken") {
              try {
                
                  accessToken = await refreshToken(d.email, d.refreshToken, env.REDIRECT_URL, client_secret,query);
                  console.log("accessToken");
                  console.log(accessToken);
                  

                  graph = Client.initWithMiddleware({
                  authProvider: {
                      getAccessToken: async () => accessToken
                  }
                  })
                  
                  let eventDetails = await graph.api('/me/calendar/calendarView?startDateTime='+startDate.toISOString()+'&endDateTime='+endDate.toISOString()+'&$top=15&$filter=isOrganizer eq true').get();
                  
                  if( eventDetails.value.length > 0 ) {
                    console.log("event details2");
                    await checkMeetings(eventDetails.value, finalArry, userMainId, query);
                  }


              } catch (error) {
                  console.log("____________");
                  console.error(error);
                  console.log("____________");
                  // throw error;
              } 
          }
        }   

      });
      await Promise.all(promises);
    }
    console.log("End")
      
  } catch (error) {
    console.error('SOME ERROR OCCURED');
  } finally {
    connection.release()
  }
}

// To get dates 
async function getDates(dateVal) {
  let date = dateVal.getFullYear()+'-'+(dateVal.getMonth()+1)+'-'+dateVal.getDate();
  let time = dateVal.getHours() + ":" + dateVal.getMinutes() + ":" + dateVal.getSeconds();
  let datetime = date+' '+time;

  return datetime;
}

// common function to check the meeting starts here
async function checkMeetings(details, meetIds, userMainId, query) { 
  if(meetIds) {
    let promises = details.map(async (el) => {
      console.log("Subject : ", el["subject"], " Type : ", el['type']);
      if (el.type == "occurrence") {
        if (!finalRMArray.includes(el.seriesMasterId) && el.isOrganizer) {
          console.log(`New Recurring Meeting Found ${el.seriesMasterId }`);
          // await createMeeting(el, userMainId, query);
        }
      }else{
        if (!meetIds.includes(el.iCalUId) && el.isOrganizer) {
          console.log(`New Normal Meeting Found ${el.iCalUId}`);
          await createMeeting(el, userMainId, query);
        }
      }
    });
    await Promise.all(promises);
  }
}
// common function to check the meeting Ends here

//function to refresh access token starts here
async function refreshToken(email, refresh_token, redirect_uri, client_secret,query) {
  try {
    const requestBody = {
      client_id: env.CLIENT_ID,
      refresh_token: refresh_token,
      redirect_uri: redirect_uri,
      grant_type: 'refresh_token',
      client_secret: client_secret
    };

    const axiosConfig = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }

    const graphResponse = await axios.post('https://login.microsoftonline.com/common/oauth2/v2.0/token',
      querystring.stringify(requestBody), axiosConfig);
    console.log(graphResponse);
    

    if (graphResponse.status === 200) {
      const tokenData = graphResponse.data;
      const sql = `UPDATE user_tokens SET accessToken = ?, refreshToken = ? WHERE email = ? `;
      await query(sql, [tokenData.access_token, tokenData.refresh_token, email])
      return tokenData.access_token;
    } else {
      throw new Error(`failed to refresh access token for ${email}`);
    }
  } catch (error) {
    console.log(error);
    
    console.log("error at refreshToken function")
  }
}
//function to refresh access token ends here

//common function create meeting starts here
async function createMeeting(calendarResource, userId, query) {
  try {
    console.log("------- Creating Meeting for subject : " , calendarResource.subject);
    let incomingInviteeDetailed = [];
    let meetingInvites = [];
      for (let j = 0; j < calendarResource.attendees.length; j++) {
        if ((calendarResource.attendees[j].status.response != "declined") && (calendarResource.attendees[j].emailAddress.address != calendarResource.organizer.emailAddress.address)) {
          meetingInvites.push(calendarResource.attendees[j].emailAddress.address)
          incomingInviteeDetailed.push({ firstname: calendarResource.attendees[j].emailAddress.name, email: calendarResource.attendees[j].emailAddress.address });
        }
    }

    // Data - meeting 
    const location = calendarResource.location.displayName || null;
      // calling timezone function
      let timeZoneOffset = await getTimeZoneOffsetValue(calendarResource.originalStartTimeZone, query);
      let meetingData = {
        outlook_meeting_id: calendarResource.iCalUId,
        meeting_id: calendarResource.id,
        subject: calendarResource.subject,
        location: location,
        type: calendarResource.type,
        start_date: calendarResource.start.dateTime,
        end_date: calendarResource.end.dateTime,
        TimeZoneOffset: timeZoneOffset,
        TimeZoneName: calendarResource.originalStartTimeZone,
        cancellation_date: null,
        user_id: userId,
        initiativeId: 182,
        countable: 1,
        rm_id : calendarResource['rm_id'] !== undefined ? calendarResource['rm_id'] : null
      }
      

      //opt-out check meetings starts here
      let optOutVal = [];
      if (incomingInviteeDetailed) {
          optOutVal = incomingInviteeDetailed.filter(function (listItem) {
              return listItem.email == 'opt-out@sustainably.run';
          });
      }
      
      if(calendarResource['bodyPreview'].includes("<---")) {
        meetingData.countable = 1;
      }
      if(optOutVal.length>0) {
        meetingData.countable = 0;
      }
      //opt-out check meetings ends here



      //get user details data
      let userDetail = await organizationSettingsById(userId, query);
      console.log("userDetail");
      console.log(userDetail);
      
      let mailTimes;
      if (userDetail[0]) {
        let addHourinToSecond = userDetail[0].hour * 60 * 60;
        let addMinuteinToSecond = userDetail[0].minute * 60;

        if (userDetail[0].emailSend == 'After') {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.end_date);
          mailTimes = mailTimes + totalSeconds;
        } else {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.start_date);
          mailTimes = mailTimes - totalSeconds;
        }
        const firstDate = new Date().setHours(12, 0, 0, 0);
        const secondDate = new Date(userDetail[0].trialEnd);
        let user_status = userDetail[0].user_status;
        if (secondDate > firstDate) {
          user_status = 'active'
        }
        console.log("status", user_status);
        userDetail[0].startTime = meetingData.start_date;
        userDetail[0].TimeZoneOffset = meetingData.TimeZoneOffset;
        userDetail[0].TimeZoneName = meetingData.TimeZoneName;

        const twodays_inseconds = 60 * 60 * 480000000;
        let total_seconds = findSeconds(new Date().toISOString(), meetingData.start_date);
        if (total_seconds < twodays_inseconds) {
          if (user_status != 'inactive' && userDetail[0].status != 'force_inactive' && meetingData.countable == 1) {
            meetingData.org_id = userDetail[0].orginasation_id;
          
            //create the meeting 
            let meetingSave = await createMeetingFun(meetingData, query);

            // check meetings saved or not
            if (meetingSave && meetingSave.insertId) {

              // formatting the invites array
              let inviters = createInvites(incomingInviteeDetailed, meetingSave.insertId);

              // calling step function
              let stepfunc = await createStepFunction(incomingInviteeDetailed, mailTimes, userDetail[0], meetingSave.insertId);
              let arnName = stepfunc.executionArn;

              //update the meetings by arnName
              await query(`update meetings SET arnName='${arnName}' where id=${meetingSave.insertId}`); 

              //insert invites data to Db
              let insetInvitees = await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company,createdAt,updatedAt) VALUES ?`, [inviters]);
            }
          }
        }
      } 
  } catch (error) {
    console.log(error);
  }
  return true;
}
//create meeting Ends here

// get time zone from DB starts here
async function getTimeZoneOffsetValue(timeZoneName, query) {
  let sql = `select time_zone_offset from time_zone_details where time_zone_name = ?`;
  let timeZoneOffset = await query(sql, timeZoneName);
  if (timeZoneOffset[0]) {   
    return timeZoneOffset[0].time_zone_offset
  } else {
      return "+0";
  }
}
// get time zone from DB ends here

// get user data from DB starts here 
async function organizationSettingsById (userId, query) {
  let sql = `SELECT  users.id,u1.status as user_status,u1.trialEnd, settings.id as setting_id,users.orginasation_id,settings.emailSend,settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id LEFT JOIN users u1 on users.orginasation_id=u1.id WHERE users.id = ?`;
  
  const resultData = await query(sql,userId);

  return resultData;
}
// get user data from DB ends here 

// common function for time start here
function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  var dif = t1.getTime() - t2.getTime();
  var Seconds_from_T1_to_T2 = dif / 1000;
  
  return Seconds_from_T1_to_T2;
}
// common function for time ends here

// stepfunction starts here
async function createStepFunction(invites, delay, setting, id) {

  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    const stateMachineArn = env.AWS_STEP;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
          "delay_seconds": Math.round(delay),
          "obj": invites,
          "id": id,
          "setting": setting
        }),
    }).promise();
  return newstateMachineArn;
  } catch (error) {
    throw error;
  }
}
// stepfunction ends here

//insert or create meeting start here
async function createMeetingFun(meetingData, query) {
  try {
    let sql = `INSERT INTO meetings(outlook_meeting_id,meeting_id,subject,location,type,start_date,end_date,TimeZoneOffset,TimeZoneName,cancellation_date,user_id,initiativeId,countable,rm_id,org_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    let insResult = await query(sql, [meetingData.outlook_meeting_id, meetingData.meeting_id, meetingData.subject, meetingData.location, meetingData.type, meetingData.start_date, meetingData.end_date, meetingData.TimeZoneOffset, meetingData.TimeZoneName, meetingData.cancellation_date, meetingData.user_id, meetingData.initiativeId, meetingData.countable, meetingData.rm_id, meetingData.org_id]);

    return insResult;
    
  } catch(err) {
    console.log(err);
  }
}
//insert or create meeting ends here

//invites array format start here
function createInvites(obj, meeting_id) {
  let invites = []
  for (let i = 0; i < obj.length; i++) {
    let inviteIndiividual = [];
    inviteIndiividual.push(obj[i].firstname);
    inviteIndiividual.push(obj[i].lastname);
    inviteIndiividual.push(meeting_id);
    inviteIndiividual.push(obj[i].email);
    inviteIndiividual.push(obj[i].title || " ");
    inviteIndiividual.push(obj[i].company);
    inviteIndiividual.push(new Date());
    inviteIndiividual.push(new Date());

    invites.push(inviteIndiividual);
  }
  return invites;
}
//invites array format ends here