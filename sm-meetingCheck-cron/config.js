const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    CLIENT_SECRET: "bWF_oh~ZtYv5G7a.1x-Za-59t9kAm1dg.O",
    CLIENT_ID: "cec04b71-137b-4a99-80c6-e0fc88a2e7c5",
    REDIRECT_URL : "https://dev-outlook-addin-ss.emvigotechnologies.com/auth/openid/return",
    AWS_STEP: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev"
  },
  prod: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    CLIENT_SECRET: "bWF_oh~ZtYv5G7a.1x-Za-59t9kAm1dg.O",
    CLIENT_ID: "cec04b71-137b-4a99-80c6-e0fc88a2e7c5",
    REDIRECT_URL : "https://outlook-addin-meetings.sustainably.run/auth/openid/return",
    AWS_STEP: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable"
  },
};

let env = config[process.env.stage];
exports.env = env;
exports.pool = mysql.createPool({
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
});
