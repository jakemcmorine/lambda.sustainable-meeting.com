'use strict';

// include mysql module
const util = require('util');
var pool = require('./config').pool;
const getConnection = util.promisify(pool.getConnection).bind(pool);

module.exports.getFooter = async (event) => {
  // TODO implement
  event = event.body;
  console.log(event)
  let budgetExceeded = false, footer = "";
  let responseBody = {};
  if (event.Email) {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    // let resp2 = await query(`select *from users where LOWER(email) ='${event.Email.toLocaleLowerCase()}'`);
    let resp2 = await query(`SELECT users.*, u1.status as user_status, u1.trialEnd, u1.isMailVerified as mailVerified, u1.isActive as IPActive FROM users LEFT JOIN users u1 on users.orginasation_id = u1.id where LOWER(users.email) ='${event.Email.toLocaleLowerCase()}'`);
    console.log(resp2);
    let resp3 = await query(`select * from settings where user_id=${resp2[0].orginasation_id}`);
    console.log(resp3);

    /* Check feature access for budget and footer */
    let planAccess = await query(`SELECT P.feature_id FROM user_plans AS UP LEFT JOIN plans AS P ON P.plan_details_id = UP.plan_id
       WHERE org_id = ${resp2[0].orginasation_id}`);
    console.log("Plan access list", planAccess);
    let budgetAccess = planAccess.some(ft => ft.feature_id == 4);
    let footerAccess = planAccess.some(ft => ft.feature_id == 1);
    let budgetStatus = resp3[0].budgetStatus;
    if(resp3[0].budget == null || resp3[0].budget == 0 || resp3[0].budget == undefined || resp3[0].budget == "") {
      budgetStatus = 0;
    }
    if(budgetAccess &&  budgetStatus == 1) {
      let resp4 = await query(`SELECT SUM(tree_counts.trees) AS trees FROM tree_counts
      LEFT JOIN subscriptions ON subscriptions.subscriptionId=tree_counts.subscriptionId
      WHERE tree_counts.subscriptionId IS NOT NULL AND subscriptions.status = "active" AND
      org_id = ${resp2[0].orginasation_id} AND tree_counts.date >= subscriptions.cycleStartDate AND tree_counts.date <= subscriptions.billingDate`)
      if(resp4[0].trees >= resp3[0].budget) {
        budgetExceeded = true;
      }
    }
    footer = resp3[0].footer;
    if (footerAccess == false || resp3[0].footer == null || resp3[0].footer == undefined || resp3[0].footer == '') {
      let resp4 = await query(`select  * from intiatives where id=${resp2[0].initiativeId}`);
      console.log(resp4);
      footer = resp4[0].mail5_body;
    }

    console.log("Footer", footer);
    connection.release();
    const firstDate = new Date().setHours(12, 0, 0, 0);
    const secondDate = new Date(resp2[0].trialEnd);
    let user_status = resp2[0].user_status
    if (secondDate > firstDate) {
      user_status = 'active'
    }
    console.log("user status", user_status);
    if (user_status != 'active' || resp2[0].mailVerified != 1 || resp2[0].IPActive == 0 || budgetExceeded == true) {
      footer = ''
    }
    responseBody = { "Id": resp2[0].id, "FirstName": resp2[0].firstName, "Footer": footer };
  }
 
  
  var response = {
    "statusCode": 200,
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify(responseBody),
  };
  return responseBody;

};