'use strict';
const util = require('util');
const moment = require('moment');
const { env } = require('./config');
var pool = require('./config').pool;
const axios = require('axios');
const AWS = require('aws-sdk');
const { parse } = require('path');
const { weekdays } = require('moment');
const stepfunctions = new AWS.StepFunctions();
// include mysql module
const query = util.promisify(pool.query).bind(pool);
const WEEK_DAYS = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

exports.desktopRM = async () => {
  try {
    let todayMeetingDatas = await query(`SELECT M.*, date(M.next_meeting_date) AS nextday, TIME(M.start_date) AS start, TIME(M.end_date) AS end, 
    IF(SUBSTRING(M.TimeZoneOffset, 1, 1) = '+', ADDTIME(M1.meeting_start, SUBSTRING(M.TimeZoneOffset,2)), SUBTIME(M1.meeting_start, SUBSTRING(M.TimeZoneOffset,2))) AS M_START,date(CURDATE()) AS curdate 
    FROM recurring_meetings AS M LEFT JOIN recurring_meeting_activities AS M1 on M.recurring_meeting_id = M1.recurring_meetings_id GROUP BY M.recurring_meeting_id HAVING M.calendar_type = "outlookDesktop" AND M.countable = 1 AND (date(M.next_meeting_date) = CURDATE() OR date(M_START) = CURDATE())`);
    console.log(todayMeetingDatas.length);
    if (todayMeetingDatas.length > 0) {
      let promises = todayMeetingDatas.map(async (todayMeetingData) => {
        // todayMeetingDatas.forEach(async (todayMeetingData) => {
        console.log('inside todayMeetingData', todayMeetingData);
        let offsetSubString = todayMeetingData.TimeZoneOffset.substring(2)
        var todayActivityRMs = await query(`SELECT *, IF(SUBSTRING("${todayMeetingData.TimeZoneOffset}", 1, 1) = '+', ADDTIME(meeting_start, "${offsetSubString}"), SUBTIME(meeting_start,"${offsetSubString}")) AS M_START, 
        IF(SUBSTRING("${todayMeetingData.TimeZoneOffset}", 1, 1) = '+', ADDTIME(meeting_date, "${offsetSubString}"), SUBTIME(meeting_date,"${offsetSubString}")) AS M_DATE
        FROM recurring_meeting_activities HAVING recurring_meetings_id = ${todayMeetingData.recurring_meeting_id} and (date(M_START) = CURDATE() or date(M_DATE) = CURDATE())`);

        console.log('todayActivityRMs', JSON.stringify(todayActivityRMs));
        console.log('inside todayMeetingData', JSON.stringify(todayMeetingData));

        var todayInviteRMResult = await query(`SELECT * FROM recurring_meeting_invitees WHERE recurring_meetings_id = ${todayMeetingData.recurring_meeting_id}`);
        let todayInviteRM = [];
        for (let i = 0; i < todayInviteRMResult.length; i++) {
          let inviteIndividual = {
            'firstname': todayInviteRMResult[i].first_name,
            'lastname': todayInviteRMResult[i].last_name,
            'email': todayInviteRMResult[i].email_id,
          };
          todayInviteRM.push(inviteIndividual);
        }
        console.log('todayInviteRM', JSON.stringify(todayInviteRM));

        let today = moment().format('YYYY-MM-DD');
        let meetingStart = moment(today + ' ' + todayMeetingData.start).format();
        let meetingEnd = moment(today + ' ' + todayMeetingData.end).format();
        ({meetingStart, meetingEnd} = await calculateStartEnd(todayMeetingData))
        let meetingData = {
          subject: todayMeetingData.subject,
          location: todayMeetingData.location,
          start_date: meetingStart,
          end_date: meetingEnd,
          is_all_day: todayMeetingData.AllDay == 1 ? 'Y' : 'N',
          TimeZoneOffset: todayMeetingData.TimeZoneOffset,
          TimeZoneName: todayMeetingData.TimeZoneName,
          outlook_meeting_id: todayMeetingData.outlook_meeting_id
        }
        console.log(meetingData);
        if (todayActivityRMs.length > 0) {
          console.log('inside todayActivityRMs');
          todayInviteRM = JSON.parse(todayActivityRMs[0].invitees)
          if (isValidate(todayActivityRMs[0].meeting_start) == true && isValidate(todayActivityRMs[0].meeting_end) == true) {
            meetingData["start_date"] = todayActivityRMs[0].meeting_start;
            meetingData["end_date"] = todayActivityRMs[0].meeting_end;
          }
        }        

        let userDetail = await query(`SELECT users.id, u1.status as user_status, u1.trialEnd, users.isMailVerified, users.initiativeId, settings.id as setting_id, users.orginasation_id,
        settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
        FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
        LEFT JOIN users u1 on users.orginasation_id = u1.id 
        LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
        WHERE
        LOWER(users.email) = '${todayMeetingData.organizer.toLocaleLowerCase()}'`);
        console.log(userDetail);
        let weekdaysArray=[];
        if (todayMeetingData.weekdays_selected){
          weekdaysArray = todayMeetingData.weekdays_selected.split(",")
        }

        let nextMeetingDateArray = {
          RecurrenceType: todayMeetingData.frequency_type,
          PatternStartDate: meetingStart,
          PatternEndDate: todayMeetingData.end_date,
          Interval: todayMeetingData.repeat_every,
          Instance: todayMeetingData.day_index,
          DaysOfWeek: weekdaysArray,
        }
    
        let todayMeetingEndDate = moment(todayMeetingData.end_date).format('YYYY-MM-DD');
        let today1 = new Date(moment().endOf('day'));
        console.log("todayMeetingEndDate ", todayMeetingEndDate);
        
        if (new Date(todayMeetingEndDate) > today1 ){
            let {nextMeetingDate} = await setNextMeetingDate(nextMeetingDateArray, todayMeetingData.TimeZoneOffset);
            console.log("nextMeetingDate ", nextMeetingDate);
            let updateMeetings1 = await query(`UPDATE recurring_meetings SET next_meeting_date = ? WHERE recurring_meeting_id = ?`, [nextMeetingDate, todayMeetingData.recurring_meeting_id]);
        } else{
            console.log("patternEndDate was today ");
        }
        // let invitesArray = await createInvites(todayInviteRM, todayMeetingData.id)
        // console.log(invitesArray);
        if (moment(meetingData["start_date"]).utcOffset(todayMeetingData.TimeZoneOffset).format('YYYY-MM-DD') === today) {
          //calling Create Meeting
          let meeting = await createMeetingFun(meetingData, todayInviteRM, userDetail);
        }
      })
      await Promise.all(promises);
    } else {
      console.log("todayMeetingData is empty");
    }
  }
  catch (error) {
    console.log("err", error);
  }
};

exports.createRecurring = async (event) => {
  try {
    console.log("event.body", event.body);
    event = event.body;
    var meetingId = 0;
    var obj = JSON.stringify(event);
    obj = JSON.parse(obj)
    let invitesOr = Object.assign([], obj.invites);
    
    let optOutVal = [];
    let countable = 1;
    try {
      if (invitesOr.length) {
        optOutVal = invitesOr.filter(function (listItem) {
          return listItem.email == 'opt-out@sustainably.run';
        });
      }
    } catch (error) {
      console.log(error);
    }
    if (optOutVal.length > 0) {
      countable = 0;
    }

    delete obj.invites;
    delete obj.organiser;
    event.email = event.organiser && event.organiser.email ? event.organiser.email : null;
    event.RecurrenceType = getRecurrenceType(event.RecurrenceType);

    let meetingCheckID = await query(`SELECT * FROM recurring_meetings WHERE outlook_meeting_id = '${event.OutlookID}'  `)
    if (!isValidate(meetingCheckID[0])) {
      meetingCheckID = await query(`SELECT * FROM recurring_meetings WHERE start_date = '${event.PatternStartDate}' AND subject = '${event.subject}' AND organizer = '${event.organiser.email.toLocaleLowerCase()}'`)
    }
    console.log("meetingCheckID", meetingCheckID);
    if (isValidate(meetingCheckID[0])) {
      event.OutlookID = meetingCheckID[0].outlook_meeting_id;
      console.log("inside sync ");
      let meetingData = await query(`SELECT * FROM recurring_meetings as M LEFT JOIN recurring_meeting_invitees as MI on MI.recurring_meetings_id = M.recurring_meeting_id  WHERE outlook_meeting_id = '${event.OutlookID}' `);
      if (!Array.isArray(meetingData) || meetingData.length <= 0 || !meetingData[0].recurring_meeting_id) {
        return ({ "code": 0, "message": "Recurring meeting not found", "Meeting": event });
      }
      let checkMeeting = await checkMeetings(event, meetingData);
      console.log("checkMeeting", checkMeeting);
      if (checkMeeting.message) {
        if (checkMeeting.inviteesChange) {
          await query(`DELETE FROM recurring_meeting_invitees WHERE recurring_meetings_id = '${meetingData[0].recurring_meeting_id}' `);
          if (checkMeeting.invitees.length > 0) {
            let invitees = createRecurringInvites(checkMeeting.invitees, meetingData[0].recurring_meeting_id);
            let sql1 = `INSERT INTO recurring_meeting_invitees(recurring_meetings_id, first_name, last_name, email_id,title,company) VALUES ?`;
            await query(sql1, [invitees]);
          }
        }
      }

      if (meetingCheckID[0].user_id == null || meetingCheckID[0].org_id == null) {

        let userDetail1 = await query(`SELECT users.id, u1.status as user_status, u1.trialEnd, users.isMailVerified, users.initiativeId, settings.id as setting_id, users.orginasation_id,
        settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
        FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
        LEFT JOIN users u1 on users.orginasation_id = u1.id 
        LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
        WHERE
        LOWER(users.email) = '${event.organiser.email.toLocaleLowerCase()}'`);
        console.log(userDetail1);

        await query(`UPDATE recurring_meetings SET user_id = '${userDetail1[0].id}',
    org_id = '${userDetail1[0].orginasation_id}' WHERE outlook_meeting_id = '${meetingCheckID[0].outlook_meeting_id}' `);
      }
      return ({ "code": 0, "message": "duplicate entry", "Meeting": event })
    }

    let keysCheck = [
      { value: 'OutlookID', type: "string" }, { value: 'invites', type: "array" },
      { value: 'PatternStartDate', type: "date" }, { value: 'PatternEndDate', type: "date" },
      { value: 'start', type: "date" }, { value: 'end', type: "date" }, { value: 'email', type: "string" }
    ]
    let isValid = await isValidateInputs(event, keysCheck)
    console.log("isValid", isValid)
    if (!isValid) {
      return ({ "code": 0, "message": "Invalid inputs", "Meeting": event })
    }
    // if (new Date(event.PatternStartDate) < new Date()) {
    //   const diffTime = Math.abs(new Date().setHours(0,0,0,0) - new Date(event.PatternStartDate).setHours(0,0,0,0));
    //   const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
    //   console.log("diffDays", diffDays)
    //   if (diffDays > 730) {
    //     return ({ "code": 0, "message": "Cannot create Recurring Meeting with past start date.", "Meeting": event});
    //   }
    // }
    if (new Date(event.PatternEndDate) < new Date()) {

      let responseBody = { "code": 0, "message": "Cannot create Recurring Meeting in past date.", "Meeting": event };
      return responseBody;
    }
    let userDetail = await query(`SELECT users.id, u1.status as user_status, u1.trialEnd, users.isMailVerified, users.initiativeId, settings.id as setting_id, users.orginasation_id,
        settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
        FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
        LEFT JOIN users u1 on users.orginasation_id = u1.id 
        LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
        WHERE
        LOWER(users.email) = '${event.organiser.email.toLocaleLowerCase()}'`);
    console.log(userDetail);

    if (userDetail[0]) {
      try {
        console.log("event.PatternStartDate", event.PatternStartDate);

        let eventDate = moment(event.PatternStartDate).utcOffset(event.TimeZoneOffset);
        let todayDate = moment().utcOffset(event.TimeZoneOffset);
        let isMeetingCreate = false;
        event.isMeetingCreate = isMeetingCreate;
        let nextMeetingDate = moment(event.PatternStartDate).utcOffset(event.TimeZoneOffset).format();
        let endOfDay = moment().utcOffset(event.TimeZoneOffset).endOf('day');
        let localPatternEnd = moment(event.PatternEndDate).utcOffset(event.TimeZoneOffset);

        if ((moment(event.start).utcOffset(event.TimeZoneOffset)).isSameOrBefore(endOfDay) && 
          endOfDay.isBefore(localPatternEnd)) {
          console.log("inside condition");
          let end = (moment().utcOffset(event.TimeZoneOffset)).endOf('day');
          let localDate='';
          do {
            ({ nextMeetingDate, isMeetingCreate } = await setNextMeetingDate(event, event.TimeZoneOffset));
            event.PatternStartDate = new Date(nextMeetingDate)
            localDate = moment(nextMeetingDate).utcOffset(event.TimeZoneOffset);
          } while (nextMeetingDate != null && localDate.isBefore(end));
          event.PatternStartDate = event.start;
        }

        // set next meeting when pattern old date to today with coming time
        let dateFormat = localPatternEnd.format('YYYY-MM-DD')
        if (eventDate.isBefore(endOfDay) && todayDate.isBefore(localPatternEnd) &&
          dateFormat == todayDate.format('YYYY-MM-DD')) {
			console.log("inside past day pattern");
          isMeetingCreate = true;
          let date = `${moment(event.PatternEndDate).format('YYYY-MM-DD')}T${moment(event.PatternStartDate).format('HH:mm:ss')}`;
          console.log("date", date)
          nextMeetingDate = moment(date).utcOffset(event.TimeZoneOffset).format()
        }
        console.log("1111111111111", isMeetingCreate, nextMeetingDate)

        console.log("nextMeetingDate", nextMeetingDate, isMeetingCreate);

        let recurringDataArr = [
          "outlookDesktop",
          event.PatternStartDate,
          event.PatternEndDate,
          event.AllDay == 1 ? 'Y' : 'N',
          event.Interval,
          event.RecurrenceType,
          event.DaysOfWeek.toString() || "",
          event.day_index,
          event.subject,
          event.location,
          event.TimeZoneOffset,
          event.TimeZoneName,
          new Date(),
          new Date(),
          userDetail[0].id,
          event.organiser.email.toLocaleLowerCase(),
          userDetail[0].orginasation_id,
          event.OutlookID,
          countable,
          nextMeetingDate,
        ]
        console.log("recurringDataArr");
        console.log(recurringDataArr);

        let sql = `INSERT INTO recurring_meetings(calendar_type,start_date,end_date,is_all_day,repeat_every,frequency_type,weekdays_selected,day_index,subject,location,TimeZoneOffset,TimeZoneName,created_at,updated_at,user_id,organizer,org_id,outlook_meeting_id,countable,next_meeting_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        let meetingDetails = await query(sql, recurringDataArr);
        meetingId = meetingDetails.insertId;

        let inviters = createRecurringInvites(invitesOr, meetingDetails.insertId);
        // let meetingInvitees = createInvites(invitesOr, meetingDetails.insertId);
        console.log(inviters);
        let sql1 = `INSERT INTO recurring_meeting_invitees(recurring_meetings_id, first_name, last_name, email_id,title,company) VALUES ?`;
        let inviteMeetingDetails = await query(sql1, [inviters]);

        if ((eventDate.format('YYYY-MM-DD') === todayDate.format('YYYY-MM-DD') &&
          todayDate.isSameOrBefore(eventDate)) || isMeetingCreate) {
          let startTime = ((event.start).split("T"))[1];
          let endTime = ((event.end).split("T"))[1];
          let meetingData = {
            subject: event.subject,
            location: event.location,
            start_date: `${moment().format('YYYY-MM-DD')}T${startTime}`,
            end_date: `${moment().format('YYYY-MM-DD')}T${endTime}`,
            is_all_day: event.AllDay == 1 ? 'Y' : 'N',
            TimeZoneOffset: event.TimeZoneOffset,
            TimeZoneName: event.TimeZoneName,
            outlook_meeting_id: event.OutlookID
          }
          console.log("1111111111111", meetingData)

          await createMeetingFun(meetingData, invitesOr, userDetail);
        }
      } catch (error) {
        console.log(error);
      }
    }
    event['MeetingId'] = meetingId;
    let responseBody = { "code": 0, "message": "Recurring Meeting Created.", "Meeting": event };
    return responseBody;
  } catch (error) {
    console.log("error", error)
  }
};

exports.updateRecurring = async (event) => {
  event = event.body;
  let newMeetingData = {};
  event.email = event.organiser && event.organiser.email ? event.organiser.email : null;
  event.RecurrenceType = getRecurrenceType(event.RecurrenceType);
  console.log("event", JSON.stringify(event));
  let keysCheck = [
    {value:'OutlookID',type:"string"}, {value:'invites',type:"array"},
    {value:'PatternStartDate',type:"date"}, {value:'PatternEndDate',type:"date"},
    {value:'start',type:"date"}, {value:'end',type:"date"}, {value:'email',type:"string"}
  ]
  let isValid = await isValidateInputs(event,keysCheck)
  console.log("isValid", isValid)
  if (!isValid) {
    return ({ "code": 0, "message": "Invalid input", "Meeting": event})
  }

  if (new Date(event.PatternEndDate).getTime() < new Date().getTime()) {
    return await exports.deleteRecurring({body: event});
  }  
  
  // if (new Date(event.PatternStartDate) < new Date(moment().startOf('day'))) {
  //   const diffTime = Math.abs(new Date().setHours(0,0,0,0) - new Date(event.PatternStartDate).setHours(0,0,0,0));
  //   const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
  //   console.log("diffDays", diffDays)
  //   if (diffDays > 730) {
  //     return ({ "code": 0, "message": "Cannot create Recurring Meeting with past start date.", "Meeting": event});
  //   }
  // }

  let isMeetingCreate = false;
  let meetingData = await query(`SELECT * FROM recurring_meetings as M LEFT JOIN recurring_meeting_invitees as MI on MI.recurring_meetings_id = M.recurring_meeting_id  WHERE outlook_meeting_id = '${event.OutlookID}' `);
  if(!Array.isArray(meetingData) || meetingData.length <= 0 || !meetingData[0].recurring_meeting_id) {
    meetingData = await query(`SELECT * FROM recurring_meetings as M LEFT JOIN recurring_meeting_invitees as MI on MI.recurring_meetings_id = M.recurring_meeting_id
    WHERE M.start_date = '${event.PatternStartDate}' AND M.subject = '${event.subject}' AND M.organizer = '${event.organiser.email.toLocaleLowerCase()}'`)
      console.log("meetingCheckSub", meetingData);
      if (isValidate(meetingData[0])) {
        event.OutlookID = meetingData[0].outlook_meeting_id;
        //return ({ "code": 0, "message": "Duplicate Recurring meeting ", "Meeting": meetingCheckSub[0] });
      } else {
        return await exports.createRecurring({body: event});
      }
  }
  let checkMeeting = await checkMeetings(event, meetingData);
  console.log("checkMeeting",checkMeeting);
  let updateMeetings=0;

  if (checkMeeting.message && Array.isArray(meetingData) && meetingData.length > 0) {
    newMeetingData = {
      start_date: event.start,
      end_date: event.end,
      OriginalDate: event.start,
      outlook_meeting_id :event.OutlookID,
      invites: event.invites,
      subject: event.subject,
      TimeZoneOffset: event.TimeZoneOffset,
      TimeZoneName: event.TimeZoneName,
      IsCancelled : event.IsCancelled,
      location: event.location,
      is_all_day: event.AllDay == 1 ? 'Y' : 'N',
    };            
    if (checkMeeting.recurrenceChange == true){
      let eventDate = moment(event.PatternStartDate).utcOffset(event.TimeZoneOffset);
      let nextMeetingDate = moment(event.start).utcOffset(event.TimeZoneOffset).format();
      let endOfDay = (moment().utcOffset(event.TimeZoneOffset)).endOf('day');
      let localPatternEnd = moment(event.PatternEndDate).utcOffset(event.TimeZoneOffset);
      let todayDate = moment().utcOffset(event.TimeZoneOffset);

      if ((moment(event.start).utcOffset(event.TimeZoneOffset)).isSameOrBefore(endOfDay) && 
      endOfDay.isBefore(localPatternEnd)) {
        checkMeeting.isMeetingCreate = isMeetingCreate;
        let localDate = '';
        do {
          ({nextMeetingDate, isMeetingCreate} = await setNextMeetingDate(checkMeeting, event.TimeZoneOffset));
          checkMeeting.PatternStartDate = new Date(nextMeetingDate)
          localDate = moment(nextMeetingDate).utcOffset(event.TimeZoneOffset);
		  console.log("update inside do ");
        } while (nextMeetingDate != null && localDate.isBefore(endOfDay)); 
        checkMeeting.PatternStartDate = event.start; 
      }

      let dateFormat = localPatternEnd.format('YYYY-MM-DD')
      if (eventDate.isBefore(endOfDay) && todayDate.isBefore(localPatternEnd) &&
      dateFormat == todayDate.format('YYYY-MM-DD')) {
        isMeetingCreate = true;
        let date = `${moment(event.PatternEndDate).format('YYYY-MM-DD')}T${moment(event.PatternStartDate).format('HH:mm:ss')}`
        console.log("date", date)
        nextMeetingDate = moment(date).utcOffset(checkMeeting.TimeZoneOffset).format()
      }
      nextMeetingDate = (new Date(nextMeetingDate) <= new Date(checkMeeting.PatternEndDate)) ? nextMeetingDate: null;
      console.log("nextMeetingDate", nextMeetingDate, isMeetingCreate);
      updateMeetings = await query(`UPDATE recurring_meetings SET start_date = '${checkMeeting.PatternStartDate}', end_date = '${checkMeeting.PatternEndDate}', subject = '${checkMeeting.subject}', 
      TimeZoneName='${checkMeeting.TimeZoneName}', TimeZoneOffset='${checkMeeting.TimeZoneOffset}',repeat_every = ${checkMeeting.Interval},frequency_type = '${checkMeeting.RecurrenceType}',weekdays_selected = '${checkMeeting.days_of_week}',
      location='${checkMeeting.location}', is_all_day='${checkMeeting.is_all_day}', next_meeting_date = ?  WHERE outlook_meeting_id = '${event.OutlookID}'`,[nextMeetingDate]);
    } else{
      updateMeetings = await query(`UPDATE recurring_meetings SET start_date = '${checkMeeting.PatternStartDate}', end_date = '${checkMeeting.PatternEndDate}', subject = '${checkMeeting.subject}', 
      location='${checkMeeting.location}', TimeZoneName='${checkMeeting.TimeZoneName}', TimeZoneOffset='${checkMeeting.TimeZoneOffset}' WHERE outlook_meeting_id = '${event.OutlookID}' `);
    }

    if (checkMeeting.inviteesChange){
      await query(`DELETE FROM recurring_meeting_invitees WHERE recurring_meetings_id = '${meetingData[0].recurring_meeting_id}' `);
      if (checkMeeting.invitees.length > 0){
        let invitees = createRecurringInvites(checkMeeting.invitees, meetingData[0].recurring_meeting_id);
        let sql1 = `INSERT INTO recurring_meeting_invitees(recurring_meetings_id, first_name, last_name, email_id,title,company) VALUES ?`;
        await query(sql1, [invitees]);
      }
    }

    console.log("updated success", updateMeetings);
  }

  let today = moment().utcOffset(event.TimeZoneOffset).format('YYYY-MM-DD')
  await query(`delete FROM recurring_meeting_activities WHERE outlook_meeting_id = '${event.OutlookID}'`);
  if (event.SubAppointmentsException.length && Array.isArray(meetingData) && meetingData.length > 0) {
    //  console.log("isMain false loop ");
    for (let each of event.SubAppointmentsException) {
      let start_format = moment(each.start).utcOffset(event.TimeZoneOffset).format('YYYY-MM-DD');
      let original_format = moment(each.OriginalDate).utcOffset(event.TimeZoneOffset).format('YYYY-MM-DD');
      if (start_format === today || original_format === today) {
        newMeetingData = {
          start_date: each.start,
          end_date: each.end,
          OriginalDate: each.OriginalDate,
          outlook_meeting_id :event.OutlookID,
          invites: each.invites ? each.invites : [],
          subject: each.subject,
          TimeZoneOffset: each.TimeZoneOffset,
          TimeZoneName: each.TimeZoneName,
          IsCancelled : each.IsCancelled,
          location: each.location ? each.location : null,
          is_all_day: each.AllDay == 1 ? 'Y' : 'N',
        }
      };
      // let activityData = await query(`SELECT * FROM recurring_meeting_activities WHERE outlook_meeting_id = '${each.OutlookID}' AND DATE_FORMAT(meeting_date, '%Y-%m-%d')='${original_format}' `);
      let is_deleted = each.IsCancelled ? 1: 0;
      let invitesArray = each.invites ? JSON.stringify(each.invites) : JSON.stringify([]);
      // if (Array.isArray(activityData) && activityData.length > 0){
      //   await query(`UPDATE recurring_meeting_activities SET meeting_date = '${each.OriginalDate}', meeting_start = '${each.start}', meeting_end = '${each.end}',  invitees = '${invitesArray}', is_deleted = '${is_deleted}'
      //  WHERE outlook_meeting_id = '${event.OutlookID}' AND DATE_FORMAT(meeting_date, '%Y-%m-%d')='${original_format}' `);
      // } else {
      let activities= [
        meetingData[0].recurring_meeting_id,
        event.OutlookID,
        each.OriginalDate,
        each.start,
        each.end,
        invitesArray,
        is_deleted
      ];
      console.log("activities", activities)
      let sql2 = `INSERT INTO recurring_meeting_activities (recurring_meetings_id, outlook_meeting_id, meeting_date , meeting_start, meeting_end,  invitees, is_deleted) VALUES (?,?,?,?,?,?,?)`;
      await query(sql2, activities);
     // }
    }
  }
  let todaysMeeting = await query(`SELECT *,M.id as m_id, IF(SUBSTRING(M.TimeZoneOffset, 1, 1) = '+', ADDTIME(M.start_date, SUBSTRING(M.TimeZoneOffset,2)), SUBTIME(M.start_date, SUBSTRING(M.TimeZoneOffset,2))) AS M_START 
  from meetings as M JOIN meeting_invites as MI on M.id = MI.meeting_id HAVING M.outlook_meeting_id='${event.OutlookID}' AND DATE_FORMAT(M_START, '%Y-%m-%d') = '${today}'`);
  console.log('todaysMeeting', todaysMeeting);
  console.log('newMeetingData', newMeetingData);
  let emailSendStatus = true;
  todaysMeeting.map(eachMeeting => {
    if(eachMeeting.emailsent_date != null) emailSendStatus = false;
  })
  if (Object.keys(newMeetingData).length > 0 && emailSendStatus) {
    if (isMeetingCreate) {
      let startTime = ((newMeetingData.start_date).split("T"))[1];
      let endTime = ((newMeetingData.end_date).split("T"))[1];
      newMeetingData.start_date = `${today}T${startTime}`;
      newMeetingData.end_date = `${today}T${endTime}`;
    }
    let userDetail = await query(`SELECT users.id, u1.status as user_status, u1.trialEnd, users.isMailVerified, users.initiativeId, settings.id as setting_id, users.orginasation_id,
    settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
    FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
    LEFT JOIN users u1 on users.orginasation_id = u1.id 
    LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
    WHERE
    LOWER(users.email) = '${event.organiser.email.toLocaleLowerCase()}'`);
    console.log("aaaaaaaaaaa")
    if (Array.isArray(todaysMeeting) && todaysMeeting.length > 0) {
      console.log("bbbbbbbbbbbbbbbbb")
      let originalLocalDate = moment(newMeetingData.OriginalDate).utcOffset(event.TimeZoneOffset).format('YYYY-MM-DD')
      let startLocalDate = moment(newMeetingData.start_date).utcOffset(event.TimeZoneOffset).format('YYYY-MM-DD')
      if ((originalLocalDate === today && startLocalDate !== today) 
       || newMeetingData.IsCancelled || newMeetingData.invites.length <=0 
       || (new Date(moment().endOf('day')) < new Date(event.PatternStartDate) && startLocalDate !== today)
       || (new Date() >= new Date(event.PatternEndDate) && startLocalDate !== today) 
       || new Date() >= new Date(newMeetingData.start_date)) {
        console.log("ccccccccccccc")
        await cancelMeeting(todaysMeeting[0].m_id, todaysMeeting[0].arnName);
      } else {
        console.log("dddddddddddddddd")
        await updateMainMeeting(userDetail, todaysMeeting, newMeetingData, today)
      }
    } else {
      console.log("eeeeeeeeeeeeeeee") 
      if (isMeetingCreate || (((newMeetingData.start_date).split("T"))[0] === today && new Date() <= new Date(newMeetingData.start_date))) {
        console.log("vvvvvvvvvvvv")
        await createMeetingFun(newMeetingData, newMeetingData.invites, userDetail);
      }
    }
  }

  let responseBody = { "code": 0, "message": "Test Message.", "Meeting": event };
  return responseBody;
};

exports.deleteRecurring = async (event) => {
  try {
	event = event.body;
	console.log("event", event);
	let rmID = await query(`select recurring_meeting_id from recurring_meetings where outlook_meeting_id = '${event.OutlookID}' `);
	if (rmID[0]) {

	  let deletRM = await query(`delete from recurring_meetings where recurring_meeting_id='${rmID[0].recurring_meeting_id}'`);
	  let deletRMInvites = await query(`delete from recurring_meeting_invitees where recurring_meetings_id	='${rmID[0].recurring_meeting_id}'`);
	  let deletRMActivites = await query(`delete from recurring_meeting_activities where recurring_meetings_id	='${rmID[0].recurring_meeting_id}'`);
	}
  let resp2 = await query(`select *, IF(SUBSTRING(TimeZoneOffset, 1, 1) = '+', ADDTIME(start_date, SUBSTRING(TimeZoneOffset,2)), SUBTIME(start_date, SUBSTRING(TimeZoneOffset,2))) AS M_START
  from meetings HAVING outlook_meeting_id = '${event.OutlookID}' AND date(M_START) = CURDATE()`);
  console.log("resp2", resp2)
  if (resp2[0]) {
	  let cancelMeeting1 = await cancelMeeting(resp2[0].id, resp2[0].arnName);
	} else {
	  console.log("resp2 is null");
	}
	return {
	  "code": 0,
	  "message": `The Meeting has been Cancelled.`,
	};
  }
  catch (error) {
    console.log("err", error);
  }
};
//get next meeting

async function setNextMeetingDate(calendarResource, timeZoneOffset) {
try {
    let isMeetingCreate = calendarResource.isMeetingCreate ? true : false;
    var resultNM;
    let recurrenceType = calendarResource.RecurrenceType;
    let startDay = calendarResource.PatternStartDate;
    let endDay = calendarResource.PatternEndDate;
    let noOfOccurrences = calendarResource.Occurrences;

    let interval = calendarResource.Interval || 1;
    let nextMeetingDate = new Date(startDay);
    let index = calendarResource.Instance || 1;
    switch (index) {
        case 1:
            index = "first";
            break;
        case 2:
            index = "second";
            break;
        case 3:
            index = "third";
            break;
        case 4:
            index = "fourth";
            break;
        case 5:
            index = "last";
            break;
        default:
            break;
    };

  if (recurrenceType == "daily") {
        nextMeetingDate.setDate(nextMeetingDate.getDate() + interval);
        ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
    } else if (recurrenceType == "weekly") {
        let weekdaysSelected = calendarResource['DaysOfWeek'];
        if (weekdaysSelected.length == 1) {
            nextMeetingDate.setDate(nextMeetingDate.getDate() + 7 * interval);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
        } else {
            let meetingDay = WEEK_DAYS[nextMeetingDate.getDay()];
            console.log(`--------meeting day : ${meetingDay}-----------------`);
            if (weekdaysSelected.indexOf(meetingDay) == (weekdaysSelected.length - 1)) {
                //Set next meeting date after weeks from 1st day in array.
                let gapBwFirstnLastMeetingInWeek = WEEK_DAYS.indexOf(meetingDay) - WEEK_DAYS.indexOf(weekdaysSelected[0])
                let repeatMeetingAfter = (interval - 1) * 7 + (7 - gapBwFirstnLastMeetingInWeek);
                nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
            } else {
                //Set next meeting day for next day in weekDaysSelected array.
                console.log("------Weekdays selected are : ------", weekdaysSelected);
                let meetingDayIndex = weekdaysSelected.indexOf(meetingDay); //This will give index of day in weekDaysSelected array in DB

                //Following code will give the week day number (b)
                let meetingWeekDay = WEEK_DAYS.indexOf(meetingDay);
                let nextMeetingWeekDay = WEEK_DAYS.indexOf(weekdaysSelected[meetingDayIndex + 1]);
                let repeatMeetingAfter;
                (nextMeetingWeekDay < meetingWeekDay) ?
                    (repeatMeetingAfter = 7 - meetingWeekDay + nextMeetingWeekDay) : (repeatMeetingAfter = nextMeetingWeekDay - meetingWeekDay);

                console.log(`--------nextMeetingWeekDay : ${nextMeetingWeekDay} repeatMeetingAfter : ${repeatMeetingAfter} -----------------`);
                nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
            }
        }
    } else if (recurrenceType == "absoluteMonthly") {
        nextMeetingDate.setMonth(nextMeetingDate.getMonth() + interval);
        ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));

    } else if (recurrenceType == "relativeMonthly") {
        let dayIndex = nextMeetingDate.getDay();
        let monthIndex = nextMeetingDate.getMonth();
        let nextMonthIndex = monthIndex + interval;

        let firstDayOfNextMonth = new Date(nextMeetingDate.getFullYear(), nextMonthIndex, 1);
        firstDayOfNextMonth.setHours(nextMeetingDate.getHours());
        firstDayOfNextMonth.setMinutes(nextMeetingDate.getMinutes());
        // console.log("FIrst day of next month ", firstDayOfNextMonth);

        if (index == "first") {
            if (dayIndex == firstDayOfNextMonth.getDay()) {
                // return firstDayOfNextMonth;
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
            } else {
                while (firstDayOfNextMonth.getDay() !== dayIndex) {
                    firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
                }
                // return firstDayOfNextMonth;
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
            }
        }
        if (index == "second") {
            while (firstDayOfNextMonth.getDay() !== dayIndex) {
                firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
            }
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7)
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "third") {
            while (firstDayOfNextMonth.getDay() !== dayIndex) {
                firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
            }
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 2);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "fourth") {
            while (firstDayOfNextMonth.getDay() !== dayIndex) {
                firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
            }
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 3);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "last") {
            let lastDayOfNextMonth = new Date(firstDayOfNextMonth.getFullYear(), firstDayOfNextMonth.getMonth() + 1, 0, firstDayOfNextMonth.getHours(), firstDayOfNextMonth.getMinutes(), firstDayOfNextMonth.getSeconds());
            // console.log("Last Day of Next Month is : ", lastDayOfNextMonth);
            while (lastDayOfNextMonth.getDay() !== dayIndex) {
                lastDayOfNextMonth.setDate(lastDayOfNextMonth.getDate() - 1);
            }
            // return lastDayOfNextMonth;
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(lastDayOfNextMonth.getTime(), timeZoneOffset, calendarResource));
        }
    } else if (recurrenceType == "absoluteYearly") {
        nextMeetingDate.setFullYear(nextMeetingDate.getFullYear() + interval);
        ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
    } else if (recurrenceType == "relativeYearly") {
        let dayIndex = nextMeetingDate.getDay();
        let firsDayOfSameMonthNextYear = new Date(nextMeetingDate.getFullYear() + 1, nextMeetingDate.getMonth(), 1);
        firsDayOfSameMonthNextYear.setHours(nextMeetingDate.getHours());
        firsDayOfSameMonthNextYear.setMinutes(nextMeetingDate.getMinutes());
        firsDayOfSameMonthNextYear.setSeconds(nextMeetingDate.getSeconds());

        if (index == "first") {
            if (dayIndex == firsDayOfSameMonthNextYear.getDay()) {
                // return firsDayOfSameMonthNextYear;
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
            } else {
                while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
                    firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
                }
                // return firsDayOfSameMonthNextYear;
                ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
            }
        }
        if (index == "second") {
            while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
                firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
            }
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "third") {
            while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
                firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
            }
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 2);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "fourth") {
            while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
                firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
            }
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 3);
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
        }
        if (index == "last") {
            let lastDayOfSameMonthNextYear = new Date(firsDayOfSameMonthNextYear.getFullYear(), firsDayOfSameMonthNextYear.getMonth() + 1, 0, firsDayOfSameMonthNextYear.getHours(), firsDayOfSameMonthNextYear.getMinutes(), firsDayOfSameMonthNextYear.getSeconds());
            // console.log("Last Day of sam Month next year is : ", lastDayOfSameMonthNextYear);
            while (lastDayOfSameMonthNextYear.getDay() !== dayIndex) {
                lastDayOfSameMonthNextYear.setDate(lastDayOfSameMonthNextYear.getDate() - 1);
            }
            lastDayOfSameMonthNextYear;
            ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(lastDayOfSameMonthNextYear.getTime(), timeZoneOffset, calendarResource));
        }
    } else {
        nextMeetingDate.setDate(nextMeetingDate.getDate() + interval);
        ({resultNM, isMeetingCreate} = await calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset, calendarResource));
    }
    return ({nextMeetingDate: resultNM, isMeetingCreate});
} catch (error) {
    console.log("error", error)
}
}

async function calculateDateTimeWithOffset(nextMeetingDateTime, timeZoneOffset, calendarResource) {
    if (isNaN(nextMeetingDateTime)) {
        return ({resultNM: null, isMeetingCreate: calendarResource.isMeetingCreate});
    }
    var nextMeetingDateTime1 = moment(nextMeetingDateTime).utcOffset(timeZoneOffset).format();
    console.log("nextMeetingDateTime1", nextMeetingDateTime1)
    if (new Date(calendarResource.PatternEndDate) < new Date(nextMeetingDateTime1)) {
      return ({resultNM: new Date(calendarResource.PatternStartDate), isMeetingCreate: calendarResource.isMeetingCreate});
    }
    if ((moment().utcOffset(timeZoneOffset)).isBefore(nextMeetingDateTime1)) {
      return ({resultNM: nextMeetingDateTime1, isMeetingCreate: calendarResource.isMeetingCreate});
    }
    else {
        console.log("calling recursion", new Date(nextMeetingDateTime))
        let eventDate = moment(nextMeetingDateTime).utcOffset(timeZoneOffset);
        let todayDate = moment().utcOffset(timeZoneOffset);
        if (eventDate.format('YYYY-MM-DD') === todayDate.format('YYYY-MM-DD') &&
        todayDate.isBefore(eventDate)) {
          calendarResource.isMeetingCreate = true;
        }
        return ({resultNM: nextMeetingDateTime1, isMeetingCreate: calendarResource.isMeetingCreate});
    }
}
// To get dates 
async function getDates(dateVal) {
  let date = dateVal.getFullYear() + '-' + (dateVal.getMonth() + 1) + '-' + dateVal.getDate();
  let time = dateVal.getHours() + ":" + dateVal.getMinutes() + ":" + dateVal.getSeconds();
  let datetime = date + ' ' + time;

  return datetime;
}

// common function to check the meeting starts here
async function checkMeetings(incomingMeetingData, meetingData) {
  console.log(JSON.stringify(meetingData))
  // let startTime = ((incomingMeetingData.start).split("T"))[1];
  // let endTime = ((incomingMeetingData.end).split("T"))[1];
  // let patternStartDate = moment(incomingMeetingData.PatternStartDate).format('YYYY-MM-DD')
  // let patternEndDate = moment(incomingMeetingData.PatternEndDate).format('YYYY-MM-DD')

  if (meetingData[0] && meetingData[0].outlook_meeting_id) {
    let result = {
      "message": true,
      "invitees": [],
      "inviteesChange": false,
      "user_id": meetingData[0].user_id,
      "subject": incomingMeetingData.subject,
      // "PatternStartDate": `${patternStartDate}T${startTime}`,
      // "PatternEndDate": `${patternEndDate}T${endTime}`,
      "PatternStartDate": incomingMeetingData.PatternStartDate,
      "PatternEndDate": incomingMeetingData.PatternEndDate,
      "TimeZoneOffset": incomingMeetingData.TimeZoneOffset,
      "TimeZoneName": incomingMeetingData.TimeZoneName,
      "location": incomingMeetingData.location,
      "id": meetingData[0].outlook_meeting_id,
      "recurrenceChange": false
    };
    let is_all_day = incomingMeetingData.AllDay == '1' ? 'Y' : 'N';
    if (meetingData[0].frequency_type != incomingMeetingData.RecurrenceType || 
      meetingData[0].repeat_every != incomingMeetingData.Interval ||
      meetingData[0].weekdays_selected != incomingMeetingData.DaysOfWeek.toString() ||
      meetingData[0].is_all_day != is_all_day ||
      moment(result.PatternStartDate).format() != moment(meetingData[0].start_date).format() ||
      moment(result.PatternEndDate).format() != moment(meetingData[0].end_date).format()){
      result.recurrenceChange = true;
      result.RecurrenceType = incomingMeetingData.RecurrenceType;
      result.days_of_week = incomingMeetingData.DaysOfWeek.toString();
      result.DaysOfWeek = incomingMeetingData.DaysOfWeek;
      result.Interval = incomingMeetingData.Interval;
      result.Instance = incomingMeetingData.Instance;
      result.is_all_day = is_all_day;
    }
    
    /*------------------------------------
        Meeting Invitees Checking Start   
    --------------------------------------*/
    let incomingInviteeList = [];
    let incomingInviteeFullDetailed = [];
    console.log("incomingMeetingData.attendees_list", incomingMeetingData.invites);
    console.log("incomingMeetingData attendees length ", incomingMeetingData.invites.length);
    for (let j = 0; j < incomingMeetingData.invites.length; j++) {
      incomingInviteeList.push(incomingMeetingData.invites[j].email);
      incomingInviteeFullDetailed.push({
        email: incomingMeetingData.invites[j].email,
        firstname: incomingMeetingData.invites[j].firstname,
        lastname: incomingMeetingData.invites[j].lastname, 
        title: incomingMeetingData.invites[j].title,
        company: incomingMeetingData.invites[j].company,
      });
    }

    let inviteesList = [];
    let inviteeCheck = [];
    for (let i = 0; i < meetingData.length; i++) {
      inviteesList.push(meetingData[i].email_id);
      if (!incomingInviteeList.includes(meetingData[i].email_id)) {
        inviteeCheck.push(meetingData[i].email_id);
      }
    }

    if (inviteeCheck.length == 0) {
      inviteeCheck = incomingInviteeList.filter(function (item) {
        console.log("inviteeCheck.length = 0", inviteeCheck);
        return !inviteesList.includes(item);
      });
    }
    console.log("inviteeCheck", JSON.stringify(inviteeCheck));
    
    if (inviteeCheck.length != 0) {
      // console.log("inviteeCheck.length != 0",inviteeCheck);
      result.inviteesChange = true;
      result.invitees = incomingInviteeFullDetailed;
      return ( result );
    }

    /*------------------------------------
      Meeting Details Checking Start
    --------------------------------------*/
    // subject: calendarResource.subject,
    // let start_date = new Date(meetingData[0].start_date).getTime();
    // let end_date = new Date(meetingData[0].end_date).getTime();
    // let incomingStart_date = new Date(result.PatternStartDate).getTime();
    // let incomingEnd_date = new Date(result.PatternEndDate).getTime();
    // console.log(`start_date,end_date, incomingStart_date, incomingEnd_date`);
    // console.log(start_date, end_date, incomingStart_date, incomingEnd_date);

    if (result.recurrenceChange) {
      return (result);
    }
  }
  return ({"message": false});
}
// common function to check the meeting Ends here

// get time zone from DB starts here
async function getTimeZoneOffsetValue(timeZoneName) {
  let sql = `select time_zone_offset from time_zone_details where time_zone_name = ?`;
  let timeZoneOffset = await query(sql, timeZoneName);
  if (timeZoneOffset[0]) {
    return timeZoneOffset[0].time_zone_offset
  }
  else {
    return "+0";
  }
}
// get time zone from DB ends here

// common function for time start here
function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  var dif = t1.getTime() - t2.getTime();
  var Seconds_from_T1_to_T2 = dif / 1000;

  return Seconds_from_T1_to_T2;
}
// common function for time ends here

// stepfunction starts here
async function createStepFunction(invites, delay, setting, id) {

  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    const stateMachineArn = env.AWS_STEP;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
        "delay_seconds": Math.round(delay),
        "obj": invites,
        "id": id,
        "setting": setting
      }),
    }).promise();
    return newstateMachineArn;
  }
  catch (error) {
    throw error;
  }
}
// stepfunction ends here

//insert or create meeting start here
async function createMeetingFun(meetingData, meetingInvitees, userDetail) {
  try {
    if (!Array.isArray(meetingInvitees) || meetingInvitees.length <= 0) return null;
    let meetingStart = new Date(meetingData.start_date);
    let addHourinToSecond = userDetail[0].hour * 60 * 60;
    let addMinuteinToSecond = userDetail[0].minute * 60;
    let mailTimes;

    if (userDetail[0].emailSend == 'After') {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), meetingData.end_date);
      mailTimes = mailTimes + totalSeconds;
    } else {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), meetingData.start_date);
      mailTimes = mailTimes - totalSeconds;
    }
    console.log("after delay calc", mailTimes);

    let isCountable = false;

    const firstDate = new Date().setHours(12, 0, 0, 0);
    const secondDate = new Date(userDetail[0].trialEnd);
    let user_status = userDetail[0].user_status;
    /* Check-1 Change status to active if the user is inside trial period */
    if (secondDate > firstDate) {
      user_status = 'active'
    }

    /* Check-2 Checking IP status and enabling countable flag */
    if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive') {
      isCountable = true
    }

    /* Check-3 If the meeting time is after trial end, check whether subscription exists, to count it as SM */
    if (meetingStart > userDetail[0].trialEnd) {
      console.log("meeting after trial period");
      /* If no active subscription is present */
      if (!userDetail[0].subscriptionId) {
        // message = " No Active Subscription"
        console.log("No subscription");
        /* This meeting is not a Sustainable Meeting. So change countable to false */
        isCountable = false;
        // return;
      }
    }
    /* Check-4 Check if the IP is verified */
    if (userDetail[0].isMailVerified == 0) {
      /* Not an SM */
      console.log("IP's email is not verified yet");
      isCountable = false;
      // message = "IP's email is not verified yet "
    }

    // console.log(arn)
    let meeting_info_array = [userDetail[0].id,"outlookDesktop", userDetail[0].initiativeId, new Date(), new Date(), userDetail[0].orginasation_id,
    meetingData.subject, meetingData.location, "recurring", meetingData.start_date, meetingData.end_date, meetingData.is_all_day, 
    meetingData.TimeZoneOffset, meetingData.TimeZoneName, isCountable, meetingData.outlook_meeting_id, meetingData.outlook_meeting_id];
    let meeting_info_query = `INSERT INTO meetings(user_id,calendar_type,initiativeId,createdAt,updatedAt,org_id,subject,location,type,
            start_date,end_date,all_Day,TimeZoneOffset,TimeZoneName,countable,outlook_meeting_id,rm_id) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    let resp = await query(meeting_info_query, meeting_info_array)
    console.log(meetingInvitees)
    let inviters = createInvites(meetingInvitees, resp.insertId)


    console.log("=================");
    console.log(userDetail[0].user_status);
    console.log("=================");

    //add meeting creat related data to userDetails
    userDetail[0].startTime = meetingData.start_date
    userDetail[0].TimeZoneOffset = meetingData.TimeZoneOffset
    userDetail[0].TimeZoneName = meetingData.TimeZoneName


    if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive' && isCountable == true) {
      let arn = await createStepFunction(meetingInvitees, mailTimes, userDetail[0], resp.insertId)
      let newstateMachineArnUpdate = await query(`update meetings SET arnName='${arn.executionArn}'  
                      where id=${resp.insertId}`)
    }

    let resp2 = await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company) VALUES ?`, [inviters])
    return resp.insertId;
  }
  catch (err) {
    console.log(err);
  }
}
//insert or create meeting ends here

// delete meetings and ARN 
async function cancelMeeting(mId,mArn){
  try {
    console.log("mArn", mId, mArn)
    let meetingData = await query(`select *,M.id as m_id, IF(SUBSTRING(M.TimeZoneOffset, 1, 1) = '+', ADDTIME(M.start_date, SUBSTRING(M.TimeZoneOffset,2)), SUBTIME(M.start_date, SUBSTRING(M.TimeZoneOffset,2))) AS M_START
     from meetings as M JOIN meeting_invites as MI on M.id = MI.meeting_id HAVING M.id = '${mId}' AND date(M_START) = CURDATE()`);
    let emailSendStatus = true;
    meetingData = Array.isArray(meetingData) ? meetingData : [];
    meetingData.map(eachMeeting => {
      if(eachMeeting.emailsent_date != null) emailSendStatus = false;
    })
    console.log("emailSendStatus", emailSendStatus)
    if (emailSendStatus && isValidate(mId) == true && isValidate(mArn) == true) {
      console.log("testttttttttt", mArn)
      // delet meetings table entry
      let delet = await query(`delete from meetings where id='${mId}'`);
      let deletInvites = await query(`delete from meeting_invites where meeting_id='${mId}'`);
      //stop ARN
      const stateMachineArn = mArn;
      var params = {
        executionArn: stateMachineArn /* required */
      };
      if (stateMachineArn) {
        const result = await stepfunctions.stopExecution(params).promise();
        console.log(`State machine ${stateMachineArn} executed successfully`, result);
      }
      return {
        "code": 0,
        "message": `Deleted meeting table entry and stopped step function`,
      };
    }
    return;
  } catch (error) {
    console.log("err", error);
  }
}

//invites array format start here
function createInvites(obj, meeting_id) {
  let invites = []
  for (let i = 0; i < obj.length; i++) {
    let inviteIndiividual = [];
    inviteIndiividual.push(obj[i].firstname)
    inviteIndiividual.push(obj[i].lastname)
    inviteIndiividual.push(meeting_id)
    inviteIndiividual.push(obj[i].email)
    inviteIndiividual.push(obj[i].title || " ")
    inviteIndiividual.push(obj[i].company)
    invites.push(inviteIndiividual);
  }

  //   console.log(obj.invites);
  return invites;
}

function createRecurringInvites(obj, meeting_id) {
  let invites = []
  for (let i = 0; i < obj.length; i++) {
    let inviteIndiividual = [];
    inviteIndiividual.push(meeting_id)
    inviteIndiividual.push(obj[i].firstname)
    inviteIndiividual.push(obj[i].lastname)
    inviteIndiividual.push(obj[i].email)
    inviteIndiividual.push(obj[i].title || " ");
    inviteIndiividual.push(obj[i].company)
    invites.push(inviteIndiividual);
  }

  //   console.log(obj.invites);
  return invites;
}
//invites array format ends here
// input validating function
function isValidate(input) {
  console.log("input ", input)
  if (input == null) {
    return false;
  }
  else if (input == "null") {
    return false;
  }
  else if (input == "") {
    return false;
  }
  else if (input == undefined) {
    return false;
  }
  else if (input == []) {
    return false;
  }
  else {
    return true;
  }
}

async function updateMainMeeting(userDetail, todaysMeeting, newMeetingData, today) {
  let resp2 = await query(`select *,M.id as m_id from meetings as M LEFT JOIN meeting_invites as MI on M.id = MI.meeting_id
    where M.outlook_meeting_id = '${newMeetingData.outlook_meeting_id}' AND date(M.start_date) = CURDATE()`);
  let emailSendStatus = true;
  resp2 = Array.isArray(resp2) ? resp2 : [];
  resp2.map(eachMeeting => {
    if(eachMeeting.emailsent_date != null) emailSendStatus = false;
  })
  if(!emailSendStatus) {
    return;
  }
  const executableStateMachineArn = todaysMeeting[0].arnName;
  var params = {
    executionArn: executableStateMachineArn /* required */
  };
  if (executableStateMachineArn) {
    await stepfunctions.stopExecution(params).promise();
  }
  let all_Day = newMeetingData.is_all_day == 'Y' ? 1 : 0;
  await query(`UPDATE meetings SET subject='${newMeetingData.subject}',start_date='${newMeetingData.start_date}',end_date='${newMeetingData.end_date}',
  TimeZoneOffset='${newMeetingData.TimeZoneOffset}',TimeZoneName='${newMeetingData.TimeZoneName}', location='${newMeetingData.location}', all_Day='${all_Day}'
  WHERE outlook_meeting_id = '${newMeetingData.outlook_meeting_id}' AND DATE_FORMAT(start_date, '%Y-%m-%d') = '${today}' `);
  await query(`delete  from meeting_invites where meeting_id=${todaysMeeting[0].m_id}`);
  let inviters = createInvites(newMeetingData.invites, todaysMeeting[0].m_id)
  await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company) VALUES ?`, [inviters])

  let meetingStart = new Date(newMeetingData.start_date);
  let addHourinToSecond = userDetail[0].hour * 60 * 60;
  let addMinuteinToSecond = userDetail[0].minute * 60;
  let mailTimes;

  if (userDetail[0].emailSend == 'After') {
    let totalSeconds = addHourinToSecond + addMinuteinToSecond;
    mailTimes = findSeconds(new Date().toISOString(), newMeetingData.end_date);
    mailTimes = mailTimes + totalSeconds;
  } else {
    let totalSeconds = addHourinToSecond + addMinuteinToSecond;
    mailTimes = findSeconds(new Date().toISOString(), newMeetingData.start_date);
    mailTimes = mailTimes - totalSeconds;
  }
  console.log("after delay calc", mailTimes);

  let isCountable = false;

  const firstDate = new Date().setHours(12, 0, 0, 0);
  const secondDate = new Date(userDetail[0].trialEnd);
  let user_status = userDetail[0].user_status;
  if (secondDate > firstDate) {
    user_status = 'active'
  }

  if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive') {
    isCountable = true
  }

  if (meetingStart > userDetail[0].trialEnd) {
    if (!userDetail[0].subscriptionId) {
      isCountable = false;
    }
  }
  
  if (userDetail[0].isMailVerified == 0) {
    console.log("IP's email is not verified yet");
    isCountable = false;
  }

  userDetail[0].startTime = newMeetingData.start_date
  userDetail[0].TimeZoneOffset = newMeetingData.TimeZoneOffset
  userDetail[0].TimeZoneName = newMeetingData.TimeZoneName

  if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive' && isCountable == true) {
    let arn = await createStepFunction(newMeetingData.invites, mailTimes, userDetail[0], todaysMeeting[0].m_id)
    console.log(arn)
    await query(`update meetings SET arnName='${arn.executionArn}' where id=${todaysMeeting[0].m_id}`)
  }
  console.log("The Invites has been Updated.") 
  return;
}

function isValidateInputs(input, keys) {
  let isValid = true;
  for (let key of keys) {
    let each = input[key.value] ? input[key.value] : null;
    let type = input[key.type] ? input[key.type] : "string";
    if (each === null || each === "" || !each) {
      isValid = false;
    } else if (type == "array" && !Array.isArray(each)) {
      isValid = false;
    } else if (type == "date" && (new Date(each) instanceof Date && isNaN(new Date(each).getTime()))) {
      isValid = false;
    } else {
      isValid = true;
    }
    if (!isValid) break;
  }
  return isValid;
}

function getRecurrenceType(recurrenceType) {
  let result = recurrenceType;
  switch (recurrenceType) {
    case "olRecursDaily":
      result = "daily";
      break;
    case "olRecursWeekly":
      result = "weekly";
      break;
    case "olRecursMonthly":
      result = "absoluteMonthly";
      break;
    case "olRecursMonthNth":
      result = "relativeMonthly";
      break;
    case "olRecursYearly":
      result = "absoluteYearly";
      break;
    case "olRecursYearNth":
      result = "relativeYearly";
      break;
    default:
      break;
  }
  return result;
}

async function calculateStartEnd(todayMeetingData) {
	let today = moment().format('YYYY-MM-DD');
	let nextFormat = moment(todayMeetingData.next_meeting_date).format('YYYY-MM-DDTHH:mm:ss')
	let offset = todayMeetingData.TimeZoneOffset.substr(0, 6);
	let meetingStart = moment(`${nextFormat}${offset}`).format();
	let meetingStartFormat = moment(meetingStart).format('YYYY-MM-DD');
	let todayStart = moment(today + ' ' + todayMeetingData.start).format();
	let todayEnd = moment(today + ' ' + todayMeetingData.end).format();
	let meetingEnd = moment(meetingStartFormat + ' ' + todayMeetingData.end).format();
  if (new Date(todayEnd) <= new Date(todayStart)) {
    meetingEnd = (moment(meetingEnd).add(1, 'days')).format()
  }
  console.log("testttttttttttt inside", `${nextFormat}${offset}`, meetingStart, meetingEnd,todayMeetingData.subject)
  return ({meetingStart, meetingEnd});
}