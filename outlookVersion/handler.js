'use strict';

module.exports.outlookVersion = async event => {
  // return {
  //   statusCode: 200,
  //   body: JSON.stringify(
  //     { "code": 0, "message": "This Version Define before.", "version": { "Id": "12", "Outlook": "Microsoft Outlook 16.0.0.11328", "Version": "11328.20438", "Build": "16.0.11328.20438", "IsEnable": true } }
  //   ),
  // };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  return {
    "code": 0, "message": "This Version Define before.", "version": { "Id": "12", "Outlook": "Microsoft Outlook 16.0.0.11328", "Version": "11328.20438", "Build": "16.0.11328.20438", "IsEnable": true } };
}
