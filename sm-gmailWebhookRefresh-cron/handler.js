'use strict';

// include mysql module
const aws = require('aws-sdk');
const util = require('util');
const axios = require('axios');
const db = require('./config').pool;
const getConnection = util.promisify(db.getConnection).bind(db);
const { SQS: sqsURL } = require('./config');
const sqs = new aws.SQS({ apiVersion: '2012-11-05' });
const sendMessage = util.promisify(sqs.sendMessage).bind(sqs);

module.exports.gmailWebhookRefresh = async event => {
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  console.log('connected as id ' + connection.threadId);
  let sql = `SELECT * FROM google_user_tokens WHERE DATE_FORMAT(expirationTime, '%Y-%m-%d') = CURDATE()`;
  let result = await query(sql);

  for (let i = 0; i < result.length; i++) {
    let queueData = {
      email: result[i].email,
      accessToken: result[i].access_token,
      refreshToken: result[i].refresh_token,
      webhookId: result[i].webhookId,
    };
    await sendSQS(queueData)
  }
  connection.release();
  return true;
}

async function sendSQS(item) {
  try {
    console.log('Befor Push to SQS item', item);
    let body = JSON.stringify(item);
    console.log('Befor Push ** Body', body);
    var params = {
      MessageBody: body,
      QueueUrl: sqsURL,
    };
    console.log('param', params);
    let res = await sendMessage(params);
    console.log('Pushed **', item, res);
  } catch (err) {
    console.log('Error', err);
  }
}
