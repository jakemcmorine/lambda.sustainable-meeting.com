const Hubspot = require('hubspot');
const HUBSPOT_API_KEY = require('./config').HUBSPOT_API_KEY;

module.exports = new Hubspot({
  apiKey: HUBSPOT_API_KEY
});