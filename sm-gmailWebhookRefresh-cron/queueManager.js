const moment = require('moment');
const util = require('util');
const axios = require('axios');
const querystring = require('querystring');
const db = require('./config').pool;
const getConnection = util.promisify(db.getConnection).bind(db);
const { REDIRECT_URL: REDIRECT_URL, CLIENT_SECRET: CLIENT_SECRET, CLIENT_ID: CLIENT_ID } = require('./config');
const hubspot = require('./HubspotService');
const { v4: uuidv4 } = require('uuid');

module.exports.refreshSubscription = async event => {
    // return true;
    var connection = await getConnection();
    var email="";
    try {
        const query = util.promisify(connection.query).bind(connection);
        console.log('Event \n', event);
        for (let i = 0; i < event.Records.length; i++) {
            console.log('\n*** Body \n', event.Records[i].body);
            let userData = JSON.parse(event.Records[i].body);
            email = userData.email;
            await updateWebhookSubscription(userData, query);
        }
    } catch (error) {
        console.log("Error in Main Function");
        console.log(error);
        try {
            if (email){
                console.log(`email : ${email}`);
                const updatedHsContact = await hubspot.contacts.createOrUpdate(email, {
                    "properties": [
                        {
                            "property": "app_plugin_status",
                            "value": 'Inactive'
                        }
                    ]
                });
                const query = util.promisify(connection.query).bind(connection);
                const updateStatusResult = await query(`UPDATE users SET status = 'inactive' WHERE email ="${email}" AND id != orginasation_id`);
                console.log(updateStatusResult);
            }
        } catch (error) {
            console.log(error);
        }
    } finally {
        console.log("=========End");
        connection.release();
        return true;
    }
}

async function updateWebhookSubscription(userData, query, fromRefreshToken = false) {
    let currentDate = new Date();
    currentDate.setMinutes(currentDate.getMinutes() + 5);
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${userData.accessToken}`
    }
    let channelBody = {
        id: uuidv4(),
        type: 'web_hook',
        address: REDIRECT_URL,
        params: {
            "ttl": 7776000
        }
    }
    // let url = `https://www.googleapis.com/calendar/v3/calendars/${userData.email}/events/watch`;
    let url = `https://www.googleapis.com/calendar/v3/calendars/primary/events/watch`;

    try {
        let updateSubscription = await axios.post(url, channelBody, { headers });
        console.log("updateSubscription");
        console.log(updateSubscription);
        if(updateSubscription.data){
            let updatedResult = [
                updateSubscription.data.id, 
                userData.accessToken,
                new Date().toISOString(),
                new Date(parseInt(updateSubscription.data.expiration)).toISOString(),
                updateSubscription.data.resourceId,
                userData.email
            ];
            let updateQry = `UPDATE google_user_tokens SET webhookId=?,access_token=?,subscriptionTime=?, expirationTime=? , resourceId =? WHERE email = ?`;
            let result = await query(updateQry, updatedResult);
            console.log(result);
        }
    } catch (error) {
        console.log("updateWebhookSubscription error");
        console.log(error);
        if (error.response.status === 401 && error.response.statusText === 'Unauthorized' && !fromRefreshToken) {
            await refreshAccessToken(userData, query);
        }else{
            throw error;
        }
    }finally{
        return true;
    }
}

const refreshAccessToken = async (userData, query) => {
    try {
        const res = await axios.post('https://oauth2.googleapis.com/token', {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            grant_type: 'refresh_token',
            refresh_token: userData.refreshToken
        });
        // let access_token = res.data.access_token;
        // return access_token;
        console.log(res.data);
        userData.accessToken = res.data.access_token;
        await updateWebhookSubscription(userData, query, true);
    } catch (error) {
        // console.log(error);
        throw error;
    }
}

// async function updateWebhookSubscription(userData, query, fromRefreshToken = false) {
//     let expiration = moment().add(43200, 'minutes');
//     let calendarSubscription = {
//         "id": uuidv4(),
//         "type": "web_hook",
//         "address": REDIRECT_URL,
//         "params": {
//             "ttl": expiration
//         }
//     };
//     var accessToken = userData.accessToken;
//     var refresh_token = userData.refreshToken;
//     var sub = userData.webhookId;

//     const updateSubscription = await axios.post('https://login.microsoftonline.com/common/oauth2/v2.0/token',
//         querystring.stringify(requestBody), axiosConfig);

//     let graph = Client.initWithMiddleware({
//         authProvider: {
//             getAccessToken: async () => accessToken
//         },
//     });
//     try {
//         let webhook = await graph.api('/subscriptions/' + sub)
//             .patch(calendarSubscription);
//         console.info('subscription updated successfully')
//         console.log(webhook);
//         let sql = `UPDATE user_tokens SET subscriptionTime = ? WHERE user_tokens.email = ?; `;
//         let result = await query(sql, [moment().format("YYYY/MM/DD HH:mm:ss"), userData.email]);
//         console.log(result)
//     } catch (error) {
//         console.error('failed to update subscription');
//         console.error(error);
//         if (error.code == 'InvalidAuthenticationToken') {
//             console.log("Token Expired");
//             if (!fromRefreshToken) { 
//                 await refreshToken(refresh_token, userData, query);
//             }else { 
//                 throw new Error(`Refresh Token Revoked`);
//             }
//         }else{
//             throw new Error(`Not able Refresh Subscription`);
//         }
//     }
//     console.log("=========End of updateWebhookSubscription function");
//     return true;
// }

// async function refreshToken(refresh_token, userData, query) {
//     const requestBody = {
//         client_id: "cec04b71-137b-4a99-80c6-e0fc88a2e7c5",
//         scope: [
//             'openid',
//             'offline_access',
//             'profile',
//             'OnlineMeetings.ReadWrite',
//             'Calendars.ReadWrite',
//             'People.Read.All'
//         ],
//         refresh_token: refresh_token,
//         redirect_uri: REDIRECT_URL,
//         grant_type: 'refresh_token',
//         client_secret: CLIENT_SECRET
//     };

//     const axiosConfig = {
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         }
//     }
//     const graphResponse = await axios.post('https://login.microsoftonline.com/common/oauth2/v2.0/token',
//         querystring.stringify(requestBody), axiosConfig);
//     // console.log(graphResponse);
    
//     if (graphResponse.status === 200) {
//         const tokenData = graphResponse.data;
//         let sql = `UPDATE user_tokens SET accessToken = ?, refreshToken = ? WHERE email = ?`;
//         let result = await query(sql, [tokenData.access_token, tokenData.refresh_token, userData.email]);
//         console.log(result);
//         userData.accessToken = tokenData.access_token;
//         await updateWebhookSubscription(userData, query, true);
//         // return tokenData.access_token;
//     } else {
//         throw new Error(`failed to refresh access token for ${email}`);
//     }
//     return true;
// }