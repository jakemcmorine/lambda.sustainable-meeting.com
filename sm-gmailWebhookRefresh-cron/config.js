const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/gmailWebhookRefresh-dev",
    REDIRECT_URL: "https://dev-google-addin-ss.emvigotechnologies.com/auth/webhook-subscription",
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4",
    CLIENT_SECRET: "us3TwNzT91x6BGudFIicnM6g",
    CLIENT_ID: "985040735401-p8eimmdbjr5fadjp1tsg7ghuq1kpndcp.apps.googleusercontent.com"
  },
  prod: {
    DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/gmailWebhookRefresh-prod",
    REDIRECT_URL: "https://google-addin-meetings.sustainably.run/auth/webhook-subscription",
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4",
    CLIENT_SECRET : "tlKuMMWdFhUO2Ua9EaY0xu6O",
    CLIENT_ID : "243817113489-qq4rl90p0fovi85ikf3l6he53d8qnu1c.apps.googleusercontent.com"
  },
};
let env = config[process.env.stage];
var pool = mysql.createPool({
  connectionLimit: 1000,
  connectTimeout: 60 * 60 * 1000,
  acquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
  debug: false,
});

module.exports.pool = pool;
module.exports.SQS = env.SQS;
module.exports.REDIRECT_URL = env.REDIRECT_URL;
module.exports.CLIENT_SECRET = env.CLIENT_SECRET;
module.exports.CLIENT_ID = env.CLIENT_ID;
module.exports.HUBSPOT_API_KEY = env.HUBSPOT_API_KEY;
