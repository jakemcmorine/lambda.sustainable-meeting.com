'use strict';

// include mysql module
const util = require('util');
const axios = require('axios');
var connection = require('./config').connection;
const {
  goCardlessURL,
  goCardlessToken
} = require('./config');


module.exports.currencyConversion = async event => {
  try {
    let currencyRates = [];
    let url = `${goCardlessURL}currency_exchange_rates?source=GBP`;
    let header = {
      headers: {
        "GoCardless-Version": "2015-07-06",
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + goCardlessToken
      }
    };
    await axios.get(url, header)
      .then(response => {
        console.log(' *** ^^^^ Go Cardless getUserCurrency response \n', response);
        currencyRates = response.data.currency_exchange_rates;
      })
      .catch(err => {
        console.log("Error in GoCardless API Call");
        console.log(err.response.data);
        console.log(err.response.data.error.errors);
        throw err;
      });
    await this.updateCurrencyConversion(currencyRates);
    await this.insertHistoricCurrencyConversion(currencyRates);
    await this.deleteHistoricCurrencyConversion(currencyRates);
    return true;
    
  } catch (error) {
    console.log("Error Message from GoCardless API Call:", error);
    throw error;
  }
    
};

module.exports.updateCurrencyConversion = async currencyRates => {
  try {
    const query = util.promisify(connection.query).bind(connection);
    let queryString = "";
    currencyRates.forEach(currency => {
        queryString = queryString + `("GBP", "${currency.target}", ${parseFloat(currency.rate)}, "${currency.time}"),`;
    });
    queryString = queryString.slice(0, -1);

    console.log("in updateCurrencyConversion queryString", queryString);
    let sqlquery = `INSERT INTO exchange_rates(source, target, rate, updated_at)
                        VALUES ${queryString}
                        ON DUPLICATE KEY UPDATE
                        target = VALUES(target),
                        rate = VALUES(rate),
                        updated_at = VALUES(updated_at)`;
    // console.log("query", query);
    let result = await query(sqlquery);
    console.log("result in update", result);
  }
  catch (error) {
    console.log("Error in updating exchange rates ", error);
    throw error;
  }
};

module.exports.insertHistoricCurrencyConversion = async currencyRates => {
  try {
    const query = util.promisify(connection.query).bind(connection);
    let queryString = "";
    currencyRates.forEach(currency => {
        queryString = queryString + `("GBP", "${currency.target}", ${parseFloat(currency.rate)}, "${currency.time}"),`;
    });
    queryString = queryString.slice(0, -1);
    console.log("in insertHistoricCurrencyConversion queryString", queryString);

    let sqlquery = `INSERT INTO historic_exchange_rates(source, target, rate, updated_at)
                        VALUES ${queryString}`
    let result = await query(sqlquery);
    console.log("result in insert", result);
  }
  catch (error) {
    console.log("Error in inserting historic exchange rates ", error);
    throw error;
  }
};

module.exports.deleteHistoricCurrencyConversion = async currencyRates => {
  try {
    const query = util.promisify(connection.query).bind(connection);

    let sqlquery = `DELETE FROM historic_exchange_rates where updated_at < DATE_SUB(NOW() , INTERVAL 30 DAY) `
    let result = await query(sqlquery);
    console.log("result in delete", result);
  }
  catch (error) {
    console.log("Error in deleting historic exchange rates ", error);
    throw error;
  }
};