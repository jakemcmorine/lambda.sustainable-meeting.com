const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
    GOCARDLESS_URL: "https://api-sandbox.gocardless.com/",
    GOCARDLESS_TOKEN: "sandbox_WyfqKHjjrnll7N0V0Nd28h0pUON_Bs5etYVSBEOU",
  },
  prod: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    GOCARDLESS_URL: "https://api.gocardless.com/",
    GOCARDLESS_TOKEN: "live_uaeyuv4d-46z8iVyI6yLAAtTvAiEcRYAeuQ-s_re",
  },
};
let env = config[process.env.stage];
var connection = mysql.createConnection({
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
});

module.exports.goCardlessURL = env.GOCARDLESS_URL;
module.exports.goCardlessToken = env.GOCARDLESS_TOKEN;
module.exports.connection = connection;
