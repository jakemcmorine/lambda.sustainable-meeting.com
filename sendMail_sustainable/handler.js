'use strict';

// include mysql module
const util = require('util');
const moment = require('moment');
var pool = require('./config').pool;
var USER_URL = require('./config').USER_URL;
var S3_BUCKET_NAME = require('./config').S3_BUCKET_NAME;
var AWS_REGION = require('./config').AWS_REGION;
// var AWS_ACCESSKEYID = require('./config').AWS_ACCESSKEYID;
// var AWS_ACCESSKEY = require('./config').AWS_ACCESSKEY;
const getConnection = util.promisify(pool.getConnection).bind(pool);
const Hubspot = require('hubspot')
const aws = require('aws-sdk');
const s3 = new aws.S3();

const hubspot = new Hubspot({
  apiKey: '2f5a0715-c458-4862-87f0-3971d81c57d4',
})
function callback(error, info) {
  if (error) {
    console.log(error);
  } else {
    console.log('Message sent: ' + info.response);
  }
}
function twoDigits(d) {
  if (0 <= d && d < 10) return "0" + d.toString();
  if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
  return d.toString();
}

/**
 * …and then create the method to output the date string as desired.
 * Some people hate using prototypes this way, but if you are going
 * to apply this to more than one Date object, having it as a prototype
 * makes sense.
 **/
Date.prototype.toMysqlFormat = function () {
  return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

module.exports.sendMail = async (event) => {
  console.log("Check 123")
  console.log(event.obj)
  console.log(event)
  event.obj.forEach(ob => {
    ob.email = ob.email.toLowerCase();
  });
  console.log("lower case email obj", event.obj);

  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let dte = new Date();
  dte = dte.toMysqlFormat()
  console.log(dte);

  let user = await query(`SELECT 	firstName,lastName,email, status from users where id=${event.setting.id}`);

  if (user.length == 0) {
    console.log("user not present");
    return;
  }
  let organizerDetails = user[0];

  let IPStatus = await query(`SELECT status, isActive, trialEnd, webUrl from users where id=${event.setting.orginasation_id}`);
  console.log("Ip status", IPStatus);
  var mailArray = [];

  const firstDate = new Date().setHours(12, 0, 0, 0);
  const secondDate = new Date(IPStatus[0].trialEnd);
  let user_status = IPStatus[0].status;
  console.log("user status from db: ", user_status, "-----trialEnd-----", IPStatus[0].trialEnd);

  let freeTrial = false;

  if (secondDate > firstDate && IPStatus[0].status != 'force_inactive') {
    console.log("before trial end IP active");
    user_status = 'active';
    freeTrial = true;
    /* After Trial period, check the IP's status */
  } else if (firstDate > secondDate && IPStatus[0].status != 'inactive' && IPStatus[0].status != 'force_inactive') {
    console.log("after trial end IP active");
    user_status = 'active';
  }
  if (user_status != 'active') {
    console.log("user is not active, so return");
    let meetingUpdate = await query(`update meetings SET countable=0  where id='${event.id}'`);
    console.log("after updating countable in sendMail", meetingUpdate, "meeting id", event.id);
    return;
  }

  console.log("user status after trial period check: ", user_status);

  /* Check if IP has deactivated his account */
  if (IPStatus[0].isActive == 0) {
    console.log("IP's account is not active");
    let meetingUpdate = await query(`update meetings SET countable=0  where id='${event.id}'`);
    console.log("after updating countable in sendMail", meetingUpdate, "meeting id", event.id);
    return;
  }

  /* Plan access for budget and trees planted */
  let treeAccess = false, budgetAccess = false;
  
  // Check for feature access only after free trial period
  if(firstDate > secondDate) {
    let planAccess = await query(`SELECT P.feature_id FROM user_plans AS UP LEFT JOIN plans AS P ON P.plan_details_id = UP.plan_id
       WHERE org_id = ${event.setting.orginasation_id}`);
    console.log("Plan access list", planAccess);
    treeAccess = planAccess.some(ft => ft.feature_id == 3); 
    budgetAccess = planAccess.some(ft => ft.feature_id == 4); 
    console.log("Trees", treeAccess);
    console.log("Budget", budgetAccess);
  }

  /*user budget check starts here */
  let budgetStatus = 0, budgetCount = 0, currentCount = 0, monthlyDates, freeTrialTreesPlanted = 0, freeTrialTreesAvailable = 0, freeTrialTreesPlanting = 0;
  let orgMainId = event.setting.orginasation_id;
  let budgetQuery = await query(`SELECT exclude_meeting,domain_url,budget, budgetStatus, free_trees, free_trial_tree_mode, treesPerMeetingStatus, treesPerMeeting
                     from settings WHERE user_id = ?`, [orgMainId]);
  let treesFeatureStatus = budgetQuery[0].treesPerMeetingStatus || 0;
  let globalTreeCount = budgetQuery[0].treesPerMeeting || 1;

  if (freeTrial) {
    console.log("Inside free trial");
    try {
      freeTrialTreesPlanted = await query(`select sum(trees) as treeCount from free_trial_tree_count where org_id = ? `, [orgMainId]);
      freeTrialTreesPlanted = freeTrialTreesPlanted[0].treeCount || 0;
      console.log("freeTrialTreesPlanted", freeTrialTreesPlanted);

      freeTrialTreesAvailable = budgetQuery[0].free_trees || 0;
      console.log("freeTrialTreesAvailable", freeTrialTreesAvailable);
    } catch (error) {
      freeTrialTreesPlanted = 0;
      freeTrialTreesAvailable = 0;
      console.log("error checking free tree count");
      console.error(error);
    }
  }

  
  if (budgetAccess == true) {

    if (budgetQuery.length > 0) {
      // if the user is IP get org id and budget limit
      budgetStatus = budgetQuery[0].budgetStatus;
      budgetCount = budgetQuery[0].budget;
      if(budgetCount == null || budgetCount == 0 || budgetCount == undefined || budgetCount == "") {
        budgetStatus = 0;
      }
    } 
    //get start and end date for the current active subscription 
    monthlyDates = await query(`select subscriptionId, cycleStartDate, billingDate from subscriptions where orginasation_id = ? and status = 'active'`, [orgMainId]);

    if (budgetStatus == 1 && monthlyDates.length > 0) {
      let subStartDate = monthlyDates[0].cycleStartDate;
      let subEndDate = monthlyDates[0].billingDate;
      let subId = monthlyDates[0].subscriptionId;

      //check here internal meeting or not and free trail      
      //for get the sum of the current organization tree count
      let getTreeCount = await query(`select sum(trees) as treeCount from tree_counts where subscriptionId = ? AND date BETWEEN ? AND ? and org_id = ? `, [subId, subStartDate, subEndDate, orgMainId]);
      currentCount = getTreeCount[0].treeCount || 0;
      console.log("Current cycle tree count", currentCount);

      if (currentCount >= budgetCount) {
        //checking the tree count with the budget count
        //to do - sent mail for exceed the limit
        //update the meeting countable to 0 - limit exceeded
        await query(`update meetings set countable = 0  where id='${event.id}'`);
        console.log("*** budget limit reached. making countable 0 here ***")
        return;
      }
    }
  }
  /*user budget check ends here */

  let meetingStart = event.setting.startTime;
  let meetingZoneOffset = event.setting.TimeZoneOffset;
  let timeZoneName = event.setting.TimeZoneName;

  let tempDate = new Date(meetingStart)
  var meetingDateTime = tempDate
  console.log(`meetingDateTime : ${meetingDateTime}`);
  let meetingDate, meetingTime;
  // if (meetingZoneOffset) {
  //   let meetingZoneOffsetMilleSec = parseFloat(meetingZoneOffset) * 60 * 60 * 1000; //  meetingZoneOffset in milliseconds

  //   if (meetingZoneOffset.charAt(0) == '+') {
  //     // meetingDateTime = new Date(tempDate.setHours(tempDate.getHours() + parseFloat(meetingZoneOffset)));
  //     meetingDateTime = new Date(tempDate.getTime() + meetingZoneOffsetMilleSec)
  //   }
  //   else {
  //     // meetingDateTime = new Date(tempDate.setHours(tempDate.getHours() - parseFloat(meetingZoneOffset)));
  //     meetingDateTime = new Date(tempDate.getTime() - meetingZoneOffsetMilleSec)
  //   }
  // }
  // let meetingDate = meetingDateTime.toDateString().substring(4);
  // let meetingTime = meetingDateTime.toTimeString().substring(0, 5);
  if (meetingZoneOffset) {
    let meetingLocalTime = moment(meetingDateTime).utcOffset(meetingZoneOffset).format('DD-MM-YYYYTHH:mm');
    meetingTime = meetingLocalTime.substring(meetingLocalTime.indexOf('T') + 1, meetingLocalTime.length);
    meetingDate = meetingLocalTime.substring(0, 10);
    console.log("Using moment", meetingLocalTime);
  }
  console.log(timeZoneName);
  if (timeZoneName) {
    let timeZoneAbbr = await query(`select time_zone_abbr FROM time_zone_details  WHERE time_zone_name LIKE '${timeZoneName}'`);
    console.log("timeZoneAbbr");
    console.log(timeZoneAbbr);
    if (timeZoneAbbr.length) {
      console.log(timeZoneAbbr[0].time_zone_abbr);
      meetingTime += " " + timeZoneAbbr[0].time_zone_abbr;
    }
  }

  let organizerName = organizerDetails.firstName;
  if (organizerDetails.lastName != '')
    organizerName += " " + organizerDetails.lastName;

  console.log("Meeting time, Organizer name, Meeting Date", meetingTime, organizerName, meetingDate);

  let userTreeCount = 1, subscriptionId = null;
  try {
    /* Get subscription id only after trial end */
    if (IPStatus[0].trialEnd != null && firstDate > secondDate) {
      console.log("Fetching subscription id and tree count after trialend");
      let subscription = await query(`SELECT subscriptionId FROM subscriptions WHERE orginasation_id = ${event.setting.orginasation_id} AND status="active"`);
      if (subscription.length) {
        subscriptionId = JSON.stringify(subscription[0].subscriptionId);
        /* Only if IP has access to this feature, fetch the tree count */
        if(treeAccess == true && treesFeatureStatus == 1) {
          console.log("IP has plan access and Feature is enabled");
          let treeCount = await query(`SELECT tree_number FROM user_settings WHERE user_id = ${event.setting.id}`);
          if (treeCount.length) {
            console.log("Per-user");
            userTreeCount = treeCount[0].tree_number;
          } else {
            console.log("global");
            userTreeCount = globalTreeCount;
          }
        }
      }
    }
    console.log("tree count and subscriptionId", userTreeCount, subscriptionId);
  } catch (error) {
    console.log("Error in getting tree count from user setting table.");
    console.error(error);
  }


  //budget task check starts
  let inviteeFinal;
  if (budgetStatus === 1) {
    if (currentCount == null || currentCount == '' || currentCount == undefined || currentCount == 'null') {
      currentCount = 0;
    }
    let finalArryInvitee = [];
    event.obj.forEach(async c => {
      if (((currentCount + userTreeCount) <= budgetCount)) {

        finalArryInvitee.push({
          firstname: c.firstname,
          email: c.email,
          lastname: c.lastname ? c.lastname : ''
        });
        currentCount += userTreeCount;
      }

    });

    inviteeFinal = finalArryInvitee;
  } else {
    let free_trial_tree_mode = budgetQuery[0].free_trial_tree_mode || 'limited'
    if (freeTrial) { /* Exclude internal meetings on Free Trial check */
      try {
        console.log("Creating trial user array");
        let domainValue = findDomainFromUrl(IPStatus[0].webUrl);
        let finalArryInvitee = [];
        let exclude_meeting_val = budgetQuery[0].exclude_meeting || '0';
        const pushToUser = usr => {
          currentCount++;
          console.log("currentCount", currentCount);
          finalArryInvitee.push({
            firstname: usr.firstname,
            email: usr.email,
            lastname: usr.lastname || ''
          });
        }
        event.obj.forEach(async c => {
          const domain_email = c.email.split("@")[1] || "";
          console.log("domainValue, domain",domainValue, domain_email);
          if (domainValue.trim().toLowerCase() == domain_email.trim().toLowerCase()) {
            console.log("Inside internal user");
            switch (free_trial_tree_mode) {
              case 'limited':
                if ((freeTrialTreesPlanted + freeTrialTreesPlanting) < freeTrialTreesAvailable && exclude_meeting_val == '0') {
                  console.log("Inside internal user-limited mode");
                  freeTrialTreesPlanting++;
                  console.log("freeTrialTreesPlanting", freeTrialTreesPlanting);
                  pushToUser(c);
                }
                break;
              case 'unlimited':
                console.log("Inside internal user- unlimited mode");
                if(exclude_meeting_val == '0') {
                  freeTrialTreesPlanting++;
                  console.log("Inside internal user-unlimited mode - include internal");
                  pushToUser(c);
                }
                break;
            }
          } else {
            console.log("Insed external user");
            pushToUser(c)
          }
        });
        console.log("finalArryInvitee", finalArryInvitee);
        inviteeFinal = finalArryInvitee;
      } catch (error) {
        console.log("Error in creating free trial user");
        console.error(error);
      }
    } else {
      inviteeFinal = event.obj;
    }
  }

  //budget task check ends

  console.log("Final invitee list---------", inviteeFinal);
  let totalTrees = 0;
  //check budget- invitee list
  if (inviteeFinal.length == 0) {
    console.log("***Budget Exhausted (Or) Internal domain excluded*** \nMaking countable 0")
    await query(`update meetings SET countable = 0  where id='${event.id}'`);
  } else {
    let promises = inviteeFinal.map(async (meetingData) => {

      var newHubspotUser = true
      try {
        let hsContact = await hubspot.contacts.getByEmail(meetingData.email.toLowerCase());
        console.log("hsContact2")
        // console.log(hsContact)
        if (hsContact) {
          newHubspotUser = false
        }
      } catch (error) {
        // console.log(error.error);
        if (error.error && error.error.category && error.error.category == 'OBJECT_NOT_FOUND') {
          console.log("Hubspot Contact Not Found")
        } else {
          console.log(error);
        }
      }
      let domainName = event.setting.domain_url || "";
      let exclude_meeting_status = event.setting.exclude_meeting || "";
      if (budgetQuery.length > 0) {
        domainName = budgetQuery[0].domain_url;
        exclude_meeting_status = budgetQuery[0].exclude_meeting || '0';
      }
      domainName = findDomainFromUrl(domainName);
      console.log("++++++++++ exclude meeting value +++++++++++++", exclude_meeting_status, typeof(exclude_meeting_status), domainName);
      var firstname = meetingData.firstname;
      if (firstname == meetingData.email) {
        firstname = ""
      }
      const contactObj = {
        "properties": [
          {
            "property": "sustainable_meeting_certificate_link",
            "value": USER_URL + "/certificate/" + meetingData.email.toLowerCase()
          },
          { "property": "sustainable_meeting_certificate_sent", "value": "Yes" },
          { "property": "firstname", "value": firstname },
          { "property": "lastname", "value": meetingData.lastname },
          { "property": "sustainable_meeting_meeting_organiser", "value": organizerName },
          { "property": "sustainable_meeting_meeting_time", "value": meetingTime },
          { "property": "sustainable_meeting_meeting_date", "value": meetingDate },
          // { "property": "csr_hub_viewed", "value": "No" },
        ]
      };
      if (newHubspotUser) {
        contactObj.properties.push({ "property": "lifecyclestage", "value": "other" },)
      }
      console.log(contactObj);

      // console.log("dev-user.ss.emvigotechnologies.com/certificate/" + event.id + "/" +meetingData.email)
      console.log(`${USER_URL}/certificate/${meetingData.email}`)
      try {
        /* Exclude domain check only for after free trial. Free trial check is already done above */
        if (exclude_meeting_status == '1' && domainName && freeTrial == false) {
          var domain = meetingData.email.split("@");
          if (domain[1].toLowerCase() != domainName.toLowerCase()) {

            var data1 = await hubspot.contacts.createOrUpdate(meetingData.email, contactObj)
            console.log((data1))
            mailArray.push(meetingData.email);

            totalTrees += userTreeCount;
            return updateCertificateData(query, meetingData, event.id, userTreeCount);
          } else {
            console.log("after free trial, excluded domain", meetingData.email)
            return true;
          }
        } else {
          let dd = await hubspot.contacts.createOrUpdate(meetingData.email, contactObj)
          console.log((dd))
          mailArray.push(meetingData.email);
          totalTrees += userTreeCount;

          return updateCertificateData(query, meetingData, event.id, userTreeCount);
        }

      } catch (error) {
        console.log((error))
      }
    })
    await Promise.all(promises);
  }

  /* Setting First Sustainable Meeting Created property in Hubspot  Start */
  let organizerContact = null, update = true;
  try {
    organizerContact = await hubspot.contacts.getByEmail(organizerDetails.email.toLowerCase());
  } catch (error) {
    if (error.statusCode == 404 && error.error.category == 'OBJECT_NOT_FOUND') {
      console.log("################### FATAL ERROR -Contact does not exist in Hubspot##################")
    }
  }
  if (organizerContact) {
    const firstSMCreated = organizerContact.properties.first_sustainable_meeting_created;
    if (firstSMCreated) {
      if (firstSMCreated.value == 'Yes') {
        update = false;
      } else {
        // already Yes. No need to change
      }
    } else {
      console.log("property not set yet. Set it to 'Yes' now.")
    }
    if (update) {
      const userProperties = {
        "properties": [
          {
            "property": "first_sustainable_meeting_created",
            "value": "Yes"
          }
        ]
      };
      console.log("Updating Organizer First SM Created Property");
      console.log(organizerDetails.email, userProperties);
      await hubspot.contacts.updateByEmail(organizerDetails.email, userProperties);
    }
  }
  /* Setting First Sustainable Meeting Created property in Hubspot  End */


  let emailCondition = mailArray.join("','");
  console.log("===========");
  console.log(dte);
  console.log(mailArray);
  console.log(emailCondition);
  console.log("===========");

  let newstateMachineArnUpdate = await query(`update meeting_invites SET emailsent_date='${dte}'  where meeting_id='${event.id}' and email in ('${emailCondition}')`)
  console.log(newstateMachineArnUpdate);

  if (Array.isArray(mailArray) && mailArray.length > 0) {
    await createGreenStandTable(mailArray, event, freeTrial, userTreeCount, query)
  }

  if (freeTrialTreesPlanting > 0) {
    await createEntryFreeInternal(event.setting.id, event.setting.orginasation_id, freeTrialTreesPlanting, dte, query);
  }

  try {
    console.log("Before updating tree_counts table, tree count", totalTrees, "user id", event.setting.id, "orgId", event.setting.orginasation_id);
    
    let selectQuery = '';
    if (subscriptionId) {
      selectQuery = `SELECT * FROM tree_counts 
      WHERE user_id = ${event.setting.id} AND subscriptionId = ${subscriptionId} AND
      date = DATE('${dte}') AND invoiced = 0`;

    } else {
      selectQuery = `SELECT * FROM tree_counts 
      WHERE user_id = ${event.setting.id} AND subscriptionId IS NULL AND
      date = DATE('${dte}') AND invoiced = 0`;
    }

    console.log("select query ====> ", selectQuery);

    let selectResult = await query(selectQuery);
    console.log("select Rsult===>", selectResult);
    if (selectResult.length) {
      let updtQuery = '';
      if(subscriptionId) {
        updtQuery = `UPDATE tree_counts SET trees = trees + ${totalTrees} WHERE user_id = ${event.setting.id} AND 
        subscriptionId = ${subscriptionId} AND
        date = DATE('${dte}') AND invoiced = 0 ORDER BY id DESC LIMIT 1`;
      } else {
        updtQuery = `UPDATE tree_counts SET trees = trees + ${totalTrees} WHERE user_id = ${event.setting.id} AND 
        subscriptionId IS NULL AND
        date = DATE('${dte}') AND invoiced = 0 ORDER BY id DESC LIMIT 1`;
      }
      console.log("new update query===>", updtQuery);
      let updateResult = await query(updtQuery);
      console.log("new update result===>", updateResult);
    } else {

      let insertQuery = `INSERT INTO tree_counts (user_id, org_id, subscriptionId, trees, invoiced, date, day, month, year, createdAt, updatedAt) 
      VALUES(${event.setting.id}, ${event.setting.orginasation_id}, ${subscriptionId}, ${totalTrees}, 0, DATE('${dte}'), DAY('${dte}'), MONTH('${dte}'), YEAR('${dte}'), now(), now())`;

      console.log("new insert query=====>", insertQuery);
      let insertResult = await query(insertQuery);
      console.log("new insert result===>", insertResult);
    }


    // console.log("Before updating tree_counts table, tree count", totalTrees, "user id", event.setting.id, "orgId", event.setting.orginasation_id);
    // let updateQuery = `INSERT INTO tree_counts (user_id, org_id, subscriptionId, trees, invoiced, date, day, month, year, createdAt, updatedAt) 
    // VALUES(${event.setting.id}, ${event.setting.orginasation_id}, ${subscriptionId}, ${totalTrees}, 0, DATE('${dte}'), DAY('${dte}'), MONTH('${dte}'), YEAR('${dte}'), now(), now())
    // ON DUPLICATE KEY UPDATE trees= trees + ${totalTrees}`;
    // console.log("tree cont query", updateQuery);
    // let treesUpdate = await query(updateQuery);
    // console.log("tree count updated", treesUpdate)
    // Updating company csr hub certificate
    await updateCompanyCertificate(query, event.setting.orginasation_id);
  } catch (error) {
    console.log("Error in updating tree counts table");
    console.error(error);
  }
  // Pushing Budget exhaustion % to Hubspot
  if (budgetStatus === 1) {
    console.log("Total trees planted after this meeting", currentCount);
    let percentage = ((currentCount / budgetCount) * 100).toFixed(2);
    console.log("-- % of trees planted --", percentage);
    const hsupdate = await hubspot.contacts.createOrUpdate(organizerDetails.email.toLowerCase(), {
      properties : [
        {
          property: 'budget_used_percentage',
          value: percentage
        }
      ]
    });
    console.log("Updated budget percentage in hubspot", hsupdate);
  }
  connection.release();
  return true;

};

async function updateCertificateData(query, meetingData, meetingID, treeNumber) {
  console.log("tree count from args", treeNumber)
  let certData = await query(`SELECT * FROM certificates WHERE email = ?`, [meetingData.email.toLowerCase()]);
  console.log("certData=================");
  console.log(certData);
  var fullname = "";
  let dbResult = true;
  if (certData[0]) {
    if (meetingData.firstname || meetingData.firstname != undefined) {
      fullname = meetingData.firstname;
      if (meetingData.lastname || meetingData.lastname != undefined) {
        fullname += " " + meetingData.lastname;
      }
    }
    if (certData[0].certficate_name && certData[0].certficate_name != '') {
      console.log("Deleting old certificate", certData[0].certficate_name);
      await deleteOldCertificate(certData[0].certficate_name);
    }
    dbResult = await query(`UPDATE certificates SET number_of_tree_planted=number_of_tree_planted + ${treeNumber}, certficate_name = '', name = ?, meeting_id= ?, certificate_date= ?, hubViewed = ? where email = ?`, [fullname, meetingID, new Date(), false, meetingData.email.toLowerCase()])
  } else {
    if (meetingData.firstname || meetingData.firstname != undefined) {
      fullname = meetingData.firstname;
      if (meetingData.lastname || meetingData.lastname != undefined) {
        fullname += " " + meetingData.lastname;
      }
    }
    var InsertData = [fullname, meetingData.email.toLowerCase(), meetingID, treeNumber, new Date(), false, new Date(), new Date()];
    dbResult = await query("INSERT INTO certificates(name, email, meeting_id, number_of_tree_planted, certificate_date, hubViewed, createdAt, updatedAt) VALUES(?,?,?,?,?,?,?,?)", InsertData);
  }
  return dbResult;
}

async function deleteOldCertificate(filename) {
  try {
    var filePrefix = "https://" + S3_BUCKET_NAME + ".s3." + AWS_REGION + ".amazonaws.com/";
    console.log("filePrefix", filePrefix)
    var res = filename.split(filePrefix);

    let awsFile = res[1].toString();
    console.log("AWS file name", awsFile);
    var params = {
      Bucket: S3_BUCKET_NAME,
      Key: awsFile
    };
    let deletedStatus = await s3.deleteObject(params).promise();
    console.log("***** deleted successfully *****", deletedStatus);
    return deletedStatus;

  } catch (error) {
    console.log("Error in deleting certificate from S3", error);
  }
}

async function updateCompanyCertificate (query, org_id) {
  let certData = await query(`SELECT * FROM company_certificates WHERE org_id = ?`, [org_id]);
  console.log("company cert Data=================");
  console.log(certData);
  let dbResult = true;
  if (certData[0]) {
    if (certData[0].certificate_name && certData[0].certificate_name != '') {
      console.log("Deleting old certificate", certData[0].certificate_name);
      await deleteOldCertificate(certData[0].certificate_name);
    }
    dbResult = await query(`UPDATE company_certificates SET certificate_name = '', certificate_date= ? where org_id = ?`, [new Date(), org_id])
  } else {
    var InsertData = [org_id, new Date(), new Date(), new Date()];
    dbResult = await query("INSERT INTO company_certificates(org_id, certificate_date, createdAt, updatedAt) VALUES(?,?,?,?)", InsertData);
  }
  return dbResult;
}

const findDomainFromUrl = urlVal => {
  console.log("findDomainFromUrl", urlVal);
  if (urlVal) {
    try {
      urlVal = urlVal.toLowerCase();
      const trimEl = /:\/\/(.[^/]+)/;
      if (urlVal.match(trimEl)) {
        urlVal = urlVal.match(trimEl)[1];
      }
      urlVal = urlVal.split(".");
      urlVal = urlVal.filter(url => url != "www");
      console.log("Url value-----", urlVal);
      return urlVal.join('.');
      // let len = urlVal.length;
      // return `${urlVal[len - 2]}.${urlVal[len - 1]}`;
    } catch (error) {
      console.log("error in findDomainFromUrl");
      console.error(error);
      return "";
    }
  } else {
    return "";
  }
}

const createEntryFreeInternal = async (user_id, org_id, trees_count, date, query) => {
  try {
    console.log("Crating free trial entry user_id, org_id, trees, date, query", user_id, org_id, trees_count, date, query);
    let updateQuery = `INSERT INTO free_trial_tree_count (user_id, org_id, trees, date) 
    VALUES(${user_id}, ${org_id}, ${trees_count}, DATE('${date}'))
    ON DUPLICATE KEY UPDATE trees= trees + ${trees_count}`;
    await query(updateQuery);
  } catch (error) {
    console.log('error in createEntryFreeInternal');
    console.error(error);
  }
}

const createGreenStandTable = async (invitees, event, payment_status, tree_count, query) => {
  try {
    let invites = []
    for (let each of invitees) {
      let inviteIndiividual = [];
      inviteIndiividual.push(event.id)
      inviteIndiividual.push(event.setting.orginasation_id)
      inviteIndiividual.push(event.setting.id)
      inviteIndiividual.push(each)
      inviteIndiividual.push(tree_count)
      inviteIndiividual.push(payment_status)
      invites.push(inviteIndiividual);
    }
    let sqlQuery = `INSERT INTO greenstand_tree_tokens (meeting_id,org_id,user_id,invitee_email, tree_count ,invoice_status) VALUES ?`;
    let resp2 = await query(sqlQuery, [invites])
    return resp2;
  } catch (error) {
    console.log('error in greenstand table creation');
    console.error(error);
    return false;
  }
}
