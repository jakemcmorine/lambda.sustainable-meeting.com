'use strict';

// include mysql module
const util = require('util');
var pool = require('./config').pool;
const getConnection = util.promisify(pool.getConnection).bind(pool);

module.exports.createLog = async (event) => {
  console.log(event);
  event = event.body;
  console.log(event);
  console.log(event.Emails);
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let resp2 = await query(`select *from users where LOWER(email)='${event.Emails[0].toLocaleLowerCase()}'`);
  var response = {
    code: 0,
    message: "Log Not Added.",
  };
  console.log(resp2);
  if (resp2.length){

    let log_query = `INSERT INTO plugin_logs(LogType,Email,LogDate,user_id,createdAt)
        VALUES(?,?,?,?,?)`;
    let logDetails = [event.LogType, event.Emails[0], event.LogDate, resp2[0].id, new Date().toISOString()]
    await query(log_query, logDetails)
    response.message = "Log Has Been Added Successfully.";
  }
  //  connection.end();
  connection.release();
  return response;
};