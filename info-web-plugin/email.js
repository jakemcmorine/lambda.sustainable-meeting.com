const nodemailer = require('nodemailer');
const { environment } = require('./config');

exports.sendPluginAccessEmail = async function (data, email) {
  let transporter = getTransport();
  return transporter.sendMail({
    from: environment.MAIL_FROM,
    to: email,
    subject: 'Invite your people to Sustainable Meeting!',
    html: data
  })
}

exports.sendOrphanUserEmail = async function (data, email) {
  let transporter = getTransport();
  return transporter.sendMail({
    from: environment.MAIL_FROM,
    to: email,
    subject: 'Unknown user installed the plugin!',
    html: data
  })
}

function getTransport() {
  return nodemailer.createTransport({
    port: 587,
    host: environment.MAIL_HOST,
    auth: {
      user: environment.MAIL_USERNAME,
      pass: environment.MAIL_PASSWORD
    },
    debug: true,
  })
}