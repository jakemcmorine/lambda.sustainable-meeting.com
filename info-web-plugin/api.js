const { Auth_Url, Geenstand_Url } = require('./config');

const axios = require('axios');

module.exports.addNewUser = async (email) => {
  try {
    let url = `${Auth_Url}/v1/initiative_partners`;
    let header = {
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      }
    };
    let data = {
      "initiativeId": 182,
      "emailEnabled": "true",
      "signup": "true",
      "type": "initiative_partner",
      "status": "inactive",
      "email": email,
      "isFromPlugin": "true"
    };
    console.log(data);
    await axios.post(url, data, header)
      .then(response => {
        console.log(' *** ^^^^ addNewUser response \n', response);
      })
      .catch(err => {
        console.log("Error in addNewUser API Call");
        console.log(err);
        console.log(err.response);
        throw err;
      });
  } catch (error) {
    console.log("Error Message from addNewUser API Call:", error);
    throw error;
  }
};

module.exports.gsCreateWallet = async (email, initiative_partner) => {
  try {
    let url = `${Geenstand_Url}/api/create_wallet`;
    let data = {
      "email": email,
      "hierarchy": [initiative_partner],
      "platform": "Sustainably Run Meetings"
    };
    await axios.post(url, data, {})
      .then((response) => {
        //   responseData = response.data;
        console.log("Greenstand Wallet creation success", response);
      })
      .catch((err) => {
        console.log("Error in Greenstand Create Wallet API Call");
        console.error(err);
      });
  } catch (error) {
    console.log("$Err", error);
    // throw error;
  }
};