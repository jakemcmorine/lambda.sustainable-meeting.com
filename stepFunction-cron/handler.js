'use strict';

// include mysql module
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
// var connection = require('./config').connection;
var pool = require('./config').pool;
var ARN = require('./config').ARN;
const getConnection = util.promisify(pool.getConnection).bind(pool);

function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  var dif = t1.getTime() - t2.getTime();
  var Seconds_from_T1_to_T2 = dif / 1000;
  // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
  // return Seconds_Between_Dates;
  return Seconds_from_T1_to_T2;
}
async function createStepFunction(invites, delay, setting, id) {
  console.log("delayyyyy", delay)
  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    // const stateMachineArn = "arn:aws:states:eu-west-1:255593839762:stateMachine:emailScheduler_sustainable";
    // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev";
    const stateMachineArn = ARN;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
        "delay_seconds": Math.round(delay),
        "obj": invites,
        "id": id,
        "setting": setting
      }),
    }).promise();
    return newstateMachineArn;

  } catch (error) {
    throw error;
  }
}

// console.log(connection);
module.exports.stepFunctions_cron = async event => {
  console.log("**** First line of Step function ****")
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);

  // let userDetail = await query(`SELECT * FROM meetings WHERE arnName IS NULL AND start_date <= (NOW() + INTERVAL 2 DAY)`)
  let userDetail = await query(`SELECT meetings.id as meeting_ins_id,meetings.user_id as id,meetings.initiativeId,meetings.org_id as orginasation_id,meetings.start_date,meetings.end_date,
        meetings.TimeZoneOffset,meetings.TimeZoneName,settings.id as setting_id,settings.emailSend,settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url,
        users.status as user_status, users.trialEnd as trialEnd
        FROM meetings 
        LEFT JOIN settings ON settings.user_id = meetings.org_id 
        LEFT JOIN users ON users.id = meetings.org_id 
        WHERE meetings.arnName IS NULL AND meetings.start_date BETWEEN CURDATE() AND (CURDATE() + INTERVAL 2 DAY) AND countable = 1`);
  // WHERE meetings.arnName IS NULL AND meetings.start_date <= (CURDATE() + INTERVAL 2 DAY) AND countable = 1`)

  console.log(userDetail);
  let mailTimes;
  for (let i = 0; i < userDetail.length; i++) {
    let userData = userDetail[i];
    let addHourinToSecond = userData.hour * 60 * 60;
    let addMinuteinToSecond = userData.minute * 60;

    let meetingDetails = await query(`SELECT first_name as firstname, last_name as lastname, email FROM meeting_invites WHERE meeting_id = ${userData.meeting_ins_id}`)

    let totalSeconds = addHourinToSecond + addMinuteinToSecond;
    if (userData.emailSend == 'After') {
      mailTimes = findSeconds(new Date().toISOString(), userData.end_date);
      mailTimes = mailTimes + totalSeconds;
    } else {
      mailTimes = findSeconds(new Date().toISOString(), userData.start_date);
      mailTimes = mailTimes - totalSeconds;
    }

    userData.startTime = userData.start_date;
    try {
      if (userData.status != 'inactive' && userData.status != 'force_inactive') {
        let arn = await createStepFunction(meetingDetails, mailTimes, userData, userData.meeting_ins_id)
        let newstateMachineArnUpdate = await query(`update meetings SET arnName='${arn.executionArn}'  
                where id=${userData.meeting_ins_id}`)        
      }

    } catch (err) {
      console.log(err)
    }
    connection.release();
    return { "code": 0, "message": "The Meeting has been updated." }
  }
};



