const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/outlookWebhookRefresh-dev",
    REDIRECT_URL: "https://dev-outlook-addin-ss.emvigotechnologies.com/auth/openid/return",
    CLIENT_SECRET: "bWF_oh~ZtYv5G7a.1x-Za-59t9kAm1dg.O",
    CLIENT_ID: "cec04b71-137b-4a99-80c6-e0fc88a2e7c5",
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4",
  },
  prod: {
    DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/outlookWebhookRefresh-prod",
    REDIRECT_URL: "https://outlook-addin-meetings.sustainably.run/auth/openid/return",
    CLIENT_SECRET: "bWF_oh~ZtYv5G7a.1x-Za-59t9kAm1dg.O",
    CLIENT_ID: "cec04b71-137b-4a99-80c6-e0fc88a2e7c5",
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4",
  },
};
let env = config[process.env.stage];
var pool = mysql.createPool({
  connectionLimit: 1000,
  connectTimeout: 60 * 60 * 1000,
  acquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
  debug: false,
});

module.exports.pool = pool;
module.exports.SQS = env.SQS;
module.exports.REDIRECT_URL = env.REDIRECT_URL;
module.exports.CLIENT_SECRET = env.CLIENT_SECRET;
module.exports.CLIENT_ID = env.CLIENT_ID;
module.exports.HUBSPOT_API_KEY = env.HUBSPOT_API_KEY;
