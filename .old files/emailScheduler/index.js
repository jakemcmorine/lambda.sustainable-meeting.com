// include mysql module
var mysql = require('mysql');
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
// create a connection variable with the required details
var connection = mysql.createConnection({
    host: "db.sustainable-meeting.com", // ip address of server running mysql
    user: "sustainable-user", // user name to your mysql database
    password: "e1908DnwFs7HzXZJ", // corresponding password
    database: "sustainable-meeting" // use the specified database
});
function createInvites(obj, meeting_id) {
    let invites = []
    for (let i = 0; i < obj.length; i++) {
        let inviteIndiividual = [];
        inviteIndiividual.push(obj[i].firstname)
        inviteIndiividual.push(obj[i].lastname)
        inviteIndiividual.push(meeting_id)
        inviteIndiividual.push(obj[i].email)
        inviteIndiividual.push(obj[i].title)
        inviteIndiividual.push(obj[i].company)
        invites.push(inviteIndiividual);
    }

    //   console.log(obj.invites);
    return invites;
}

function traverse(obj) {
    let meeting_in = {};
    for (var i in obj) {

        meeting_in[i] = obj[i];

    }
    return meeting_in;
}
function findSeconds(from, to) {
    var t1 = new Date(to);
    var t2 = new Date(from);
    var dif = t1.getTime() - t2.getTime();
    var Seconds_from_T1_to_T2 = dif / 1000;
    // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
    // return Seconds_Between_Dates;
    return Seconds_from_T1_to_T2;
}
async function createStepFunction(invites, delay, setting, id) {
    
    console.log("delayyyyy", delay)
    delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
    try {
        const stateMachineArn = "arn:aws:states:eu-west-1:255593839762:stateMachine:emailScheduler_sustainable";
        const newstateMachineArn = await stepfunctions.startExecution({
            stateMachineArn,
            input: JSON.stringify({
                "delay_seconds": Math.round(delay),
                "obj": invites,
                "id": id,
                "setting": setting
            }),
        }).promise();
        return newstateMachineArn;

    } catch (error) {
        throw error;
    }

}

// console.log(connection);
exports.handler = async (event, context, callback) => {

    var obj = JSON.stringify(event);
    obj = JSON.parse(obj)
    let invitesOr = Object.assign([], obj.invites);
    delete obj.invites;
    delete obj.organiser;
    console.log(event)

    //   if(userDetail) 
    let meetingDataParsed = traverse(obj)
    const query = util.promisify(connection.query).bind(connection);
    let userDetail = await query(`SELECT  users.id,users.initiativeId,settings.id as setting_id,users.orginasation_id,settings.emailSend,
       settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url
        FROM    users 
        LEFT JOIN settings 
        ON settings.user_id = users.orginasation_id
        WHERE users.email = '${event.organiser.email}'`)
    console.log(userDetail);
    let mailTimes;
    if (userDetail[0]) {
        let addHourinToSecond = userDetail[0].hour * 60;
        let addMinuteinToSecond = userDetail[0].minute * 60;


        if (userDetail[0].emailSend == 'After') {
            let totalSeconds = addHourinToSecond + addMinuteinToSecond;
            mailTimes = findSeconds(new Date().toISOString(), event.end);
            mailTimes = mailTimes + totalSeconds;
        } else {
            let totalSeconds = addHourinToSecond + addMinuteinToSecond;
            mailTimes = findSeconds(new Date().toISOString(), event.start);
            mailTimes = mailTimes - totalSeconds;
        }
        

        // console.log(arn)
        let meeting_info_array = [userDetail[0].id, userDetail[0].initiativeId, new Date(), new Date(), userDetail[0].orginasation_id,
        meetingDataParsed.subject, meetingDataParsed.location, meetingDataParsed.type, meetingDataParsed.start,
        meetingDataParsed.end, meetingDataParsed.AllDay, meetingDataParsed.TimeZoneOffset];
        let meeting_info_query = `INSERT INTO meetings(user_id,initiativeId,createdAt,updatedAt,org_id,subject,location,type,
            start_date,end_date,all_Day,TimeZoneOffset) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;
        try {
            let resp = await query(meeting_info_query, meeting_info_array)
            console.log(invitesOr)
            let inviters = createInvites(invitesOr, resp.insertId)

            const twodays_inseconds = 60 * 60 * 48;
            let total_seconds = findSeconds(new Date().toISOString(), event.start);
            if(total_seconds < twodays_inseconds){
                if (userDetail[0].status != 'inactive' && userDetail[0].status != 'force_inactive') {
                    let arn = await createStepFunction(event.invites, mailTimes, userDetail[0], resp.insertId)
                    let newstateMachineArnUpdate = await query(`update meetings SET arnName='${arn.executionArn}'  
                       where id=${resp.insertId}`)
                }
            }

            event['MeetingId'] = resp.insertId;
            let resp2 = await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company) VALUES ?`, [inviters])
            //  let resp3=await query(`INSERT INTO mfc_meeting_organiser (first_name, last_name,email_address,title ,company) VALUES(?,?,?,?,?)`,meetingDataParsed.organiser)
            //console.log(resp)
        } catch (err) {
            console.log(err)
        }

        return { "code": 0, "message": "The Meeting has been created.", "Meeting": event }

    }

    // connection.end();
    // console.log("out********");
    // console.log(resp)
};