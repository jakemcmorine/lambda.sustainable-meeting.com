// include mysql module
var mysql = require('mysql');
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
// create a connection variable with the required details
var connection = mysql.createConnection({
    host: "db.sustainable-meeting.com", // ip address of server running mysql
    user: "sustainable-user", // user name to your mysql database
    password: "e1908DnwFs7HzXZJ", // corresponding password
    database: "sustainable-meeting", // use the specified database,
});


function findSeconds(from,to){
    var t1 = new Date(to);
    var t2 = new Date(from);
    var dif = t1.getTime() - t2.getTime();
    var Seconds_from_T1_to_T2 = dif / 1000;
    // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
    // return Seconds_Between_Dates;
    return Seconds_from_T1_to_T2;
}
async function createStepFunction(invites,delay,setting,id){
    console.log("delayyyyy",delay)
    delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
    try{
        const stateMachineArn ="arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable";
          const newstateMachineArn = await stepfunctions.startExecution({
                  stateMachineArn,
                  input: JSON.stringify({
                 "delay_seconds": Math.round(delay),
                 "obj":invites,
                 "id":id,
                 "setting":setting
           }),
       }).promise();
        return newstateMachineArn;
        
    }catch(error){
        throw error;
    }
    
}
async function stopStepFunction(executableStateMachineArn){
    console.log("arnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn",executableStateMachineArn)
    try{
        var params = {
            executionArn:executableStateMachineArn /* required */
            
             };
        const stopStepfunctionresult = await stepfunctions.stopExecution(params).promise();
        console.log(stopStepfunctionresult)
        return stopStepfunctionresult;
     }catch(error){
         throw error
     }
        
}
// console.log(connection);
exports.handler = async (event, context, callback) => {
//let event={"Token":"27363d28081f1d365e6d9cee9bdd48b2","Id":278,"value":2,"email":"sonia.rt@emvigotech.com"}
        console.log(event)
        if(event.value==2)
          callback();
        var obj=JSON.stringify(event);
        obj=JSON.parse(obj)
        let invitesOr=Object.assign([], obj.invites);
        delete obj.invites;
        delete obj.organiser;
        console.log(event)
    //if(userDetail) 
   const query = util.promisify(connection.query).bind(connection);
    await query(`DELETE FROM meeting_invites WHERE meeting_id=${event.Id} 
          AND email='${event.email}'`);
    let meeting=await query(`SELECT *FROM meetings m LEFT JOIN meeting_invites mi on mi.meeting_id= m.id WHERE m.id=${event.Id}`);
   let userDetail=await query(`SELECT  users.id, settings.id as setting_id,users.orginasation_id,settings.emailSend,
       settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url
        FROM    users 
        LEFT JOIN settings 
        ON settings.user_id = users.orginasation_id
        WHERE users.id = '${meeting[0].user_id}'`)
    console.log(userDetail);
  if(userDetail[0]){
      let addHourinToSecond = userDetail[0].hour*60;
      let addMinuteinToSecond = userDetail[0].minute*60;
      let totalSeconds=addHourinToSecond+addMinuteinToSecond;
      let mailTimes=findSeconds(new Date().toISOString(),event.start);
      if(userDetail[0].emailSend=='After'){
          mailTimes=mailTimes+totalSeconds;
      }else{
          mailTimes=mailTimes-totalSeconds;
      }
    // console.log(arn)
        try{
         
         
          console.log(meeting)
          await stopStepFunction(meeting[0].arnName); 
          let addHourinToSecond = userDetail[0].hour*60;
          let addMinuteinToSecond = userDetail[0].minute*60;
          let totalSeconds=addHourinToSecond+addMinuteinToSecond;
          let mailTimes=findSeconds(new Date().toISOString(),meeting[0].start_date);
          if(userDetail[0].emailSend=='After'){
              mailTimes=mailTimes+totalSeconds;
          }else{
              mailTimes=mailTimes-totalSeconds;
          }

        const twodays_inseconds = 60 * 60 * 48;
        let total_seconds = findSeconds(new Date().toISOString(), meeting[0].start_date);
        if (total_seconds < twodays_inseconds) {
            if(userDetail[0].status!='inactive'&&userDetail[0].status!='force_inactive'){
                let arn=await createStepFunction(meeting,mailTimes,userDetail[0],event.Id)
                let newstateMachineArnUpdate=await query(`update meetings SET arnName='${arn.executionArn}'  
                where id=${event.Id}`)
            }
        }

        }catch(err){
            console.log(err)
        }
       
      return {"code":0,"message":"The Meeting has been created.","Meeting":event}
   
  }

     // connection.end();
    // console.log("out********");
   // console.log(resp)
};