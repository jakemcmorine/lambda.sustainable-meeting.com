var mysql = require('mysql');
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
// create a connection variable with the required details
var connection = mysql.createConnection({
    host: "db.sustainable-meeting.com", // ip address of server running mysql
    user: "sustainable-user", // user name to your mysql database
    password: "e1908DnwFs7HzXZJ", // corresponding password
    database: "sustainable-meeting", // use the specified database,

});
function createInvites(obj,meeting_id){
    let invites=[]
        for(let i=0;i<obj.length;i++){
             let inviteIndiividual=[];
              inviteIndiividual.push(obj[i].firstname)
              inviteIndiividual.push(obj[i].lastname)
              inviteIndiividual.push(meeting_id)
              inviteIndiividual.push(obj[i].email)
              inviteIndiividual.push(obj[i].title)
              inviteIndiividual.push(obj[i].company)
              invites.push(inviteIndiividual);
        }
        
       //   console.log(obj.invites);
        return invites;
}
async function createStepFunction(invites,delay,setting,id){
    console.log('delayyyyyyyyyyyyyyyyyyyyyyyyyyyyyydelay',delay)
    delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);

    try{
        const stateMachineArn ="arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable";
          const newstateMachineArn = await stepfunctions.startExecution({
                  stateMachineArn,
                  input: JSON.stringify({
                 "delay_seconds": Math.round(delay),
                 "obj":invites,
                 "id":id,
                 "setting":setting
           }),
       }).promise();
        return newstateMachineArn;
        
    }catch(error){
        throw error;
    }
    
}
function findSeconds(from,to){
    var t1 = new Date(to);
    var t2 = new Date(from);
    var dif = t1.getTime() - t2.getTime();
    var Seconds_from_T1_to_T2 = dif / 1000;
    // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
    // return Seconds_Between_Dates;
    return Seconds_from_T1_to_T2;
}
async function stopStepFunction(executableStateMachineArn){
    try{
        var params = {
            executionArn:executableStateMachineArn /* required */
            
             };
        const stopStepfunctionresult = await stepfunctions.stopExecution(params).promise();
        console.log(stopStepfunctionresult)
        return stopStepfunctionresult;
     }catch(error){
         throw error
     }
        
}
exports.handler = async (event) => {
     // TODO implement
    //return event.id;
   // event.id=151;
  //  return "hellow";
//   let event={"Token":"27363d28081f1d365e6d9cee9bdd48b2","Id":39,"invites":
//   [{"firstname":"akash.ek@emvigotech.com","lastname":"","email":"akash.ek@emvigotech.com","title":"","company":""},
//   {"firstname":"sonia.rt@emvigotech.com","lastname":"","email":"sonia.rt@emvigotech.com","title":"","company":""}],
//   "Email":"abin.joseph@emvigotech.com"}
   console.log(event)
  try{
     // event=JSON.parse(event.body)
      const query = util.promisify(connection.query).bind(connection);
        let resp2=await query(`select *from meetings where id=${event.Id}`)
        const executableStateMachineArn =resp2[0].arnName;
         await stopStepFunction(executableStateMachineArn); 
        let userDetail=await query(`SELECT  users.id, settings.id as setting_id,users.orginasation_id,settings.emailSend,
       settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url
        FROM    users 
        LEFT JOIN settings 
        ON settings.user_id = users.orginasation_id
        WHERE users.id = '${resp2[0].user_id}'`)
      let stepfunc;
        console.log(userDetail);
    let mailTimes;
    if(userDetail[0]){
        let addHourinToSecond = userDetail[0].hour*60;
      let addMinuteinToSecond = userDetail[0].minute*60;
     
      if(userDetail[0].emailSend=='After'){
           let totalSeconds=addHourinToSecond+addMinuteinToSecond;
            mailTimes=findSeconds(new Date().toISOString(),resp2[0].end_date);
            mailTimes=mailTimes+totalSeconds;
      }else{
           let totalSeconds=addHourinToSecond+addMinuteinToSecond;
            mailTimes=findSeconds(new Date().toISOString(),resp2[0].start_date);
            mailTimes=mailTimes-totalSeconds;
      }

        const twodays_inseconds = 60 * 60 * 48;
        let total_seconds = findSeconds(new Date().toISOString(), resp2[0].start_date);
        if (total_seconds < twodays_inseconds) {
            if(userDetail[0].status!='inactive'&&userDetail[0].status!='force_inactive'){
                let stepfunc=await createStepFunction(event.invites,mailTimes,userDetail[0],resp2[0].id)
                console.log("stepfunctionnnnnn",stepfunc)
                let newstateMachineArnUpdate=await query(`update meetings SET arnName='${stepfunc.executionArn}'  
                where id=${event.Id}`)
            }
        }
      
      let resp3=await query(`delete  from meeting_invites where meeting_id=${event.Id}`)
      let inviters=createInvites(event.invites,event.Id)
      let resp=await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company) VALUES ?`,[inviters])
      console.log("The Invites has been Updated.")
      return {"code":0,"message":"The Invites has been Updated."};
   }
  }catch(error){
      console.log(error)
  }
    
};