'use strict';

// include mysql module
const util = require('util');
var pool = require('./config').pool;
var USER_URL = require('./config').USER_URL;
const getConnection = util.promisify(pool.getConnection).bind(pool);
const Hubspot = require('hubspot')

const hubspot = new Hubspot({
  apiKey: '2f5a0715-c458-4862-87f0-3971d81c57d4',
})
function callback(error, info) {
  if (error) {
    console.log(error);
  } else {
    console.log('Message sent: ' + info.response);
  }
}
function twoDigits(d) {
  if (0 <= d && d < 10) return "0" + d.toString();
  if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
  return d.toString();
}

/**
 * …and then create the method to output the date string as desired.
 * Some people hate using prototypes this way, but if you are going
 * to apply this to more than one Date object, having it as a prototype
 * makes sense.
 **/
Date.prototype.toMysqlFormat = function () {
  return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

module.exports.sendMail = async (event) => {
  console.log("Check 123")
  console.log(event.obj)
  console.log(event)
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let dte = new Date();
  dte = dte.toMysqlFormat()
  console.log(dte);

  let user = await query(`SELECT 	firstName,lastName,email, status from users where id=${event.setting.id}`);
  if (user.length == 0) {
    console.log("user not present");
    return;
  }
  let IPStatus = await query(`SELECT status from users where id=${event.setting.orginasation_id}`);
  console.log("Ip status", IPStatus);
  var mailArray = [];

  const firstDate = new Date().setHours(12, 0, 0, 0);
  const secondDate = new Date(event.setting.trialEnd);
  let user_status = IPStatus[0].status;
  console.log("user status from db: ", user_status);
  
  if (secondDate > firstDate && IPStatus[0].status != 'force_inactive') {
    console.log("before trial end IP active");
    user_status = 'active';
    /* After Trial period, check the IP's status */
  } else if (firstDate > secondDate && IPStatus[0].status != 'inactive' && IPStatus[0].status != 'force_inactive') {
    console.log("after trial end IP active");
    user_status = 'active';
  }
  if (user_status != 'active') {
    console.log("user is not active, so return");
    let meetingUpdate = await query(`update meetings SET countable=0  where id='${event.id}'`);
    console.log("after updating countable in sendMail", meetingUpdate, "meeting id", event.id);
    return;
  }

  console.log("user status after trial period check: ", user_status);

  let organizerDetails = user[0];
  let meetingStart = event.setting.startTime;
  let meetingZoneOffset = event.setting.TimeZoneOffset;
  let tempDate = new Date(meetingStart)
  var meetingDateTime = tempDate
  console.log(`meetingDateTime : ${meetingDateTime}`);
  if (meetingZoneOffset){
    let meetingZoneOffsetMilleSec = parseFloat(meetingZoneOffset) * 60 * 60 * 1000 ; //  meetingZoneOffset in milliseconds

    if (meetingZoneOffset.charAt(0) == '+'){
      // meetingDateTime = new Date(tempDate.setHours(tempDate.getHours() + parseFloat(meetingZoneOffset)));
      meetingDateTime = new Date(tempDate.getTime() + meetingZoneOffsetMilleSec)
    }
    else{
      // meetingDateTime = new Date(tempDate.setHours(tempDate.getHours() - parseFloat(meetingZoneOffset)));
      meetingDateTime = new Date(tempDate.getTime() - meetingZoneOffsetMilleSec)
    }
  }
  console.log(`meetingDateTime : ${meetingDateTime}`);
  
  let meetingDate = meetingDateTime.toDateString().substring(4);
  let meetingTime = meetingDateTime.toTimeString().substring(0,5);
  
  let organizerName = organizerDetails.firstName;
  if (organizerDetails.lastName != '')
    organizerName += " " + organizerDetails.lastName;

  console.log(meetingTime, organizerName, meetingDate);

  let promises = event.obj.map(async (meetingData) => {
    var newHubspotUser = true
    try {
      let hsContact = await hubspot.contacts.getByEmail(meetingData.email.toLowerCase());
      console.log("hsContact2")
      // console.log(hsContact)
      if (hsContact) {
        newHubspotUser = false
      }
    } catch (error) {
      // console.log(error.error);
      if (error.error.category && error.error.category == 'OBJECT_NOT_FOUND') {
        console.log("Hubspot Contact Not Found")
      }
    }

    let domainName = event.setting.domain_url || "";
      // + event.id + "/" + event.obj[i].email
    var firstname = meetingData.firstname;
    if (firstname == meetingData.email){
      firstname = ""
    }
    const contactObj = {
      "properties": [
        {
          "property": "sustainable_meeting_certificate_link",
          "value": USER_URL+ "/certificate/"+meetingData.email.toLowerCase()
        },
        { "property": "sustainable_meeting_certificate_sent", "value": "Yes" },
        { "property": "firstname", "value": firstname },
        { "property": "lastname", "value":meetingData.lastname },
        { "property": "sustainable_meeting_meeting_organiser", "value": organizerName },
        { "property": "sustainable_meeting_meeting_time", "value": meetingTime },
        { "property": "sustainable_meeting_meeting_date", "value": meetingDate },
      ]
    };
    if(newHubspotUser){
      contactObj.properties.push({ "property": "lifecyclestage", "value": "other" },)
    }
    console.log(contactObj);
    
    // console.log("dev-user.ss.emvigotechnologies.com/certificate/" + event.id + "/" +meetingData.email)
    console.log(`${USER_URL}/certificate/${meetingData.email}`)
    try {
      if (event.setting.exclude_meeting == '1') {
        var domain =meetingData.email.split("@");
        if (domain[1].toLowerCase() != domainName.toLowerCase()){

          var data1 = await hubspot.contacts.createOrUpdate(meetingData.email, contactObj)
          console.log((data1))
          mailArray.push(meetingData.email);

          return updateCertificateData(query, meetingData, event.id);
        }else{
          return true;
        }
      } else {
        let dd = await hubspot.contacts.createOrUpdate(meetingData.email, contactObj)
        console.log((dd))
        mailArray.push(meetingData.email);

        return updateCertificateData(query, meetingData, event.id);
      }

    } catch (error) {
      console.log((error))
    }

    // mailOptions['to']=event.obj.invites[i].email;
    // mailOptions['html']='<p>meeting id is '+event.obj.Id +' <p>Click <a href="dev-user.ss.emvigotechnologies.com/certificate/' + event.obj.Id +"/"+event.obj.invites[i].email+'">here</a> to acces your certificate</p>';
    //   let resp=await smtpTransporter.sendMail(mailOptions);
    //   console.log(resp);
  })
  await Promise.all(promises);

  /* Setting First Sustainable Meeting Created property in Hubspot  Start */
  let organizerContact = null, update = true;
  try {
    organizerContact = await hubspot.contacts.getByEmail(organizerDetails.email.toLowerCase());
  } catch(error) {
    if(error.statusCode == 404 && error.error.category == 'OBJECT_NOT_FOUND') {
      console.log("################### FATAL ERROR -Contact does not exist in Hubspot##################")
    }
  }
  if(organizerContact) {
    const firstSMCreated = organizerContact.properties.first_sustainable_meeting_created;
    if(firstSMCreated) {
      if(firstSMCreated.value == 'Yes'){
        update = false;
      } else {
        // already Yes. No need to change
      }
    } else{
      console.log("property not set yet. Set it to 'Yes' now.")
    }
    if(update) {
      const userProperties = {
        "properties": [
          {
            "property": "first_sustainable_meeting_created",
            "value": "Yes"
          }
        ]
      };
      console.log("Updating Organizer First SM Created Property");
      console.log(organizerDetails.email, userProperties);
      await hubspot.contacts.updateByEmail(organizerDetails.email, userProperties);
    }
  }
  /* Setting First Sustainable Meeting Created property in Hubspot  End */


  let emailCondition = mailArray.join("','");
  console.log("===========");
  console.log(dte);
  console.log(mailArray);
  console.log(emailCondition);
  console.log("===========");

  let newstateMachineArnUpdate = await query(`update meeting_invites SET emailsent_date='${dte}'  where meeting_id='${event.id}' and email in ('${emailCondition}')`)
  console.log(newstateMachineArnUpdate);
  connection.release();
  return true;

};

async function updateCertificateData(query, meetingData, meetingID) {
  let certData = await query(`SELECT * FROM certificates WHERE email = ?`, [meetingData.email.toLowerCase()]);
  console.log("certData====================");
  console.log(certData);
  var fullname = "";
  let dbResult = true;
  if (certData[0]) {
    if (meetingData.firstname || meetingData.firstname != undefined) {
      fullname = meetingData.firstname;
      if (meetingData.lastname || meetingData.lastname != undefined) {
        fullname += " " + meetingData.lastname;
      }
    }
    dbResult = await query(`update certificates SET number_of_tree_planted=number_of_tree_planted + 1, certficate_name = '', name = ?, meeting_id= ?, certificate_date= ? where email = ?`, [fullname, meetingID, new Date(), meetingData.email.toLowerCase()])
  } else {
    if (meetingData.firstname || meetingData.firstname != undefined) {
      fullname = meetingData.firstname;
      if (meetingData.lastname || meetingData.lastname != undefined) {
        fullname += " " + meetingData.lastname;
      }
    }
    var InsertData = [fullname, meetingData.email.toLowerCase(), meetingID, 1, new Date(), new Date(), new Date()];
    dbResult = await query("INSERT INTO certificates(name, email, meeting_id, number_of_tree_planted, certificate_date, createdAt, updatedAt) VALUES(?,?,?,?,?,?,?)", InsertData);
  }
  return dbResult;
}