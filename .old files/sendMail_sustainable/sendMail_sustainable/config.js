const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "ogGSIpKIDCmrHqo8",
    DB_NAME: "dev-sustainable-meeting",
    AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
    // USER_URL: "https://dev-user.ss.emvigotechnologies.com",
    USER_URL: "https://dev-reskin.ss.emvigotechnologies.com",
  },
  prod: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    USER_URL: "https://app2.sustainable-meeting.com",
  },
};
let env = config[process.env.stage];
var pool = mysql.createPool({
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
});

module.exports.pool = pool;
module.exports.USER_URL = env.USER_URL;
