'use strict';
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
const ARN = require('./config').ARN;

function createInvites(obj, meeting_id) {
    let invites = []
    for (let i = 0; i < obj.length; i++) {
      let inviteIndiividual = {};
      inviteIndiividual["first_name"] = obj[i].firstname;
      inviteIndiividual["last_name"]  = obj[i].lastname !== undefined ? obj[i].lastname : "";
      inviteIndiividual["meeting_id"] =meeting_id;
      inviteIndiividual["email"]  = obj[i].email;
      inviteIndiividual["title"]  = obj[i].title || " ";
      inviteIndiividual["company"]  = obj[i].company !== undefined ? obj[i].company : "";
      inviteIndiividual["createdAt"]  = new Date().toString();
      inviteIndiividual[" updatedAt"] = new Date().toString();
      let values = Object.values(inviteIndiividual);
      invites.push(values);
    }
    return invites;
}

function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  // console.log("fromTime", t1, "toTime", t2)
  // console.log(t1.getTime(), "", t2.getTime());
  var dif = t1.getTime() - t2.getTime();
  // console.log("diff", dif)
  var Seconds_from_T1_to_T2 = dif / 1000;
  // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
  // return Seconds_Between_Dates;
  return Seconds_from_T1_to_T2;
}

async function createStepFunction(invites, delay, setting, id) {
  // console.log("delayyyyy", delay)
  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    // const stateMachineArn = "arn:aws:states:eu-west-1:255593839762:stateMachine:emailScheduler_sustainable";
    // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev";
    const stateMachineArn = ARN;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
        "delay_seconds": delay,
        "obj": invites,
        "id": id,
        "setting": setting
      }),
    }).promise();
    return newstateMachineArn;

  } catch (error) {
    throw error;
  }

}

module.exports.meetingCreate = async (calendarResource, userId, recurringMeeting, query) => {
  console.log("----- Meeting Creation For ------", recurringMeeting.nextMeetingDate , "----For ----" , calendarResource.id);
  
  let meetingCheck = await query(`SELECT id FROM meetings WHERE meeting_id = '${calendarResource.id}' `);
  console.log("Meeting Details are : ", meetingCheck);
  if (meetingCheck.length) {
    // return  { "code": 0, "message": "Duplicate Meeting.", "Meeting": event };
    console.log("-----Duplicate Meeting Entry -----Updating Next Meeting Date --------");
    var response =  "Duplicate";
    return response;
  }

  let userDetail = await query(`SELECT users.id, u1.status as user_status, u1.trialEnd, users.initiativeId, settings.id as setting_id, users.orginasation_id,
    settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
    FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
    LEFT JOIN users u1 on users.orginasation_id = u1.id 
    LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
    WHERE users.id = '${userId}'`);

  console.log(userDetail);
  console.log("meetingData", calendarResource);

  let mailTimes;
  if (userDetail[0]) {
    /* For meetings configured after trialEnd, check whether subscription exists 
       Do we need to consider TimeZoneOffset? */
    let meetingStart = new Date(calendarResource.start.dateTime);
    let location = calendarResource.location.displayName || null;
    let trialEnd = new Date(userDetail[0].trialEnd);

    console.log("meeting Start Date", meetingStart);
    console.log("trial End Date", trialEnd);
    //checking for time zone in abbr and time zone name column
    let timeZoneOffset = await query(`select * from time_zone_details where time_zone_name = ? or time_zone_abbr = ?`, [calendarResource.originalStartTimeZone, calendarResource.originalStartTimeZone]);
    timeZoneOffset = timeZoneOffset[0];
    let countable = 1;
    let incomingInviteeDetailed = [];
    let meetingInvites = [];
    
    //adding logs for events
    console.log(calendarResource);
    for (let j = 0; j < calendarResource.attendees.length; j++) {
      console.log(calendarResource.attendees[j])
      if (calendarResource.attendees[j].status.response != "declined" && calendarResource.attendees[j].emailAddress.address != calendarResource.organizer.emailAddress.address) {
        meetingInvites.push(calendarResource.attendees[j].emailAddress.address)
        incomingInviteeDetailed.push({ firstname: calendarResource.attendees[j].emailAddress.name, email: calendarResource.attendees[j].emailAddress.address });
      }
    }

    
    //opt-out check meetings starts here
    let optOutVal = [];
    if (incomingInviteeDetailed) {
        optOutVal = incomingInviteeDetailed.filter(function (listItem) {
            return listItem.email == 'opt-out@sustainably.run';
        });
    }
    
    if(calendarResource['bodyPreview'].includes("<---")) {
      countable = 1;
    }
    if(optOutVal.length>0) {
      countable = 0;
    }
    //opt-out check meetings ends here



    let meetingSeconds = findSeconds(new Date().toISOString(), calendarResource.start.dateTime);
    let trialSeconds = findSeconds(new Date().toISOString(), userDetail[0].trialEnd);

    if(meetingSeconds > trialSeconds) {
      console. log("check using seconds - meeting after trial end")
    }
    if (meetingStart > userDetail[0].trialEnd) {
      console.log("meeting after trial period");
      /* If no active subscription is present */
      if (!userDetail[0].subscriptionId) {
        console.log("-----No subscription ; Setting Next Meeting Date ------");
        /* This meeting is not a Sustainable Meeting. So not inserting data in DB */
        return 'SUCCESS';
      }
    }
    
    let addHourinToSecond = userDetail[0].hour * 60 * 60;
    let addMinuteinToSecond = userDetail[0].minute * 60;


    if (userDetail[0].emailSend == 'After') {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), calendarResource.end.dateTime);
      mailTimes = mailTimes + totalSeconds;
    } else {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), calendarResource.start.dateTime);
      mailTimes = mailTimes - totalSeconds;
    }
    // console.log("after delay calc", mailTimes);
    
    let isCountable = false;
    const firstDate = new Date().setHours(12, 0, 0, 0);
    const secondDate = new Date(userDetail[0].trialEnd);
    let user_status = userDetail[0].user_status;
    if (secondDate > firstDate) {
      user_status = 'active'
    }
    if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive') {
      isCountable = true
    }
    
    let meeting_info_array = [userDetail[0].id, userDetail[0].initiativeId, userDetail[0].orginasation_id,
    calendarResource.subject, location, calendarResource.type, meetingStart,
    calendarResource.end.dateTime, timeZoneOffset.time_zone_offset, calendarResource.originalStartTimeZone,
    countable , recurringMeeting.outlookMeetingId, calendarResource.id, recurringMeeting.resourceId, calendarResource.isAllDay ? 'Y' : 'N'];
    let meetingSave;
    let meeting_info_query = `INSERT INTO meetings(user_id,initiativeId,org_id,subject,location,type,
            start_date,end_date,TimeZoneOffset,TimeZoneName, countable,outlook_meeting_id, meeting_id, rm_id, all_Day) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    try {
         
      // const twodays_inseconds = 60 * 60;
      const twodays_inseconds = 60 * 60 * 48;
      let total_seconds = findSeconds(new Date().toISOString(), meetingStart);

      console.log("=================", userDetail[0].user_status , "=================");
      console.log("=================",user_status , "=================");

      //add meeting creat related data to userDetails
      userDetail[0].startTime = meetingStart;
      userDetail[0].TimeZoneOffset = timeZoneOffset.time_zone_offset;
      userDetail[0].TimeZoneName = calendarResource.originalStartTimeZone;
      
      let inviters = [];
      console.log("---------Checking User status to create Meeting ------------")
      // if (total_seconds < twodays_inseconds) {
        if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive' && countable == 1)  {

            // meeting_info_array.push(userDetail[0].orginasation_id);
            
            console.log("-----------Creating Meeting In Meetings Table-----------");
            meetingSave = await query(meeting_info_query, meeting_info_array);
            console.log(`-----------Meeting Created ${meetingSave.insertId}-----------`);

            inviters = createInvites(incomingInviteeDetailed, meetingSave.insertId);
            let stepfunc = await createStepFunction(incomingInviteeDetailed, mailTimes, userDetail[0], meetingSave.insertId)
            console.log("(`-----------StepFunction", stepfunc);
            let arnName = stepfunc.executionArn;
            let newstateMachineArnUpdate =  await query(`update meetings SET arnName='${arnName}' where id=${meetingSave.insertId}`);
            let insetInvitees = await query("INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company,createdAt, updatedAt) VALUES ? ", [inviters] )
        
        }
      // }

    //   event['MeetingId'] = meetingSave.id;
    // console.log(inviters.values);
    //   let resp2 = await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company, createdAt, updatedAt) VALUES ?`, [inviters])
      //  let resp3=await query(`INSERT INTO mfc_meeting_organiser (first_name, last_name,email_address,title ,company) VALUES(?,?,?,?,?)`,meetingDataParsed.organiser)
      //console.log(resp)
    } catch (err) {
      console.log(err)
    }

    // return { "code": 0, "message": "The Meeting has been created.", "Meeting": event }
    let responseBody = { "code": 0, "message": "The Meeting has been created.", "Meeting":"As" };
    var response = "SUCCESS";
    return response;
  }

};