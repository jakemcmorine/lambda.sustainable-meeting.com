module.exports = class RecurringMeeting {

    constructor (rowData) {
        this.recurringMeetingId = rowData.recurring_meeting_id;
        this.startDate = new Date(rowData.start_date);
        this.endDate = new Date(rowData.end_date);
        this.isAllDay = rowData.is_all_day,
        this.startDay = rowData.start_day,
        this.repeatEvery = rowData.repeat_every,
        this.frequencyType = rowData.frequency_type,
        this.weekdaysSelected = new String(rowData.weekdays_selected),
        this.index = rowData.day_index,
        this.subject = rowData.subject;
        this.location = rowData.location;
        this.type = rowData.type;
        this.TimeZoneOffset = rowData.TimeZoneOffset;
        this.userId = rowData.user_id;
        this.countable = rowData.countable;
        this.organizer = rowData.organizer;
        this.nextMeetingDate = new Date(rowData.next_meeting_date);
        this.resource = rowData.resource;
        this.resourceId = rowData.resource_id;
        this.subscriptionId = rowData.subscription_id;
        this.outlookMeetingId = rowData.outlook_meeting_id;
    }

    getweekDaysSelected() {
        return (this.weekdaysSelected !== null ) ? this.weekdaysSelected.split(",") : this.weekdaysSelected;
    }

}

