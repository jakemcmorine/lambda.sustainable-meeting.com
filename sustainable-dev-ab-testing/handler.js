'use strict';
const aws = require('aws-sdk');
const util = require('util');
const axios = require('axios');
const HUBSPOT_API_KEY = require('./config').HUBSPOT_API_KEY;
const hubspot = require('./HubspotService');

const db = require('./config').pool;
const getConnection = util.promisify(db.getConnection).bind(db);

module.exports.hubspotABTesting = async event => {
  try {
    console.log('Event in hubspot AB testing\n', event);
    // for (let i = 0; i < event.Records.length; i++) {
      let promises = event.Records.map (async(record) => {
      console.log('\n*** Body \n', record.body);
      const eventBody = JSON.parse(record.body);
      const email = eventBody.email;
      const HSContactId  = eventBody.contactId;
      console.log("*** Email value ***", email, HSContactId);
      await this.calculateABvalues(email, HSContactId);
    });
    await Promise.all(promises);
    console.log("#### read all events, returning true #####",)
    return true;

  } catch (error) {
    console.log("Error in processing SQS queue for Hubspot AB testing", error);
    return false;
  }
};

exports.calculateABvalues = async (email, HSContactId) => {
  let connection = await getConnection();
  try {
    console.log("Email id in calculateABvalues", email, HSContactId)
    let ABValue = 'A', ABCValue = 'A', ABCDValue = 'A', ABCDEValue = 'A';

    const query = util.promisify(connection.query).bind(connection);
    let sqlquery = `SELECT * FROM hubspot_abtesting_logs ORDER BY id DESC LIMIT 1`;
    let result = await query(sqlquery);
    if (result.length) {
      const AB = result[0].AB;
      const ABC = result[0].ABC;
      const ABCD = result[0].ABCD;
      const ABCDE = result[0].ABCDE;
      ABValue = (AB == 'A') ? 'B' : 'A';
      ABCValue = this.nextChar(ABC);
      ABCDValue = this.nextChar(ABCD);
      ABCDEValue = this.nextChar(ABCDE);

      ABCValue = (ABCValue == 'D') ? 'A' : ABCValue;
      ABCDValue = (ABCDValue == 'E') ? 'A' : ABCDValue;
      ABCDEValue = (ABCDEValue == 'F') ? 'A' : ABCDEValue;
    } 

    console.log("ABCDE values calculated", ABValue, ABCValue, ABCDValue, ABCDEValue);
    const data = [email, ABValue, ABCValue, ABCDValue, ABCDEValue, HSContactId]
    const hsData = {AB: ABValue, ABC: ABCValue, ABCD: ABCDValue, ABCDE: ABCDEValue};
    await this.insertABTestingLogs(data);
    await this.updateHubspot(email, hsData);
  } catch (error) {
    console.log("Error in calculating AB values")
    throw error;
  } finally {
    connection.release();
  }
}

exports.insertABTestingLogs = async (data) => {
  let connection = await getConnection();
  try {
    console.log("*********Data before insert into DB******", data);
    const query = util.promisify(connection.query).bind(connection);
    let sqlquery = `INSERT INTO hubspot_abtesting_logs(email, AB, ABC, ABCD, ABCDE, HSContactId) VALUES ?`;
    let result = await query(sqlquery, [[data]]);
    console.log("^^^^^^^success in insert", result);
  } catch (error) {
    console.log("Error in inserting AB testing logs in db")
    throw error;
  } finally {
    connection.release();
  }
}

exports.updateHubspot = async (email, data) => {
  try {
    let HSProperties = {
      properties: [
        {
          property: "ab",
          value: data.AB,
        },
        {
          property: "abc",
          value: data.ABC,
        },
        {
          property: "abcd",
          value: data.ABCD,
        },
        {
          property: "abcde",
          value: data.ABCDE,
        },
      ]};
    let update = await hubspot.contacts.updateByEmail (email, HSProperties);
    return update;
  } catch (error) {
    console.log("Error in Hubspost API call");
    throw error;
  }
}

exports.nextChar = (c) => {
  const next = String.fromCharCode(c.charCodeAt(0) + 1);
  return next;
}
