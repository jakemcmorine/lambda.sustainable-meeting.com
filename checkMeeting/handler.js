'use strict';

// include mysql module
const util = require('util');
var connection = require('./config').connection;

// console.log(connection);
module.exports.checkMeeting = async (event, context, callback) => {
  event = JSON.parse(event.body);
  const query = util.promisify(connection.query).bind(connection);

  let meetingDetails = await query(`SELECT M.id,M.outlook_meeting_id AS OutlookID,U.email AS Organizer,MI.email AS Invitee FROM meetings AS M 
    LEFT JOIN meeting_invites AS MI on MI.meeting_id = M.id 
    LEFT JOIN users AS U ON U.id = M.user_id 
    WHERE M.outlook_meeting_id = '${event.OutlookID}'`)
  console.log(meetingDetails);
  let meetingDetailsArranged = {}
  if(meetingDetails[0]){
    meetingDetailsArranged = { 
      meetingId: meetingDetails[0].id, 
      OutlookID: meetingDetails[0].OutlookID, 
      organiser: meetingDetails[0].Organizer, 
      invites:[] 
    };
    for (var i in meetingDetails) {
      meetingDetailsArranged.invites.push(meetingDetails[i].Invitee);
    }
  }
  console.log(meetingDetailsArranged);

  let responseBody = { "code": 0, "message": "Meeting Details", "Meeting": meetingDetailsArranged };
  var response = {
    "statusCode": 200,
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify(responseBody),
  };
  return response;
};