'use strict';
const util = require('util');
var connection = require('./config').connection;

module.exports.appVersion = async event => {
  event = event.body;
  console.log(event);
  const query = util.promisify(connection.query).bind(connection);
  let version;
  if (event.Emails && event.Emails.length){

    let userData = await query(`SELECT * FROM plugin_user_group_map pgm
      JOIN plugin_user_groups pg on pgm.group_id = pg.id
      WHERE email = '${event.Emails[0]}'`);
    console.log(userData);
    if (userData.length){
      version = userData[0].latest_version;
    }else{
      version = await fetchGlobalVersion(query)
    }
  }else{
    version = await fetchGlobalVersion(query)
  }
  // const response = {
  //   "code": 0,
  //   "message": "Current Version is 1.1.14",
  //   "version": "1.1.14"
  // };

  const response = {
    "code": 0,
    "message": "Current Version is " + version,
    "version": version
  };

  return response;
}

async function fetchGlobalVersion(query) {
  let result = await query(`SELECT latest_version FROM plugin_user_groups WHERE group_name = "global"`);
  console.log(result);
  let response = result.length ? result[0].latest_version : "1.1.14";
  return response;
}
