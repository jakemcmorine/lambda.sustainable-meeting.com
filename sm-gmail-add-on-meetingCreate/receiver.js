// 'use strict';
var meetingHandler   = require('./meetingHandler');

module.exports.handler = async event => {
  console.log("Event body:",event.Records[0].body)

  await meetingHandler.handleMeetingEvents(event.Records[0].body);
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
