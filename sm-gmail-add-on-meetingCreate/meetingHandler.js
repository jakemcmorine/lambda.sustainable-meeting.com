const meetingController= require('./controllers/normalMeetingController');
/**
 * Handler events
 * @param msg
 * @returns {Promise<void>}
 */
module.exports.handleMeetingEvents = async msg => {
  try {
    console.log("Inside handleMeetingEvents:");
    msg = JSON.parse(msg);
    var msg_type = msg.type ? msg.type : "";
    console.log("message type :", msg_type);

    switch (msg_type) {
      case 'normal_meeting':
        await meetingController.createNormalMeeting(msg.data);
        break;

      // case 'recurring_meeting':
      //   await meetingController.createRecurringMeeting(msg.data);
      //   break;
        
      case 'cancelled_meeting':
        await meetingController.removeMeeting(msg.data);
        break;

      default:
        console.log("Undefined Message Type :", msg_type)
    }
  } catch (e) {
    console.log("Unexpected Error!", e)
  }
};

