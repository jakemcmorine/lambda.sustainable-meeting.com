var aws             = require('aws-sdk');
const { env }       = require('../config');

var accessKeyId     = env.SQS_ACCESS_KEY_ID;
var secretAccessKey = env.SQS_SECRET_ACCESS_KEY;
var region          = env.SQS_REGION;
var queueUrl        = env.SQS_URL;

var sqs = new aws.SQS({"accessKeyId":accessKeyId, "secretAccessKey": secretAccessKey, "region": region});

module.exports.sendToSqs = async(sqs_type,sqs_data,sqs_group) =>
{ 
  try{
      console.log("Inside sendToSqs");  
      var type = sqs_type ? sqs_type : "";
      var data = sqs_data ? sqs_data : {};
      var sqsMessageGroup =  sqs_group? sqs_group : "";

      let randNum = Math.floor(Math.random() * (10000 - 99999 + 1)) + 99999;
      var MsgDeduplicationId = "m-"+randNum+new Date().getTime();

      var params = {
        MessageBody: JSON.stringify({"type": type ,"data":data}),
        QueueUrl: queueUrl,
        MessageAttributes: {
          someKey: { DataType: 'String', StringValue: "string"}
        },
        MessageGroupId: sqsMessageGroup,
        MessageDeduplicationId: MsgDeduplicationId
      };
      console.log("sqs params:",params);

       var response = await sqs.sendMessage(params).promise();
       console.log("response >",response);
     
  } catch(e){
      console.log("Unexpected Error!",e)
      return false
  }
}