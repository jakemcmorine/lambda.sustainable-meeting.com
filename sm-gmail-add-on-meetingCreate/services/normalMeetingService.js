const config = require('../config');

module.exports.createNormalMeeting = async data => {
    console.log("inside create meeting", data);
    try {
        //TO DO
    } catch (e) {
        console.log("Unexpected Error!", e)
    }
};

module.exports.deleteMeetAttendeeAndData = async (meetId,sqlQuery) => {
    try {

        await sqlQuery(`delete from meeting_invites where meeting_id = ?`,[meetId]);
        await sqlQuery(`delete from meetings where id = ?`, [meetId]);
        return true;

    } catch (e) {
        console.log("Unexpected Error!", e)
    }
};

module.exports.getAndDeleteArn = async (meetId,sqlQuery) => {
    try {
        let arnData = await sqlQuery(`select arnName from meetings where id = ?`, [meetId]);
        return arnData;
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};


module.exports.getUserDetailsWithEmail = async (email, sqlQuery) => {
    try {
        let qu = `
			SELECT
				users.id,
				u1.status AS user_status,
				u1.trialEnd,
				settings.id AS setting_id,
				users.orginasation_id,
				settings.emailSend,
				settings.hour,
				settings.minute,
				settings.exclude_meeting,
				settings.domain_url
			FROM
				users
			LEFT JOIN settings ON settings.user_id = users.orginasation_id
			LEFT JOIN users u1 ON
				users.orginasation_id = u1.id
			WHERE
			users.email = ?
		`;
        const user = await sqlQuery(qu, [email]);
        return user[0];
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

module.exports.getMeetingWithId = async (id, sqlQuery) => {
    try {
        return await sqlQuery(`SELECT * FROM meetings WHERE meeting_id = ?`, id);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

module.exports.insertInvitees = async (invites, sqlQuery) => {
    console.log("insertInvitees");
    console.log(invites);
    try {
        let qu = `INSERT INTO meeting_invites(meeting_id, email, first_name, createdAt, updatedAt) VALUES ?`;
        return await sqlQuery(qu, [invites]);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

module.exports.removeInvitees = async (meeting_id, sqlQuery) => {
    try {
        return await sqlQuery(
            `DELETE FROM meeting_invites WHERE meeting_id = ? and emailsent_date IS NULL`,
            meeting_id
         );
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

module.exports.createMeeting = async (meetingData, sqlQuery) => {
    try {
        let qu = `INSERT INTO meetings( calendar_type, meeting_id, subject, location, start_date, end_date, TimeZoneOffset, TimeZoneName, user_id, all_Day, google_meeting_id, org_id, initiativeId, countable, rm_id, createdAt, updatedAt, outlook_meeting_id ) VALUES(?)`;
        let createVal = await getAllDates();
        let insVal = [
            meetingData.calendar_type,
            meetingData.meeting_id,
            meetingData.subject,
            meetingData.location,
            meetingData.start_date,
            meetingData.end_date,
            meetingData.TimeZoneOffset,
            meetingData.TimeZoneName,
            meetingData.user_id,
            meetingData.all_Day,
            meetingData.google_meeting_id,
            meetingData.org_id,
            meetingData.initiativeId,
            meetingData.countable,
            meetingData.rm_id,
            createVal,
            createVal,
            meetingData.outlook_meeting_id
        ];
        return await sqlQuery(qu, [insVal]);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};


module.exports.updateMeeting = async (meetingData, sqlQuery) => {
    try {
        let qu = `UPDATE meetings SET subject = ?, location = ?, start_date = ?, end_date = ?, TimeZoneOffset = ?, TimeZoneName = ?, all_Day = ?, initiativeId = ?, updatedAt = ? WHERE meeting_id = ?`;
        let updateVal = await getAllDates();
        let dataForUpdate = [
            meetingData.subject,
            meetingData.location,
            meetingData.start_date,
            meetingData.end_date,
            meetingData.TimeZoneOffset,
            meetingData.TimeZoneName,
            meetingData.all_Day,
            meetingData.initiativeId,
            updateVal,
            meetingData.meeting_id,
        ];
        return await sqlQuery(qu, dataForUpdate);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

module.exports.updateMeetingWithArnName = async (meetingId, arnName, sqlQuery) => {
    try {
        return await sqlQuery('UPDATE meetings SET arnName = ? WHERE id = ?', [arnName, meetingId]);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};

const getAllDates = async ()=> {
    let dateVal = new Date().toISOString().
    replace(/T/, ' '). 
    replace(/\..+/, '') ;
    return dateVal;
}

module.exports.getOutLookWithId = async (id, sqlQuery) => {
    try {
        return await sqlQuery(`SELECT * FROM meetings WHERE outlook_meeting_id = ?`, id);
    } catch (e) {
        console.log("Unexpected Error!", e);
        throw e;
    }
};