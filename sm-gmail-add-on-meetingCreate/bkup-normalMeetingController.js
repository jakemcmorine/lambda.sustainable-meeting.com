const util = require('util');
const { pool, env } = require("../config");
const MeetingService = require("../services/normalMeetingService");
const get_connection = util.promisify(pool.getConnection).bind(pool);
var moment = require('moment-timezone');


let AWS_ACCESSKEY = env.AWS_ACCESSKEY_STEP;
let AWS_ACCESSKEYID = env.AWS_ACCESSKEYID_STEP;
let AWS_REGION = env.AWS_REGION_STEP;

const AWS = require('aws-sdk');

AWS.config.update({
   region: AWS_REGION,
   accessKeyId: AWS_ACCESSKEYID,
   secretAccessKey: AWS_ACCESSKEY
})
const stepfunctions = new AWS.StepFunctions();


module.exports.createNormalMeeting = async (data) => {
   const connection = await get_connection();
   try {
      const sqlQuery = util.promisify(connection.query).bind(connection);
      let items = data.items[0];
      const userDetails = await MeetingService.getUserDetailsWithEmail(data.summary, sqlQuery);
      let outlookMeet = await MeetingService.getOutLookWithId(items.iCalUID, sqlQuery);
      if (userDetails) {
         let meetings = await MeetingService.getMeetingWithId(items.id, sqlQuery);
         let attendees = items.attendees;
         const firstDate = new Date().setHours(12, 0, 0, 0);
         const secondDate = new Date(userDetails.trialEnd);
         let user_status = userDetails.user_status;

         if (secondDate > firstDate) {
            user_status = "active";
         }
         let addHourinToSecond = userDetails.hour * 60 * 60;
         let addMinuteinToSecond = userDetails.minute * 60;
         let mailTimes;

         let meetData = {
            calendar_type: "gmail",
            google_meeting_id: items.iCalUID,
            meeting_id: items.id,
            subject: items.summary,
            location: items.location,
            type: items.type,
            start_date: items.start.dateTime || items.start.date,
            end_date: items.end.dateTime || items.end.date,
            TimeZoneOffset: moment.tz(data.timeZone).format("Z"),
            TimeZoneName: data.timeZone,
            cancellation_date: null,
            user_id: userDetails.id,
            all_Day: items.start.date ? 1 : 0,
            org_id: userDetails.orginasation_id,
            initiativeId: 182,
            countable: 1,
            rm_id: items.rm_id || "",
            outlook_meeting_id: items.iCalUID
         };

         if (userDetails.emailSend == 'After') {
            let totalSeconds = addHourinToSecond + addMinuteinToSecond;
            mailTimes = findSeconds(new Date().toISOString(), meetData.end_date);
            mailTimes = mailTimes + totalSeconds;
         } else {
            let totalSeconds = addHourinToSecond + addMinuteinToSecond;
            mailTimes = findSeconds(new Date().toISOString(), meetData.start_date);
            mailTimes = mailTimes - totalSeconds;
         }


         userDetails.startTime = meetData.start_date;
         userDetails.TimeZoneOffset = meetData.TimeZoneOffset;
         userDetails.TimeZoneName = meetData.TimeZoneName;



         //to start check the meeting budget


         if(meetData.user_id) {
            let budgetStatus = 0;
            let budgetCount = 0;
            let orgMainId;

            let budgetQuery = await sqlQuery(`select s.budget,s.budgetStatus,u.orginasation_id from settings s left join users u on u.id = s.user_id where s.user_id = ?`,[meetData.user_id]);
            
            if(budgetQuery.length > 0) {
               // if ip get org id and budget limit

               // TO DO code here for 

               budgetStatus = budgetQuery[0].budgetStatus;
               budgetCount = budgetQuery[0].budget;
               orgMainId = budgetQuery[0].orginasation_id;
            } else {
               //normal user not IP - get organization ID
               let orgIdAndBudget = await sqlQuery(`select a.orginasation_id,b.id as user_id from users a left join users b on b.orginasation_id = a.orginasation_id where a.id = ? and b.type = 'initiative_partner'`,[meetData.user_id]);

               if(orgIdAndBudget.length > 0) {
                  //get budget data for normal users
                  let budgetData = await sqlQuery(`select budget,budgetStatus from settings where user_id = ?`,[orgIdAndBudget[0].user_id]);

                  budgetStatus = budgetData[0].budgetStatus;
                  budgetCount = budgetData[0].budget;
                  orgMainId = orgIdAndBudget[0].orginasation_id;
               }

            }

            //get start and end date for the current active subscription 
            let monthlyDates = await sqlQuery(`select cycleStartDate,billingDate from subscriptions where orginasation_id = ? and status = 'active'`,[orgMainId]);

            if(budgetStatus == 1 && monthlyDates.length > 0) {
               let subStartDate = monthlyDates[0].cycleStartDate;
               let subEndDate = monthlyDates[0].billingDate;


               //for get the sum of the current organization tree count
               let getTreeCount = await sqlQuery(`select sum(trees) as treeCount from tree_counts where date BETWEEN ? AND ? and org_id = ? `, [subStartDate, subEndDate, orgMainId]);
               
               //checking the tree count with the budget count
               console.log("getTreeCount[0].treeCount", getTreeCount[0].treeCount);
               console.log("budgetCountbudgetCount", budgetCount);
               if(getTreeCount[0].treeCount >= budgetCount) {
                  meetData.countable = 0;
               }

               // check the meeting is for next-cycle starts here
               let meetStartDate = await formatDateCheck(meetData.start_date);
               if(new Date(meetStartDate) > new Date(subEndDate)) {
                  meetData.countable = 1;
               }
               // check the meeting for next-cycle ends here

            }
         }



         //end meeting budget check






         if (user_status == "inactive" || user_status == "force_inactive") {
            meetData.countable = 0;
         }
         let meetingUpValue = true;
         let meetingSave = {};
         let existInvites = [];
         if (meetings.length > 0) {
            console.log("if in meetingssss");
            const stateMachineArn = meetings[0].arnName;

            if (stateMachineArn) {
               await stopStepFunction(stateMachineArn);
            }
            existInvites = await selectInvitesNotnull(meetings[0].id, sqlQuery);
            console.log("existInvites => ", existInvites);


            meetingSave = await MeetingService.updateMeeting(meetData, sqlQuery);
         } else if (outlookMeet.length > 0) {
            console.log("else if in outlook posttt");
            let nullCheck = await sqlQuery(`select * from meetings where outlook_meeting_id = ? and meeting_id IS NULL`, [items.iCalUID]);

            if (nullCheck.length > 0) {

               console.log("else if in outlook posttt - if condition");

               existInvites = await selectInvitesNotnull(items.id, sqlQuery);
               console.log("existInvitesexistInvitesexistInvitesexistInvites",existInvites);

               await sqlQuery(`update meetings set meeting_id = ? where outlook_meeting_id = ?`, [items.id, items.iCalUID]);
               meetingSave.insertId = nullCheck[0].id;
               meetingUpValue = false;
            } else {

               console.log("else if in outlook posttt - if else condition");
               meetingSave = await MeetingService.createMeeting(meetData, sqlQuery);
            }
         } else {
            console.log("else last condition -create meeting");
            meetingSave = await MeetingService.createMeeting(meetData, sqlQuery);
         }
         
         let meetingId = meetingSave.insertId || meetings[0].id;

         console.log("meeting  check eeting id", meetingId);

         console.log("meeting check countable", meetData.countable);

         if (meetingUpValue && meetingId) {
            console.log("inside-innnnn-meeting id");
            let invites = await createInvites(attendees, meetingId, existInvites);
            let stepInvit = await createInvitesForStepFun(attendees, existInvites);
            await removeOldInviteeAndAddNew(invites, meetingId, sqlQuery);
            console.log("stepInvit in length", stepInvit.length);
            if(meetData.countable == 1 && stepInvit.length > 0 ) {
               let stepfunc = await createStepFunction(stepInvit, mailTimes, userDetails, meetingId);
               let arnName = stepfunc.executionArn;
               await MeetingService.updateMeetingWithArnName(meetingId, arnName, sqlQuery);
            }
         }
      }
   } catch (error) {
      console.error(error);
   } finally {
      connection.release();
   }

};

const createInvites = async (obj, meeting_id, existInvites) => {
   let inviteList = [];
   let dateVal = await getAllDates();
console.log("inviteeeeee- date", dateVal);
   obj.filter((el) => {
      let ind = existInvites.findIndex(ell => ell.email == el.email);
      if (!el.organizer && el.responseStatus != "declined" && ind < 0) {
         inviteList.push([meeting_id, el.email, el.email, dateVal, dateVal]);
      }
   });
   return inviteList;
};

const createInvitesForStepFun = async (obj, existInvites) => {
   let invitesList = [];
   obj.map(el => {
      let ind = existInvites.findIndex(ell => ell.email == el.email);
      if (!el.organizer && el.responseStatus != "declined" && ind < 0) {
         invitesList.push({
            firstname: el.email,
            email: el.email
         })
      }
   })
   return invitesList;
}
const selectInvitesNotnull = async (meeting_id, sqlQuery) => {
   let getInvites = await sqlQuery(`select email from meeting_invites where emailsent_date IS NOT NULL and meeting_id = ?`, [meeting_id]);
   return getInvites;
}

const removeOldInviteeAndAddNew = async (invites, meetingId, sqlQuery) => {
   await MeetingService.removeInvitees(meetingId, sqlQuery);
   if (invites.length > 0) {
      //check
      console.log("test insert test")
      return await MeetingService.insertInvitees(invites, sqlQuery);
   } else {
      return 0;
   }
};


module.exports.removeMeeting = async (meetingId) => {
   const connection = await get_connection();
   try {
      const sqlQuery = util.promisify(connection.query).bind(connection);
      //TODO DELETE ARN NAME

      let meetIdbyMeetID = await sqlQuery(`select id from meetings where meeting_id = ?`, [meetingId]);
      if (meetIdbyMeetID.length > 0) {
console.log("meeeettttttttt- if loop", meetingId);
         let checkMeetDone = await sqlQuery(`select email from meeting_invites where emailsent_date IS NOT NULL and meeting_id = ?`, [meetIdbyMeetID[0].id]);
         if (checkMeetDone.length > 0) {
            console.log("normal - meeting certificate already sent!... not able to delete meeting - normal meeting flow");
         } else {
            await delArnInvitesAll(meetIdbyMeetID[0].id, sqlQuery);
         }
      } else {

console.log("meeeettttttttt- else if loop", meetingId);
         let meetIdbyRmId = await sqlQuery(`select id from meetings where rm_id = ?`, [meetingId]);
         if (meetIdbyRmId.length > 0) {
            let promData = meetIdbyRmId.map(async (item) => {

               let checkMeetDone = await sqlQuery(`select email from meeting_invites where emailsent_date IS NOT NULL and meeting_id = ?`, [item.id]);
               if (checkMeetDone.length > 0) {
                  console.log("recurring - meeting certificate already sent!... not able to delete meeting  - recurring meeting flow");
               } else {
                  await delArnInvitesAll(item.id, sqlQuery);
               }

            });
            await Promise.all(promData);
         }
      }

   } catch (error) {
      console.error(error);
   } finally {
      connection.release();
   }

};

const delArnInvitesAll = async (meetingId, sqlQuery) => {

   let arnNameVal = await MeetingService.getAndDeleteArn(meetingId, sqlQuery);
   if (arnNameVal.length > 0) {
      await stopStepFunction(arnNameVal[0].arnName);
   }
   //DELETE ATTENDEES FROM MEETING INVITES and  meeting
   await MeetingService.deleteMeetAttendeeAndData(meetingId, sqlQuery);
}


const createStepFunction = async (invites, delay, setting, id) => {
   delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
   try {
      const stateMachineArn = env.STEP_EXE_ARN;
      const newstateMachineArn = await stepfunctions.startExecution({
         stateMachineArn,
         input: JSON.stringify({
            "delay_seconds": Math.round(delay),
            "obj": invites,
            "id": id,
            "setting": setting
         }),
      }).promise();
      return newstateMachineArn;
   } catch (error) {
      throw error;
   }
}

const stopStepFunction = async (executableStateMachineArn) => {
   try {
      var params = {
         executionArn: executableStateMachineArn /* required */
      };
      stopStepfunctionresult = await stepfunctions.stopExecution(params).promise();
      return stopStepfunctionresult;
   } catch (error) {
      throw error;
   }
}


const findSeconds = (from, to) => {
   let t1 = new Date(to);
   let t2 = new Date(from);
   let dif = t1.getTime() - t2.getTime();
   let Seconds_from_T1_to_T2 = dif / 1000;
   return Seconds_from_T1_to_T2;
}

const getAllDates = async () => {
   let dateVal = new Date().toISOString().
      replace(/T/, ' ').
      replace(/\..+/, '');
   return dateVal;
}


const formatDateCheck = async (date) => {
   var d = new Date(date),
       month = '' + (d.getMonth() + 1),
       day = '' + d.getDate(),
       year = d.getFullYear();

   if (month.length < 2) month = '0' + month;
   if (day.length < 2) day = '0' + day;

   return [year, month, day].join('-');
}