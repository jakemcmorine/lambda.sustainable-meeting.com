'use strict';
const { google } = require('googleapis');
const util = require('util');
const { pool, env } = require('./config');
const get_connection = util.promisify(pool.getConnection).bind(pool);
const querystring = require('querystring');
const axios = require('axios');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();

const clientSecret = env.CLIENT_SECRET;
const clientId = env.CLIENT_ID;

const recurringMeet = env.RECURRING_MEETING;
const normalMeet = env.NORMAL_MEETING;
const cancelledMeet = env.CANCELLED;
const optOutEmail = env.OPT_OUT_EMAIL;
const rmCreateMeet = env.RM_MEET_CREATE;

var sqsService = require('./services/sqsService');

exports.cronMeetCheckGmail = async () => {
  const connection = await get_connection();
  try {
    const query = util.promisify(connection.query).bind(connection);
    
    let stDate = new Date();
    stDate.setDate(stDate.getDate() - 1);
    let startDate = stDate.toISOString();

    let enDate   = new Date();
    enDate.setDate(enDate.getDate() + 1);
    let endDate = enDate.toISOString();

    let userSql = await query(`select g.*,u.id as userId from google_user_tokens g left join users u on u.email = g.email`);


    let sDateTime = await getDates(stDate); 
    let eDateTime = await getDates(enDate); 

    // get all the meeting outlook ids from meetings 
    let sqlIds = `select meeting_id from meetings where start_date >= ? and end_date <= ?`;
    let allMeetIDs = await query(sqlIds, [sDateTime, eDateTime]);
    let finalArry = [];
    if(allMeetIDs.length > 0) {
      allMeetIDs.forEach(async c => {
        c.meeting_id != '' ?  finalArry.push(c.meeting_id): '';
      });
    }
    
    let sqlRMIds = `select resource_id from recurring_meetings where start_date >= ? and end_date <= ?`;
    let allRMMeetIDs = await query(sqlRMIds, [sDateTime, eDateTime]);
    let finalRMArray = [];
    if (allRMMeetIDs.length > 0) {
      allRMMeetIDs.forEach(async c => {
        c.resource_id != '' ? finalRMArray.push(c.resource_id) : '';
      });
    }
    
    // check user data exists
    if(userSql.length > 0) {

      let promises = userSql.map(async (d) => {
      
        // console.log(d);
        let userMainId = d.userId ? d.userId : '';
        let accessToken = d.access_token; 
        let emailId = d.email;


        try {
          
          let headers = {
            'Authorization': `Bearer ${accessToken}`
          }
          let eventUrl = 'https://www.googleapis.com/calendar/v3/calendars/'+emailId+'/events?timeMin='+startDate+'&timeMax='+endDate;

          await axios.get(eventUrl, { headers })
          .then(response => { 
              processEventData(response, d, finalArry, finalRMArray, query); 
          })
          .catch(error => {              
              if(error.response.status === 401 && error.response.statusText === 'Unauthorized') {
                  refreshGetEvents(d,startDate,endDate, finalArry, finalRMArray, query);
              }
              
          })



        } catch (error) {
          console.log("error", error.code);
        }   

      });
      await Promise.all(promises);
    }
    console.log("End")
      
  } catch (error) {
    console.error('SOME ERROR OCCURED');
  } finally {
    connection.release()
  }
}

// // To get dates 
async function getDates(dateVal) {
  let date = dateVal.getFullYear()+'-'+(dateVal.getMonth()+1)+'-'+dateVal.getDate();
  let time = dateVal.getHours() + ":" + dateVal.getMinutes() + ":" + dateVal.getSeconds();
  let datetime = date+' '+time;

  return datetime;
}

//refresh the token if its expires
const refreshGetEvents = async(userTokenDetails,startDate,endDate, finalArry, finalRMArray, query) => {
  try {
      let newAccessToken = await refreshAccessToken(userTokenDetails.email,query);
      headers = {
          'Authorization': `Bearer ${newAccessToken}`
      }
      let url = 'https://www.googleapis.com/calendar/v3/calendars/'+emailId+'/events?timeMin='+startDate+'&timeMax='+endDate;
      await axios.get(url, { headers }).then(response => {
          console.log('--- Event Data Response if access token expires ---');
          processEventData(response, userTokenDetails, finalArry, finalRMArray, query);
      }).catch(error => {
          console.log('Error occurred while geting events refresh access token : ', error.data);
      });

  } catch(error) {
      throw error;
  }
}

const refreshAccessToken = async(emailId, query) => {
  try {
      let userData = await query(`SELECT refresh_token FROM google_user_tokens  WHERE email = ?`, [emailId]);
      
      const { refresh_token } = userData[0];
      const res = await axios.post('https://oauth2.googleapis.com/token', {
          client_id: clientId,
          client_secret: clientSecret,
          grant_type: 'refresh_token',
          refresh_token: refresh_token
      });
      let access_token = res.data.access_token;

      const qu = `UPDATE google_user_tokens SET access_token=? WHERE email = ?`;
      await query(qu, [access_token, emailId]);
      return access_token;
  } catch (error) {
      console.log(error);
      throw error;
  }
}


function processEventData(response, userTokenDetails, finalArry, finalRMArray, query) {
  try {
  console.log("in function processEventData")
  console.log(response.data);
  
  if (response && response !== null && response.data.kind == "calendar#events") {

    let isRecurring = false; //This variable determines whether the meeting is rm or not.

        for (let event of response.data.items) {

            /** checking the opt-out functionality, if the opt-out email is exist or not */
            let optOutVal = [];
            if (event.attendees) {
                let attendeesList = event.attendees;
                optOutVal = attendeesList.filter(function (listItem) {
                    return listItem.email == optOutEmail;
                });
            }
            /** If item received has recurrence field . Consider it as am RM else Normal Meeting. */
            isRecurring = (event.recurrence || event.recurringEventId) ?
                recurringMeet : (event.status == "cancelled") ? cancelledMeet : normalMeet


            switch (isRecurring) {
                case recurringMeet:
                    console.log("---------- Saving Recurring Meeting Data ----------");
                    console.log("dadatdtadtdtaaaa", event);
                    /** Checking the event owner, event has attendees and the opt-out email  */
                    if ((optOutVal.length == 0 && event.attendees && event.organizer.email === userTokenDetails.email) || (event.recurringEventId && event.status === 'cancelled')) {
                      if (!finalRMArray.includes(event.id)) {

                        let userData = [userTokenDetails];
                        //TO DO : Handle Recurring Meeting Creation Flow.
                        
                        let bodyData = {
                          eventDt: event,
                          userTokenDetails: userData,
                          responseData: response.data
                        }
                        let headers = {
                          'Content-Type': 'application/json',
                        }
                        axios.post(rmCreateMeet, bodyData, { headers }).then(response => {
                              console.log("meeting created", response);
                          
                          }).catch(error => {
                              console.log(error);
                              reject(error);
                          });
                        //rmController.saveRecurringMeetingData(event, userTokenDetails, response.data);
                      }
                    }
                    break;
                case normalMeet:
                    console.log("---------- Saving Normal Meeting Data ----------");

                    /** Checking the event owner, event has attendees and the opt-out email  */
                    if (optOutVal.length == 0 && event.attendees && event.organizer.email === userTokenDetails.email) {
                        // createMeeting(response.data)
                        if (!finalArry.includes(event.id)) {
                          let meetData = { items: [event], timeZone: response.data.timeZone, summary: response.data.summary };

                          /********* Send data to sqs ****************************************************/
                          var sqs_type = "normal_meeting";
                          var messageGroupID = userTokenDetails.email;
                          var sqs_data = meetData
                          sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
                          /******* End of Send data to sqs **********************************************/
                        }
                    }
                    break;
                default:
                console.log("default section");
            }
        }
    
  }
} catch (e) {
  console.log("Unexpected Error!", e)
}

}