const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    AWS_STEP: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev",

    CLIENT_SECRET: "us3TwNzT91x6BGudFIicnM6g",
    CLIENT_ID:  "985040735401-p8eimmdbjr5fadjp1tsg7ghuq1kpndcp.apps.googleusercontent.com",
    OPT_OUT_EMAIL: "opt-out@sustainably.run",

    RECURRING_MEETING : "recurringMeeting",
    NORMAL_MEETING : "normalMeeting",
    CANCELLED : "cancelled",

    
    SQS_ACCESS_KEY_ID : 'AKIAYLWKX2YUDFXGUG5P',
    SQS_SECRET_ACCESS_KEY : '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',
    SQS_REGION : "eu-west-1",
    SQS_ARN : 'arn:aws:sqs:eu-west-1:574877062696:gmailCreateMeeting-dev.fifo',
    SQS_URL : 'https://sqs.eu-west-1.amazonaws.com/574877062696/gmailCreateMeeting-dev.fifo',

    AWS_ACCESSKEY_STEP : '4UxIUeeut6KrdjiXURblVfRl1SviJMUuWjw1/hxx',
    AWS_ACCESSKEYID_STEP : 'AKIAYLWKX2YUDFXGUG5P',
    AWS_REGION_STEP : 'eu-west-1',

    STEP_EXE_ARN : 'arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev',

    RM_MEET_CREATE : 'https://dev-google-addin-ss.emvigotechnologies.com/meetings/checkRecurringMeetings',
  },
  prod: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    AWS_STEP: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable",

    CLIENT_SECRET: "tlKuMMWdFhUO2Ua9EaY0xu6O",
    CLIENT_ID:  "243817113489-qq4rl90p0fovi85ikf3l6he53d8qnu1c.apps.googleusercontent.com",
    OPT_OUT_EMAIL: "opt-out@sustainably.run",

    RECURRING_MEETING : "recurringMeeting",
    NORMAL_MEETING : "normalMeeting",
    CANCELLED : "cancelled",
  
        
    SQS_ACCESS_KEY_ID : 'AKIAYLWKX2YUMLLQWVLU',
    SQS_SECRET_ACCESS_KEY : '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',
    SQS_REGION : "eu-west-1",
    SQS_ARN : 'arn:aws:sqs:eu-west-1:574877062696:gmailCreateMeeting-prod.fifo',
    SQS_URL : 'https://sqs.eu-west-1.amazonaws.com/574877062696/gmailCreateMeeting-prod.fifo',

    AWS_ACCESSKEY_STEP : '4UxIUeeut6KrdjiXURblVfRl1SviJMUuWjw1/hxx',
    AWS_ACCESSKEYID_STEP : 'AKIAYLWKX2YUDFXGUG5P',
    AWS_REGION_STEP : 'eu-west-1',

    STEP_EXE_ARN : 'arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable',
    RM_MEET_CREATE : 'https://google-addin-meetings.sustainably.run/meetings/checkRecurringMeetings',
  },
  
};

let env = config[process.env.stage];
exports.env = env;
exports.pool = mysql.createPool({
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
});
