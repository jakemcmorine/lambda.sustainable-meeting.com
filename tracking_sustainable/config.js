const mysql = require('mysql');

const config = {
  'dev':{
    DB_HOST: 'sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com',
    DB_USER: 'dev-sustainable',
    DB_PASSWORD: 'PoRwge4xZSxmJEla',
    DB_NAME : 'dev-sustainable-meeting',
  },
  'prod':{
    DB_HOST : 'sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com',
    DB_USER : 'sustainable-user',
    DB_PASSWORD : 'e1908DnwFs7HzXZJ',
    DB_NAME : 'sustainable-meeting',
  }
};

let env = config[process.env.stage];
var pool = mysql.createPool({
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
});

module.exports.pool = pool;
module.exports.env = env;