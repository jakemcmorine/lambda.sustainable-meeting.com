'use strict';
const util = require('util');
var pool = require('./config').pool;
const getConnection = util.promisify(pool.getConnection).bind(pool);

module.exports.tracking = async event => {
  let connection = await getConnection();
  // TODO implement
  console.log(event);
  const query = util.promisify(connection.query).bind(connection);
  let trackingData = event.body;
  
  if(trackingData){
    let tracking_array = [trackingData.ServerId, trackingData.OutlookId, trackingData.Action, trackingData.InputData, trackingData.Output, trackingData.Message, new Date()];
    let tracking_query = `INSERT INTO tracking(ServerId,OutlookId, Action, InputData, Output, Message, ActionDate) 
    VALUES(?,?,?,?,?,?,?)`;
    try {
      let resp = await query(tracking_query, tracking_array)
      console.log(resp)
    }catch(err){
      console.log(err);
    }
  }

  const response = {
    code: 0,
    message: "The Tracking has been created.",
  };
  connection.release();
  return response;
}