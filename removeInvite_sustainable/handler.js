'use strict';

// include mysql module
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
var pool = require('./config').pool;
var ARN = require('./config').ARN;
const getConnection = util.promisify(pool.getConnection).bind(pool);

function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  var dif = t1.getTime() - t2.getTime();
  var Seconds_from_T1_to_T2 = dif / 1000;
  // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
  // return Seconds_Between_Dates;
  return Seconds_from_T1_to_T2;
}
async function createStepFunction(invites, delay, setting, id) {
  console.log("delayyyyy", delay)
  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);

  try {
    // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev";
    const stateMachineArn = ARN;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
        "delay_seconds": Math.round(delay),
        "obj": invites,
        "id": id,
        "setting": setting
      }),
    }).promise();
    return newstateMachineArn;

  } catch (error) {
    throw error;
  }

}
async function stopStepFunction(executableStateMachineArn) {
  console.log("arnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn", executableStateMachineArn)
  try {
    var params = {
      executionArn: executableStateMachineArn /* required */

    };
    const stopStepfunctionresult = await stepfunctions.stopExecution(params).promise();
    console.log(stopStepfunctionresult)
    return stopStepfunctionresult;
  } catch (error) {
    throw error
  }

}
// console.log(connection);
module.exports.removeInvite = async (event, context, callback) => {
  //let event={"Token":"27363d28081f1d365e6d9cee9bdd48b2","Id":278,"value":2,"email":"sonia.rt@emvigotech.com"}
  console.log(event)
  event = event.body;
  if (event.value != 2){
    let responseBody = { "code": 0, "message": "The Meeting has been Removed.", "Meeting": event }
    var response = {
      "statusCode": 200,
      "headers": {
        "Content-Type": "application/json"
      },
      "body": JSON.stringify(responseBody),
    };
    return response;
    callback();
  }
  console.log(event)
  //if(userDetail) 
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let resp2 = await query(`select *from meetings AS M JOIN meeting_invites as MI on MI.meeting_id = M.id 
    where M.id=${event.Id} and MI.emailsent_date IS NOT NULL`);
  console.log("Meeting Details")
  console.log(resp2)
  if (!resp2.length) {
    await query(`DELETE FROM meeting_invites WHERE meeting_id=${event.Id} 
            AND email='${event.email}'`);
    let meeting = await query(`SELECT *,mi.first_name AS firstname,mi.last_name AS lastname FROM meetings m LEFT JOIN meeting_invites mi on mi.meeting_id= m.id WHERE m.id=${event.Id}`);
    let userDetail = await query(`SELECT  users.id,u1.status as user_status,u1.trialEnd,settings.id as setting_id,users.orginasation_id,settings.emailSend,
                settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url
                  FROM    users 
                  LEFT JOIN settings 
                  ON settings.user_id = users.orginasation_id LEFT JOIN 
                  users u1 on users.orginasation_id=u1.id
                  WHERE users.id = '${meeting[0].user_id}'`)
    console.log(userDetail);
    if (userDetail[0]) {
      let addHourinToSecond = userDetail[0].hour * 60 * 60;
      let addMinuteinToSecond = userDetail[0].minute * 60;
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      let mailTimes = findSeconds(new Date().toISOString(), event.start);
      if (userDetail[0].emailSend == 'After') {
        mailTimes = mailTimes + totalSeconds;
      } else {
        mailTimes = mailTimes - totalSeconds;
      }
      // console.log(arn)
      try {
        console.log(meeting)
        if (meeting[0].arnName){

          await stopStepFunction(meeting[0].arnName);
          let addHourinToSecond = userDetail[0].hour * 60 * 60;
          let addMinuteinToSecond = userDetail[0].minute * 60;
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          let mailTimes = findSeconds(new Date().toISOString(), meeting[0].start_date);
          if (userDetail[0].emailSend == 'After') {
            mailTimes = mailTimes + totalSeconds;
          } else {
            mailTimes = mailTimes - totalSeconds;
          }

          const firstDate = new Date().setHours(12, 0, 0, 0);
          const secondDate = new Date(userDetail[0].trialEnd);
          let user_status = userDetail[0].user_status;
          if (secondDate > firstDate) {
            user_status = 'active'
          }
          console.log("status", user_status);
          userDetail[0].startTime = meeting[0].start_date;
          userDetail[0].TimeZoneOffset = meeting[0].TimeZoneOffset;
          userDetail[0].TimeZoneName = meeting[0].TimeZoneName;
          // const twodays_inseconds = 60 * 60 ;
          const twodays_inseconds = 60 * 60 * 48 * 1000;
          let total_seconds = findSeconds(new Date().toISOString(), meeting[0].start_date);
          if (total_seconds < twodays_inseconds) {
            if(user_status != 'inactive' && userDetail[0].status != 'force_inactive') {
              let arn = await createStepFunction(meeting, mailTimes, userDetail[0], event.Id)
              let newstateMachineArnUpdate = await query(`update meetings SET arnName='${arn.executionArn}'  
                    where id=${event.Id}`)
            }
          }
        }

      } catch (err) {
        console.log(err)
      }

      console.log("Remove Invite Completed ", event.email);
      

      // return { "code": 0, "message": "The Meeting has been created.", "Meeting": event }

      let responseBody = { "code": 0, "message": "The Meeting has been Removed.", "Meeting": event }
      var response = {
        "statusCode": 200,
        "headers": {
          "Content-Type": "application/json"
        },
        "body": JSON.stringify(responseBody),
      };
    }else{
      let response = { "code": 0, "message": "The Invites not Updated." };
    }
      
    connection.release();
    return response;

  }

  // connection.end();
  // console.log("out********");
  // console.log(resp)
};