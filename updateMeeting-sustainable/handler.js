'use strict';

// include mysql module
const util = require('util');
var pool = require('./config').pool;
const getConnection = util.promisify(pool.getConnection).bind(pool);

// exports.handler = async (event) => {
module.exports.meetingUpdate = async (event) => {
  //  TODO implement
  //  return event.id;
  // event.id=151;
  // console.log(event)

  event = event.body;

  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let newstateMachineArnUpdate = await query(`update meetings SET start_date='${event.start}',end_date='${event.end}'  where id=${event.Id}`)
  event['MeetingInfoId'] = event.Id;
  // return { "code": 0, "message": "The Meeting has been Updated.", "organizer": event }
  console.log(event);
  

  let responseBody = { "code": 0, "message": "The Meeting has been Updated.", "organizer": event }
  // var response = {
  //   "statusCode": 200,
  //   "headers": {
  //     "Content-Type": "application/json"
  //   },
  //   "body": JSON.stringify(responseBody),
  // };
  connection.release();
  return responseBody;

};