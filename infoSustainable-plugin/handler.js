'use strict';

// include mysql module
const util = require('util');
const AWS = require('aws-sdk');
const hubspot = require('./HubspotService');
var db = require('./config').pool;
var { addNewUser, gsCreateWallet } = require('./api');
const getConnection = util.promisify(db.getConnection).bind(db);

module.exports.info = async (event) => {
  event = event.body;
  console.log(event);
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    let email = event.Emails[0].toLocaleLowerCase();
    console.log(email);
    const updatedHsContact = await hubspot.contacts.createOrUpdate(email, {
      "properties": [
        {
          "property": "app_plugin_status",
          "value": 'Active'
        }
      ]
    });
    console.log(updatedHsContact);
    // log the access of plugin in the database
    let logPluginQuery = await query(`INSERT INTO plugin_log (email, last_access)
      VALUES ('${email}', NOW())
      ON DUPLICATE KEY UPDATE email='${email}',
      last_access = NOW()`)
    
    let statusPluginQuery = await query(`INSERT INTO plugin_status (email, plugin_type, last_access)
      VALUES ('${email}', "outlookDesktop", NOW())
      ON DUPLICATE KEY UPDATE email='${email}',last_access = NOW(),updated_at = NOW()`)

    let resp2 = await query(`select * from users where LOWER(email)='${email.toLowerCase()}'`);
    console.log(resp2);
    if (resp2.length) {
      const userStatus = resp2[0].status;
      console.log(updatedHsContact);
      console.log("User Details ----");
      console.log(resp2);
      let resp3 = await query(`select s.*,u1.email from settings s LEFT JOIN users u1 on u1.id = s.user_id where user_id=${resp2[0].orginasation_id}`);
      let info_url = "";
      if (resp3[0]) {
        info_url = resp3[0].info_url
      }
      console.log(resp3);
      console.log(info_url);

      if (userStatus != 'yes' && (resp2[0].id != resp2[0].orginasation_id)) {
        let check = await query(`update users SET status='yes' where LOWER(email)='${email.toLowerCase()}'`);
        console.log("update", check);
        await gsCreateWallet(event.Emails[0], resp3[0].email);
      }

      connection.release();
      return {
        "Id": resp2[0].id,
        "FirstName": resp2[0].firstName,
        "Url": "https://" + info_url
      };
    } else {
      connection.release();
      return await addNewUser(event.Emails[0]);
    }
  } catch (error) {
    console.error(error);
    return {
      "message": "Some error occured"
    } 
  }
};