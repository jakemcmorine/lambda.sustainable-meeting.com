"use strict";

const util = require("util");
const pool = require("./config").pool;
const hubspot = require("./HubspotService");
const get_connection = util.promisify(pool.getConnection).bind(pool);

exports.checkFreetrial = async (event) => {
  const connection = await get_connection();
  try {
    const query = util.promisify(connection.query).bind(connection);

    // query the email address who doesnt have subcription id and free trial ended
    const subscription_expired = await query(
      `SELECT email FROM users u 
      LEFT JOIN subscriptions s on u.id=s.orginasation_id 
      WHERE s.id is null AND u.type="initiative_partner" AND 
      DATE_FORMAT(u.trialEnd, '%Y-%m-%d') = DATE_FORMAT((CURDATE() - INTERVAL 1 DAY), '%Y-%m-%d')`
    );

    // Prepare batch data to update their customer status in HubSpot to 'Customer'
    const hubspotUpdateBatchExprdSubIdNull = subscription_expired.map(
      (customerstatus) => {
        const updateData = [
          {
            property: "customer_status",
            value: "Expired",
          },
        ];
        return {
          email: customerstatus.email,
          properties: updateData,
        };
      }
    );
    await hubspot.contacts.createOrUpdateBatch(
      hubspotUpdateBatchExprdSubIdNull
    );

    // Log the email id's of the contacts whose customer_status made 'Expired' without subscription id
    console.log(
      `${subscription_expired.length} customers doesnt have subscription id and with 'Expired' status. emails:`
    );
    for (let i = 0; i < subscription_expired.length; i++) {
      const expiredsubscription_ = subscription_expired[i];
      console.log(expiredsubscription_.email);
    }

    // query the email address who have subcription active and free trial ended
    const subscription_active = await query(
      `SELECT email FROM users 
      LEFT JOIN subscriptions on subscriptions.orginasation_id=users.id
      WHERE type='initiative_partner' AND DATE_FORMAT(trialEnd, '%Y-%m-%d') = DATE_FORMAT((CURDATE() - INTERVAL 1 DAY), '%Y-%m-%d') 
      AND subscriptions.status='active'`
    );

    // Prepare batch data to update their customer status in HubSpot to 'Customer'
    const hubspotUpdateBatchACustomer = subscription_active.map(
      (customerstatus) => {
        const updateData = [
          {
            property: "customer_status",
            value: "Customer",
          },
        ];
        return {
          email: customerstatus.email,
          properties: updateData,
        };
      }
    );
    await hubspot.contacts.createOrUpdateBatch(hubspotUpdateBatchACustomer);

    // Log the email id's of the contacts whose customer_status made 'Customer'
    console.log(
      `${subscription_active.length} customers have 'Customer' status. emails:`
    );
    for (let i = 0; i < subscription_active.length; i++) {
      const active_subscription = subscription_active[i];
      console.log(active_subscription.email);
    }

    // query the email address who doesnt have and free trial ended
    const subscription_not_active = await query(
      `SELECT distinct email FROM users 
      LEFT JOIN subscriptions on subscriptions.orginasation_id=users.id
      WHERE type='initiative_partner' AND 
      DATE_FORMAT(trialEnd, '%Y-%m-%d')= DATE_FORMAT((CURDATE() - INTERVAL 1 DAY), '%Y-%m-%d') AND 
      subscriptions.status!='active'`
    );

    // Prepare batch data to update their customer status in HubSpot to 'Expired'
    const hubspotUpdateBatchExpired = subscription_not_active.map(
      (expiredstatus) => {
        const updateData = [
          {
            property: "customer_status",
            value: "Expired",
          },
        ];
        return {
          email: expiredstatus.email,
          properties: updateData,
        };
      }
    );
    await hubspot.contacts.createOrUpdateBatch(hubspotUpdateBatchExpired);

    // Log the email id's of the contacts whose customer_status made 'Expired'
    console.log(
      `${subscription_not_active.length} customers have 'Expired' status. emails:`
    );
    for (let j = 0; j < subscription_not_active.length; j++) {
      const inactive_subscription = subscription_not_active[j];
      console.log(inactive_subscription.email);
    }
  } catch (error) {
    console.error("FATAL ERROR: Customer status update failed");
    console.error(error);
  } finally {
    connection.release();
  }
};
