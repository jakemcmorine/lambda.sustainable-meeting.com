'use strict';
const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
var pool = require('./config').pool;
const getConnection = util.promisify(pool.getConnection).bind(pool);

module.exports.cancelMeeting = async event => {
  let connection = await getConnection();
  console.log(event)
  event = event.body;
  console.log(event)
  console.log(event.Id)
  const query = util.promisify(connection.query).bind(connection);
  let resp = await query(`SELECT M.id as MeetingId, M.meeting_id as web_meeting_id, M.*, MI.* FROM meetings AS M LEFT JOIN meeting_invites AS MI ON M.id = MI.meeting_id WHERE M.id = ${event.Id} and MI.emailsent_date IS NOT NULL`);
  console.log(resp);
  if (!resp.length) {
    let resp2 = await query(`select arnName from meetings where id=${event.Id}`)
    let delet = await query(`delete from meetings where id=${event.Id}`)
    let deletInvites = await query(`delete from meeting_invites where meeting_id=${event.Id}`)
    const stateMachineArn = resp2[0].arnName;
    console.log(resp2)
    if (stateMachineArn){
      var params = {
        executionArn: stateMachineArn /* required */
    
      };
      const result = await stepfunctions.stopExecution(params).promise();
    
      console.log(`State machine ${stateMachineArn} executed successfully`, result);
    }
  }
  connection.release();
  return {
    "code": 0,
    "message": "The Meeting has been Cancelled.",
  }
}
