'use strict';

// include mysql module
const util = require('util');
const AWS = require('aws-sdk');
var db = require('./config').pool;
const getConnection = util.promisify(db.getConnection).bind(db);

module.exports.pluginStatus = async (event) => {
  event = event.body;
  console.log(event);
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    let email = event.Emails[0].toLocaleLowerCase();
    console.log(email);
    let desktopPluginStatus= false;

    let result = await query(`SELECT disabled FROM plugin_status 
                WHERE email = ? AND plugin_type = ?`, [email, "outlookDesktop"]);
    console.log(result[0]);
    if (result.length && result[0].disabled == 1) {
      desktopPluginStatus = true;
    }
    connection.release();
    return {
      "webPluginEnabled": desktopPluginStatus,
    };
  } catch (error) {
    console.error(error);
    return {
      "message": "Some error occured"
    } 
  }
};