const mysql = require('mysql');
const config = {
  'dev':{
    DB_HOST: 'sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com',
    DB_USER: 'dev-sustainable',
    DB_PASSWORD: 'PoRwge4xZSxmJEla',
    DB_NAME : 'dev-sustainable-meeting',
    AUTH_URL: 'https://dev-auth-ss.emvigotechnologies.com',
    HUBSPOT_API_KEY: '2f5a0715-c458-4862-87f0-3971d81c57d4',
  },
  'prod':{
    DB_HOST : 'sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com',
    DB_USER : 'sustainable-user',
    DB_PASSWORD : 'e1908DnwFs7HzXZJ',
    DB_NAME : 'sustainable-meeting',
    AUTH_URL: 'https://auth2.sustainable-meeting.com',
    HUBSPOT_API_KEY: '2f5a0715-c458-4862-87f0-3971d81c57d4',
  }
};
let env = config[process.env.stage];
var pool = mysql.createPool({
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME
});

module.exports.environment = env;
module.exports.pool = pool;
module.exports.Auth_Url = env.AUTH_URL;