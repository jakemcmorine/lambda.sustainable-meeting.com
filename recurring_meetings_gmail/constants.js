const constants = Object.freeze({
    GOOGLE_CALENDAR_URL: "https://www.googleapis.com/calendar/v3/calendars/primary",
    repeating_frequency  : {
        DAYS:   "days",
        MONTHS:  "months",
        WEEKS :  "weeks",
        YEARS :  "years",
        DAILY : "daily",
        RELATIVE_MONTHLY : "relativeMonthly",
        ABSOLUTE_MONTHLY : "absoluteMonthly",
        WEEKLY : "weekly",
        RELATIVE_YEARLY : "relativeYearly",
        ABSOLUTE_YEARLY : "absoluteYearly"
    },
    weekdays : ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
    frequency : {
        EVER_DAY : "ED",
        EVER_WEEK : "EW",
        EVER_MONTH : "EM",
        EVERY_YEAR : "EY",
        CUSTOM : "C"
    },

    DAYS_IN_WEEK : 7,
    OPT_OUT_EMAIL: 'opt-out@sustainably.run'
    
});


module.exports = constants;
