require('isomorphic-fetch');
const axios = require('axios');
const RecurringMeeting = require("./beans/recurringMeetingBean");
const constants = require('./constants');
const meetings = require('./meetings');
const util = require('util');
const db = require('./config').pool;
const clientId = require('./config').CLIENT_ID; //env. specific
const clientSecret = require('./config').CLIENT_SECRET; //env. specific
//const clientId = constants.CLIENT_ID;
//const clientSecret = constants.CLIENT_SECRET;

const getConnection = util.promisify(db.getConnection).bind(db);

let currentDate = new Date();
let msInDay = 24*60*60*1000; //1 day = 24Hrs => 24 * 60 Min =>  24*60*60 scnds => 24*60*60*1000 ms

// getGmailRecurringMeetings();
/**
     * Step 1 : Get the list of non-cancelled gmail recurring meetings where today's date == next meeting date.
     * Step 2 : Iterate through all recurring meetings    
     * Step 3 : Get instance and set meeting and nmd
**/
// async function getGmailRecurringMeetings(event) {
module.exports.getGmailRecurringMeetings = async (event) => {
    console.log("------------------Opening DB Connection ---------------");
    var connection = await getConnection();
    try {

        const query = util.promisify(connection.query).bind(connection);
        let startTimeimeRange = new Date();
        startTimeimeRange.setHours(00);
        startTimeimeRange.setMinutes(00);
        startTimeimeRange.setSeconds(00); //To handle all day
        startTimeimeRange.setMilliseconds(00);

        let endTImeRange = new Date();
        endTImeRange.setHours(23);
        endTImeRange.setMinutes(59);
        endTImeRange.setSeconds(59); //To handle all day
        endTImeRange.setMilliseconds(59);

        console.log(`------Start TIme is : ` , startTimeimeRange , ` and End Time is : `, endTImeRange);
         /**  Following is to get recurring meetings which include today's day/date.
        * where start_date <= '2020-09-23' AND end_date >= '2020-09-23'; */

       /**
        * Following query checks if there is any meeting for today even though nmd is different.
        * Will gets instances from whose rm instance updated in advance.
        * TO BE TESTED
        */

        const sql = `SELECT rm.recurring_meeting_id, rm.outlook_meeting_id, rm.calendar_type, rm.start_date, rm.end_date, 
        rm.is_all_day, rm.start_day, rm.repeat_every, rm.frequency_type, rm.weekdays_selected, rm.day_index, rm.subject, 
        rm.location, rm.type, rm.TimeZoneOffset, rm.TimeZoneName, rm.organizer, rm.next_meeting_date, rm.resource, rm.resource_id, 
        rma.meeting_start FROM recurring_meetings rm LEFT JOIN recurring_meeting_activities rma 
        ON rm.recurring_meeting_id = rma.recurring_meetings_id 
        WHERE cancelled_at is null
        and calendar_type='google'
        and ( rm.next_meeting_date >= '${startTimeimeRange.toISOString()}' and rm.next_meeting_date <= '${endTImeRange.toISOString()}'  
        or rma.meeting_start >= '${startTimeimeRange.toISOString()}' and rma.meeting_start <= '${endTImeRange.toISOString()}' )`;

        // const sql = `select * from recurring_meetings where next_meeting_date >= '${startTimeimeRange.toISOString()}' and 
        // next_meeting_date <= '${endTImeRange.toISOString()}' and cancelled_at is null and calendar_type='google'`;

        let result = await query(sql);
        console.log(`Recurring Meeting IDs : `, result.map(x => x.recurring_meeting_id));
        for(let i= 0; i< result.length ; i++) {
        
            let recurringMeeting = new RecurringMeeting(result[i]);
            // let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime() , recurringMeeting.TimeZoneOffset);
            console.log(recurringMeeting.startDate.toISOString());
            // Step 3 : IF the repeating_frequency is : 
            /** DAYS */ 
            if(constants.repeating_frequency.DAILY == recurringMeeting.frequencyType) {
                console.log("---------- Daily Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                
                /** toDateString(); //This gives date in format : Sun Jun 20 2020  */
                if( recurringMeeting.nextMeetingDate.toDateString() ==  currentDate.toDateString() ||
                    recurringMeeting.meetingStart.toDateString() == currentDate.toDateString() ) {
                    
                    //Call Graph API and get instance details having meeting on this date
                    console.log("Today's Meeting Subject is : ----", recurringMeeting.subject);
                    let object = await callGoogleApi(recurringMeeting, query);
                    console.log("Object is : ", object);

                    if(object !== null && object !== undefined && object.instance !== undefined) {
                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        if(object.instance.length !== 0) {
                            let response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                            if(response == "SUCCESS" || response == "NO_SUBSCRIPTION" || response == "NO_INSTANCES") {
                                let repeatMeetingAfter = recurringMeeting.repeatEvery;
                                setMeetingInformation(recurringMeeting, repeatMeetingAfter, "daily", query);
                            }
                        } else {
                            console.log('-------No Meeting Found for today , setting Next Meeting Date ------')
                            let repeatMeetingAfter = recurringMeeting.repeatEvery;
                            setMeetingInformation(recurringMeeting, repeatMeetingAfter, "daily", query);
                        }
                      
                    }

                    
                }
            }
            /** WEEKS */
            else if(constants.repeating_frequency.WEEKLY == recurringMeeting.frequencyType) {
                                    
                /** toDateString(); //This gives date in format : Sun Jun 20 2020  */
                if( recurringMeeting.nextMeetingDate.toDateString() ===  currentDate.toDateString() ||
                recurringMeeting.meetingStart.toDateString() === currentDate.toDateString()) {
                    console.log("---------- Weekly Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                    console.log("Today's Meeting Subject is : ", recurringMeeting.subject);
                    //Call Graph API and get instance details having meeting on this date
                    let object = await callGoogleApi(recurringMeeting, query);
                    if(object !== null && object !== undefined && object.instance !== undefined) {
                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        let response;
                        if(object.instance.length !== 0)
                            response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                        else response = "NO_INSTANCES";
                        if(response == "SUCCESS" || response == "NO_INSTANCES" || response == "NO_SUBSCRIPTION") {
                            if(recurringMeeting.getweekDaysSelected().length == 1) {
                            
                                let repeatMeetingAfter = recurringMeeting.repeatEvery * 7;
                                setMeetingInformation(recurringMeeting , repeatMeetingAfter, "weekly", query);
                            } else {
                                
                                let meetingDay = constants.weekdays[recurringMeeting.nextMeetingDate.getDay()];
                                console.log(`--------meeting day : ${meetingDay}-----------------`);
                                if(recurringMeeting.getweekDaysSelected().indexOf(meetingDay) == (recurringMeeting.getweekDaysSelected().length - 1)) {
                                    //Set next meeting date after weeks from 1st day in array.
                                    let gapBwFirstnLastMeetingInWeek = constants.weekdays.indexOf(meetingDay) - constants.weekdays.indexOf(recurringMeeting.getweekDaysSelected()[0])
                                    let repeatMeetingAfter = (recurringMeeting.repeatEvery - 1 ) * 7 + (7 - gapBwFirstnLastMeetingInWeek);
                                    setMeetingInformation(recurringMeeting , repeatMeetingAfter, "weekly", query);
                                } else {
                                    //Set next meeting day for next day in weekDaysSelected array.
                                    console.log("------Weekdays selected are : ------",recurringMeeting.getweekDaysSelected() );
                                    let meetingDayIndex = recurringMeeting.getweekDaysSelected().indexOf(meetingDay); //This will give index of day in weekDaysSelected array in DB
                                    // console.log(`--------meetingDayIndex : ${meetingDayIndex} nextMextDayIndex : ${meetingDayIndex + 1} -----------------`);
                                    let meetingWeekDay = constants.weekdays.indexOf(meetingDay);
                                    //Following code will give the week day number (b)
                                    let nextMeetingWeekDay = constants.weekdays.indexOf(recurringMeeting.getweekDaysSelected()[meetingDayIndex + 1]); 
                                    //Subtract (b) with last occurred meeting day of the week so that we get after how many days do we need to set the next meeting to be at
                                    
                                    let repeatMeetingAfter;
                                    ( nextMeetingWeekDay < meetingWeekDay ) ? 
                                    ( repeatMeetingAfter = 7 - meetingWeekDay + nextMeetingWeekDay ) : ( repeatMeetingAfter = nextMeetingWeekDay - meetingWeekDay);
                                    
                                    console.log(`--------nextMeetingWeekDay : ${nextMeetingWeekDay} repeatMeetingAfter : ${repeatMeetingAfter} -----------------`);
                                    
                                    setMeetingInformation(recurringMeeting , repeatMeetingAfter, "weekly", query);
                                }
                            }
                        }
                    }
                }
            }
            /** RELATIVE MONTHLY */
            else if(constants.repeating_frequency.RELATIVE_MONTHLY == recurringMeeting.frequencyType) {
                if(recurringMeeting.nextMeetingDate.toDateString() == currentDate.toDateString() ||
                recurringMeeting.meetingStart.toDateString() === currentDate.toDateString()) {
                    console.log("---------- Relative Monthly Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                    //Call Graph API and get instance details having meeting on this date
                    let object = await callGoogleApi(recurringMeeting, query);
                    if(object !== null && object !== undefined && object.instance !== undefined) {

                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        let response;
                        if(object.instance.length !== 0)
                            response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                        else response = "NO_INSTANCES";
                        if(response == "SUCCESS" || response == "NO_INSTANCES" || response == "NO_SUBSCRIPTION") {
                            let repeatMeetingAfter = recurringMeeting.repeatEvery;
                            setMeetingInformation(recurringMeeting, repeatMeetingAfter, "relativeMonthly", query);
                        }  
                    }
                    
                }
            } 
            /** ABSOLUTE MONTHLY */
            else if(constants.repeating_frequency.ABSOLUTE_MONTHLY == recurringMeeting.frequencyType ) {
    
                if(recurringMeeting.nextMeetingDate.toDateString() == currentDate.toDateString() ||
                recurringMeeting.meetingStart.toDateString() === currentDate.toDateString()) {
                    console.log("---------- Abolute Monthly Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                    //Call Graph API and get instance details having meeting on this date
                    let object = await callGoogleApi(recurringMeeting, query);
                    if(object !== null && object !== undefined && object.instance !== undefined) {
                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        let response;
                        if(object.instance.length !== 0)
                            response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                        else response = "NO_INSTANCES";
                        if(response == "SUCCESS" || response == "NO_INSTANCES" || response == "NO_SUBSCRIPTION") {
                            let repeatMeetingAfter = recurringMeeting.repeatEvery;
                            setMeetingInformation(recurringMeeting, repeatMeetingAfter, "absoluteMonthly", query);
                        }
                   
                    }                    
                }
            }
            /** RELATIVE YEARLY */
            else if(constants.repeating_frequency.RELATIVE_YEARLY == recurringMeeting.frequencyType) {
                if(recurringMeeting.nextMeetingDate.toDateString() == currentDate.toDateString() ||
                recurringMeeting.meetingStart.toDateString() === currentDate.toDateString()) {
                    console.log("---------- Relative Yearly Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                    //Call Graph API and get instance details having meeting on this date
                    let object = await callGoogleApi(recurringMeeting, query);
                    if(object !== null && object !== undefined && object.instance !== undefined) {

                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        let response;
                        if(object.instance.length !== 0)
                            response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                        else response = "NO_INSTANCES";
                        if(response == "SUCCESS" || response == "NO_INSTANCES" || response == "NO_SUBSCRIPTION") {
                            let repeatMeetingAfter = recurringMeeting.repeatEvery;
                            setMeetingInformation(recurringMeeting, repeatMeetingAfter, "relativeYearly", query);
                        }
                    }
                }
            }
            /** ABSOLUTE YEARLY */
            else if(constants.repeating_frequency.ABSOLUTE_YEARLY == recurringMeeting.frequencyType) {
                if(recurringMeeting.nextMeetingDate.toDateString() == currentDate.toDateString() ||
                recurringMeeting.meetingStart.toDateString() === currentDate.toDateString()) {
                    console.log("---------- Abolute Yearly Cron Running for rmid : " +recurringMeeting.recurringMeetingId + "-----------");
                    //Call Graph API and get instance details having meeting on this date
                    let object = await callGoogleApi(recurringMeeting, query);
                    if(object !== null && object !== undefined && object.instance !== undefined) {

                        const requestBody = {
                            calendarResource : object.instance[0],
                            userId : object.userData.id
                        };
                        
                        let response;
                        if(object.instance.length !== 0)
                            response = await meetings.meetingCreate(requestBody.calendarResource, requestBody.userId, recurringMeeting, query);
                        else response = "NO_INSTANCES";
                        if(response == "SUCCESS" || response == "NO_INSTANCES" || response == "NO_SUBSCRIPTION") {
                            let repeatMeetingAfter = recurringMeeting.repeatEvery;
                            setMeetingInformation(recurringMeeting, repeatMeetingAfter, "absoluteYearly", query);
                        }

                        
                    }
                }
            }
        }

    } catch(error) {
        console.log("Error Occurred ---", error);
    } finally {
        console.log("------------------Closing DB Connection ---------------");
        connection.release();
        return true;
    }
}

async function callGoogleApi(recurringMeeting, query) {

    let currentDate = new Date();
    currentDate.setHours(00);
    currentDate.setMinutes(01);

    let endDate = new Date();
    endDate.setHours(23);
    endDate.setMinutes(59);
    // console.log("------Start date is : ", currentDate, " ---- end date is : ", endDate);
    
    const sql = `SELECT u.id, u.email, tokens.auth_code, tokens.access_token,
        tokens.refresh_token, tokens.subscriptionTime FROM google_user_tokens tokens
        LEFT JOIN users u ON tokens.email = u.email WHERE LOWER(u.email) = ?`
    let result;
    try {
        console.log("------------Getting User Details for Email Id  : ", recurringMeeting.organizer.toLocaleLowerCase());
        result = await query(sql, [recurringMeeting.organizer.toLocaleLowerCase()]);
        console.log("------------Result is :", result);
    }
    catch(error) {
        console.log("--------Error Occurred while getting user details -------", error)
    }
    
    let userData = result[0];
    if (userData && userData !== undefined && userData !== null) {userData.email
        let accessToken = userData.access_token;
       
        let instances;
        let instanceUrl
        try {
            instanceUrl = `/events/${recurringMeeting.resourceId}/instances/?timeMin=${currentDate.toISOString()}&timeMax=${endDate.toISOString()}&timeZone=UTC`;
            instanceUrl = constants.GOOGLE_CALENDAR_URL + instanceUrl;
            let headers = {
                'Authorization': `Bearer ${accessToken}`
              };

            /****STEP 1.3 ********** :
             *  If Parent RM. Check if the same already exists in our DB or not.
             *  Following api call gets (one instance for today ) for particular event.*/
            let instancesReceived = await axios.get(instanceUrl, {
                headers
            });
            instances = instancesReceived.data.items;
            // console.log(util.inspect(instancesReceived.data.items, false, null, true /* enable colors */));
            const querySyncToken = `update google_user_tokens SET syncToken=? WHERE email = ?`;
            await query(querySyncToken, [instancesReceived.data.nextSyncToken, userData.email]);
            console.log("Instances Received are : ", instancesReceived.data.items);
            if(instances[0] !== undefined && instances[0].status == 'cancelled') {
                console.log("-------This meeting is cancelled for today -------");
                return { userData : userData, instance : [] }
            } else {
                //Following condition checks if instance is not undefined and instance.start.timeZone does not exist
                //and it is NOT an all day meeting, then set the timeZoneName
                if(!instances[0] && !instances[0].start.timeZone && instances[0].start.dateTime){
                    instances[0].start.timeZone = recurringMeeting.TimeZoneName;
                }
                return {
                    userData : userData,
                    instance : instances
                }
            }
        } catch (error) {
            //Handling Error
            if (error.response && error.response.status === 401 && error.response.statusText === 'Unauthorized') {

                try {
                    console.log("Invalid Auth Refreshing Token");
                    accessToken = await refreshAccessToken(userData.email, userData.refresh_token, query);
                    let headers = {
                        'Authorization': `Bearer ${accessToken}`
                        };
        
                    /****STEP 1.3 ********** :
                     *  If Parent RM. Check if the same already exists in our DB or not.
                     *  Following api call gets (one instance for today ) for particular event.*/
                    let instancesReceived = await axios.get(instanceUrl, {
                        headers
                    });
                    console.log("Instances Received are : ", instancesReceived.data);
                    instances = instancesReceived.data.items; 
                    const querySyncToken = `update google_user_tokens SET syncToken=? WHERE email = ?`;
                    await query(querySyncToken, [instancesReceived.data.nextSyncToken, userData.email]);
                    
                    if(instances[0].status == 'cancelled') {
                        console.log("-------This meeting is cancelled for today -------");
                        return { userData : userData, instance : [] }
                    } else {
                        //Following condition checks if instance is not undefined and instance.start.timeZone does not exist
                        //and it is NOT an all day meeting, then set the timeZoneName
                        if(!instances[0] && !instances[0].start.timeZone && instances[0].start.dateTime){
                            instances[0].start.timeZone = recurringMeeting.TimeZoneName;
                        }
                        return {
                            userData : userData,
                            instance : instances
                        }
                    }
                } catch (error) {
                    console.error("------- Google Error Occurred -------", error.response);
                    if(error){
                        console.log("Invalid Grant", error);
                    }
                    return {
                        userData : userData,
                        instance : []
                    }
                    // throw error;
                }
                
            } 
            else {
                console.error("=======Not a GraphError=======");
                console.error(error);
                // throw error;
            }
        }
    } else {
        console.log("-----------User Details Not Found --------------");
        return {
            userData : {},
            instance : []
        }
    }

}

const refreshAccessToken = async (email, refreshToken, query) => {
    try {
 
        const res = await axios.post('https://oauth2.googleapis.com/token', {
            client_id: clientId,
            client_secret: clientSecret,
            grant_type: 'refresh_token',
            refresh_token: refreshToken
        });
        let access_token = res.data.access_token;
        const qu = `UPDATE google_user_tokens SET access_token=? WHERE email = ?`;
        await query(qu, [access_token, email]);
        return access_token;
    } catch (error) {
        console.error(error);
        throw error;
    }
}

async function setMeetingInformation(recurringMeeting, days, type, query) {

    console.log("Setting Next Meeting Date for Recurring Meeting ID : ", recurringMeeting.recurringMeetingId);

    let startTime = recurringMeeting.startDate.toLocaleTimeString(); // Gives time in format : 19:30:00
    let endTime = recurringMeeting.endDate.toLocaleTimeString();
    
    /** 
     * Next meeting date stored in DB has time offset change. So meeting start date time will be without offset
     * Next using 
     */
    let meetingStartDateTime = calculateDateTimeWithoutOffset(recurringMeeting.nextMeetingDate , recurringMeeting.TimeZoneOffset);
    // let meetingStartDateTime = calculateDateTimeWithoutOffset(recurringMeeting.nextMeetingDate.getTime(), recurringMeeting.TimeZoneOffset);
    let meetingDurationInMinutes = calculateMeetingDuration( startTime, endTime );
    let meetingEndDateTimeWithOffset = new Date(meetingStartDateTime.getTime() + (meetingDurationInMinutes * 60 * 1000));
    let meetingEndDateTime = calculateDateTimeWithoutOffset(meetingEndDateTimeWithOffset, recurringMeeting.TimeZoneOffset);
    /** Following code calculates the next meeting date and saves it in the recurring meetings db
     *  Following call to db will be asynchronous , so that other part of code can continue to execute.
     */
    let nextMeetingDateTime;
    let sql = `update recurring_meetings set next_meeting_date = ? where recurring_meeting_id = ?`;

    if(type == "absoluteMonthly") {

        nextMeetingDateTime = recurringMeeting.nextMeetingDate.setMonth(recurringMeeting.nextMeetingDate.getMonth() + days);
        let result = await query(sql, [`${new Date(nextMeetingDateTime).toISOString()}`, recurringMeeting.recurringMeetingId]);
        // recurringMeetingRepository.setNextMeetingDate(nextMeetingDateTime, recurringMeeting.recurringMeetingId);
        
    } else if (type == "relativeMonthly") {

        nextMeetingDateTime = setNextMeetingDateForRelativeMonth(recurringMeeting);
        let result = await query(sql, [`${new Date(nextMeetingDateTime).toISOString()}`, recurringMeeting.recurringMeetingId]);
        // recurringMeetingRepository.setNextMeetingDate(nextMeetingDateTime, recurringMeeting.recurringMeetingId)

    } else if(type == "relativeYearly") {

        nextMeetingDateTime = setNextMeetingDateForRelativeYear(recurringMeeting);
        let result = await query(sql, [`${new Date(nextMeetingDateTime).toISOString()}`, recurringMeeting.recurringMeetingId]);
        // recurringMeetingRepository.setNextMeetingDate(nextMeetingDateTime, recurringMeeting.recurringMeetingId);

    } else if( type == "absoluteYearly") {

        nextMeetingDateTime = recurringMeeting.nextMeetingDate.setFullYear(recurringMeeting.nextMeetingDate.getFullYear() + 1);
        let result = await query(sql, [`${new Date(nextMeetingDateTime).toISOString()}`, recurringMeeting.recurringMeetingId]);
        // recurringMeetingRepository.setNextMeetingDate(nextMeetingDateTime, recurringMeeting.recurringMeetingId);

    } else {
        nextMeetingDateTime = new Date(recurringMeeting.nextMeetingDate.getTime() + (days  * msInDay ));
        // console.log(`---------days : ${days} ---------rm nsxt date : ${recurringMeeting.nextMeetingDate.getTime()}----------------------rm next date : ${recurringMeeting.nextMeetingDate}`);
        console.log("Next Meeting Date for RM id :", recurringMeeting.recurringMeetingId , " is : ", nextMeetingDateTime.toISOString());
        try {
            let result = await query(sql, [nextMeetingDateTime.toISOString(), recurringMeeting.recurringMeetingId]);
        } catch(error) {
            console.log("-------Error Occurred while saving the next meeting date ------");
        }
        // recurringMeetingRepository.setNextMeetingDate(nextMeetingDateTime, recurringMeeting.recurringMeetingId);
    }
}

function setNextMeetingDateForRelativeMonth(recurringMeeting) {

    let interval = recurringMeeting.repeatEvery;
    let nextMeetingDate = recurringMeeting.nextMeetingDate;
    let index = recurringMeeting.index;

    let dayIndex = nextMeetingDate.getDay();
    let monthIndex = nextMeetingDate.getMonth();

    let nextMonthIndex = monthIndex + interval;  

    let firstDayOfNextMonth = new Date(nextMeetingDate.getFullYear(), nextMonthIndex , 1);
    firstDayOfNextMonth.setHours(nextMeetingDate.getHours());
    firstDayOfNextMonth.setMinutes(nextMeetingDate.getMinutes());
    console.log("First day of next month ", firstDayOfNextMonth);

    if(index == "first") {
        if(dayIndex == firstDayOfNextMonth.getDay()) {
        return firstDayOfNextMonth;
        } else {
        while(firstDayOfNextMonth.getDay() !== dayIndex) {
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        return firstDayOfNextMonth;
        }
    }
    if(index == "second") {
        while(firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        return firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7);
    }
    if(index == "third") {
        while(firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        return firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7*2);
    }
    if(index == "fourth") {
        while(firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        return firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7*3);
    }
    if(index == "last") {
        let lastDayOfNextMonth = new Date(firstDayOfNextMonth.getFullYear(), firstDayOfNextMonth.getMonth() + interval , 0, firstDayOfNextMonth.getHours(), firstDayOfNextMonth.getMinutes(), firstDayOfNextMonth.getSeconds());
        console.log("Last Day of Next Month is : ", lastDayOfNextMonth);
        while(lastDayOfNextMonth.getDay() !== dayIndex) {
        lastDayOfNextMonth.setDate(lastDayOfNextMonth.getDate() - 1);
        }
        return lastDayOfNextMonth;
    }
}

function setNextMeetingDateForRelativeYear(recurringMeeting) {

    let nextMeetingDate = recurringMeeting.nextMeetingDate;
    let index = recurringMeeting.index;
    let dayIndex = nextMeetingDate.getDay();

    let firsDayOfSameMonthNextYear = new Date(nextMeetingDate.getFullYear() + 1, nextMeetingDate.getMonth() , 1);
    firsDayOfSameMonthNextYear.setHours(nextMeetingDate.getHours());
    firsDayOfSameMonthNextYear.setMinutes(nextMeetingDate.getMinutes());

    if(index == "first") {
        if(dayIndex == firsDayOfSameMonthNextYear.getDay()) {
        return firsDayOfSameMonthNextYear;
        } else {
        while(firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        return firsDayOfSameMonthNextYear;
        }
    }
    if(index == "second") {
        while(firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        return firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7);
    }
    if(index == "third") {
        while(firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        return firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7*2);
    }
    if(index == "fourth") {
        while(firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        return firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7*3);
    }
    if(index == "last") {
        let lastDayOfSameMonthNextYear = new Date(firsDayOfSameMonthNextYear.getFullYear(), firsDayOfSameMonthNextYear.getMonth() + 1 , 0, firsDayOfSameMonthNextYear.getHours(), firsDayOfSameMonthNextYear.getMinutes(), firsDayOfSameMonthNextYear.getSeconds());
        console.log("Last Day of sam Month next year is : ", lastDayOfSameMonthNextYear);
        while(lastDayOfSameMonthNextYear.getDay() !== dayIndex) {
        lastDayOfSameMonthNextYear.setDate(lastDayOfSameMonthNextYear.getDate() - 1);
        }
        return lastDayOfSameMonthNextYear;
    }
}

function calculateMeetingDuration(startTime, endTime) {

    let startTimeArray = startTime.split(":"); // [ "Hour", "Minutes", "Seconds"]
    let endTimeArray = endTime.split(":");

    // let meetingStartTIme = startDate.toString().slice(16,18) + ":" + startDate.toString().slice(19,21);
    // let meetingEndTIme = endDate.toString().slice(16,18) + ":" + endDate.toString().slice(19,21);
    let duration = 0;

    if(endTimeArray[0] > startTimeArray[0]) {

        duration = (duration + parseInt(endTimeArray[0]) - parseInt(startTimeArray[0])) * 60;

        if(endTimeArray[1] > endTimeArray[1]) {
            duration = duration + parseInt(endTimeArray[1]) - parseInt(startTimeArray[1]);
        }
        if(parseInt(endTimeArray[1]) < parseInt(startTimeArray[1])) {
            duration = duration - parseInt(startTimeArray[1]) - parseInt(endTimeArray[1])
        }
    } else {
        if(parseInt(endTimeArray[1]) > parseInt(startTimeArray[1])) {
            duration = duration + parseInt(endTimeArray[1]) - parseInt(startTimeArray[1]);
    }
}
return duration;

}

function calculateDateTimeWithOffset(nextMeetingDateTime, timeZoneOffset) {
    let sign = timeZoneOffset.slice(0,1); //This can be either + or -
    if(timeZoneOffset !== null && timeZoneOffset !== undefined ) {
        let totalSeconds = getTimeZoneOffsetSeconds(timeZoneOffset);
        return sign == "?" ? 
        new Date(nextMeetingDateTime - (totalSeconds * 1000)) : new Date(nextMeetingDateTime + (totalSeconds * 1000));
    } else {
        return new Date(nextMeetingDateTime);
    }
}

function calculateDateTimeWithoutOffset(dateTime, timeZoneOffset) {
    let sign = timeZoneOffset.slice(0,1); //This can be either + or -
    let totalSeconds = getTimeZoneOffsetSeconds(timeZoneOffset);
    if(sign == "?") {
        return new Date(dateTime + (totalSeconds * 1000));
    } else {
        return new Date(dateTime - (totalSeconds * 1000));
    }
}

function getTimeZoneOffsetSeconds(timeZoneOffset) {
    
    let timeToAddSubtract = new String(timeZoneOffset.slice(1));
    if(timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
        let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
        let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":")+1));
        return (( hour*60 ) + minutes) * 60; //Converting to seconds
    } else {
        return totalSeconds = timeToAddSubtract * 60 * 60 ; //Converting Hours to seconds
    }
}
