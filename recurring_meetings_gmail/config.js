const mysql = require("mysql");
const config = {
    local: {
      DB_USER: "root",
      DB_PASSWORD: "mysql",
      DB_NAME : "dev_sm",
      DB_HOST: "localhost",
      "dialect": "mysql",
      "operatorsAliases": false,
      CLIENT_SECRET: 'us3TwNzT91x6BGudFIicnM6g',
      CLIENT_ID: '985040735401-p8eimmdbjr5fadjp1tsg7ghuq1kpndcp.apps.googleusercontent.com',
      ARN: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev",
    },
    dev: {
      DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
      DB_USER: "dev-sustainable",
      DB_PASSWORD: "PoRwge4xZSxmJEla",
      DB_NAME: "dev-sustainable-meeting",
      AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
      CLIENT_SECRET: 'us3TwNzT91x6BGudFIicnM6g',
      CLIENT_ID: '985040735401-p8eimmdbjr5fadjp1tsg7ghuq1kpndcp.apps.googleusercontent.com',
      ARN: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev",
    },
    prod: {
      DB_HOST: "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
      DB_USER: "sustainable-user",
      DB_PASSWORD: "e1908DnwFs7HzXZJ",
      DB_NAME: "sustainable-meeting",
      CLIENT_SECRET: 'tlKuMMWdFhUO2Ua9EaY0xu6O',
      CLIENT_ID: '243817113489-qq4rl90p0fovi85ikf3l6he53d8qnu1c.apps.googleusercontent.com',
      ARN: "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable",
    },
  }
  let env = config[process.env.stage];
  var pool = mysql.createPool({
    connectionLimit: 1000,
    connectTimeout: 60 * 60 * 1000,
    acquireTimeout: 60 * 60 * 1000,
    timeout: 60 * 60 * 1000,
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    debug: false,
  });
  
  module.exports.pool = pool;
  module.exports.CLIENT_SECRET = env.CLIENT_SECRET;
  module.exports.CLIENT_ID = env.CLIENT_ID;
  module.exports.ARN = env.ARN;
    