'use strict';
const util = require('util');
const { pool } = require('./config');
const get_connection = util.promisify(pool.getConnection).bind(pool);
const dns = require('dns');

// starts here
exports.cronDomainCheck = async () => {

  const connection = await get_connection();
  try {
    const query = util.promisify(connection.query).bind(connection);
    
    
    let domainAll = await query(`select * from domain where status = 0`);

    if(domainAll.length > 0) {

      let promises = domainAll.map(async (d) => {
        let domainName = d.domain_name;
        let keyValue = d.verification_key;
        let domainArray =[];

        let txtValues = await checkDomain(domainName);
        if(txtValues.length > 0) {
          for (let i = 0; i < txtValues.length; i++) {
            domainArray.push(txtValues[i][0]);
          }
          if(domainArray.length > 0) { 
              if(keyValue && domainArray.includes(keyValue)) {
                await query(`update domain set status = 1 where domain_name = ?`,[domainName]);
              } else {
               console.log("verification key not in found"); 
            }
          } else {
            console.log("TXT final array empty");
          }
        } else {
          console.log("TXT values are empty for the domain");
        }

      });
      
      await Promise.all(promises);
    }
    console.log("End");
  } catch (error) {
    console.error('SOME ERROR OCCURED');
  } finally {
    connection.release()
  }
}

//check the TXT values in the domain
async function checkDomain(domainName) {
  return new Promise(function (resolve, reject) {
  dns.resolveTxt(domainName, async(err,dValues) => {
    if(err) {
      console.log("err", err);
      let arr=[];
      return resolve(arr);
    }
    if(dValues) {
      console.log("awaitttttt",dValues);
      return resolve(dValues);
    }
  });
  });  
}