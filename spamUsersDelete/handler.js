'use strict';

// include mysql module
const util = require('util');
var db = require('./config').pool;
const getConnection = util.promisify(db.getConnection).bind(db);

module.exports.info = async (event) => {
  event = event.body;
  console.log(event);
  try {
    let limit = event.limit || 0;
    if(limit == 0) {
      return {
        "status": "failure",
        "message": "Please specify limit",
      };
    }
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
   
    
    let selectResp = await query(`SELECT id FROM users WHERE users.lastName="Come In" and firstName like 'Hot Alice%' LIMIT ${limit}`);
    
    console.log(selectResp);
    let promises = selectResp.map(async (user) => {
      let resp = await query(`DELETE users, settings, user_settings FROM users 
      inner join settings on settings.user_id=users.id
      inner join user_settings on user_settings.user_id=users.id 
      WHERE users.id=${user.id}`);
      console.log(resp);
    });
    await Promise.all(promises);

   
      connection.release();
      return {
        "status": "success",
        "message": "success",
      };
  } catch (error) {
    console.error(error);
    return {
      "message": "Some error occured"
    } 
  }
};