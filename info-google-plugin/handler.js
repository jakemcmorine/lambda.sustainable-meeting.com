'use strict';

// include mysql module
const util = require('util');
const AWS = require('aws-sdk');
const hubspot = require('./HubspotService');
const fs = require('fs');
var db = require('./config').pool;
const { sendPluginAccessEmail, sendOrphanUserEmail } = require('./email');
const getConnection = util.promisify(db.getConnection).bind(db);
var { gsCreateWallet } = require('./api');

exports.info = async (event) => {
  console.log(event);
  const email = event.body.Emails[0];
  console.log(email);
  let connection;
  try {
    connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log(email);
    const updatedHsContact = await hubspot.contacts.createOrUpdate(email, {
      "properties": [
        {
          "property": "app_plugin_status",
          "value": 'Active'
        }
      ]
    });
    console.log(updatedHsContact);
    // log the access of plugin in the database
    let logPluginQuery = await query(`INSERT INTO plugin_log (email, last_access)
      VALUES ('${email}', NOW())
      ON DUPLICATE KEY UPDATE email='${email}',
      last_access = NOW()`)

    let userQueryResult = await query(`select * from users where LOWER(email)='${email.toLowerCase()}'`);

    if (userQueryResult.length) {
      const { id, orginasation_id, firstName, status } = userQueryResult[0];
      let resp3 = await query(`select s.*,u1.email from settings s LEFT JOIN users u1 on u1.id = s.user_id where user_id=${orginasation_id}`);
      let info_url = "";
      if (resp3[0]) {
        info_url = resp3[0].info_url
      }
      console.log(updatedHsContact);
      if (status != 'yes' && (id != orginasation_id)) {
        let check = await query(`update users SET status='yes' where LOWER(email)='${email.toLowerCase()}'`);
        console.log("update", check);
        await gsCreateWallet(email.toLowerCase(), resp3[0].email);
      }

     

      return {
        Id: id,
        FirstName: firstName,
        Url: "https://" + info_url,
        message: 'SUCCESS'
        
      };

    } else {
      // let regex = /[a-zA-Z]+\.[a-z]+(?!@)$/;
      // const domainNameIndexStart = email.search(regex);
      // if(!domainNameIndexStart) throw new Error('invalid domain name in email');
      // const domain = email.substring(domainNameIndexStart);
      // console.log("domain");
      // console.log(domain);
      // // check if the domain is already registered in the domain table
      // const domainCheckQuery = 'SELECT domain.userid, domain.user_email, domain.status from domain WHERE domain.domain_name = ?'
      // const domainNameResult = await query(domainCheckQuery, [domain]);
      // console.log("domainNameResult");
      // console.log(domainNameResult);
      // if (domainNameResult[0] && domainNameResult[0].status) {
      //   console.log("domainNameResult if block");
      //   const domainResult = domainNameResult[0];
      //   let emailData = `<p>Hey, someone from your organisation with the email address ${email} accessed the sustainable meetings plugin.
      //   But you are yet to invite them to the platform.<br>
      //   <br>
      //     Please Click <a href ="https://dev-reskin.ss.emvigotechnologies.com/?redirect=inviteOrphanUser?email=${email}">here</a> to Invite user</p>`;
      //   await sendPluginAccessEmail(emailData, domainResult.user_email);
      //   return {
      //     "message": 'not_invited'
      //   }
      // } else {
      //   console.log("domainNameResult else block");
      //   try {
      //     // let sql = 'INSERT INTO orphans (email, pluginType, time) VALUES(?, ?, NOW())';
      //     let sql = `INSERT INTO orphans(email, pluginType, time) VALUES(?, ?, NOW())
      //         ON DUPLICATE KEY UPDATE time = NOW()`;
      //     let orphanUserResult = await query(sql, [email, 'web-outlook']);
      //     console.log(orphanUserResult);
      //     let emailData = `<h1>Someone installed the plugin who is not an IP or a regular user</h1>
      //     <p>Someone with the email address ${email} is installed the plugin but he is neither a user nor an IP in sustainable
      //     meeting.</p> `;
      //     await sendOrphanUserEmail(emailData, 'sustainablemeeting1@gmail.com');
      //     return {
      //       message: 'orphan_user'
      //     }
      //   } catch (error) {
      //     console.error(error);
      //     return {
      //       message: "Some error occured"
      //     }
      //   }
      // }
    }
  } catch (error) {
    console.error(error);
    return {
      "message": "Some error occured"
    }
  } finally {
    connection.release();
  }
};

