'use strict';

const util = require('util');
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
var pool = require('./config').pool;
const stateMachineArn = require('./config').ARN;
const getConnection = util.promisify(pool.getConnection).bind(pool);

function createInvites(obj, meeting_id) {
  let invites = []
  let createdDate = getAllDates();
  for (let i = 0; i < obj.length; i++) {
    let inviteIndiividual = [];
    inviteIndiividual.push(obj[i].firstname)
    inviteIndiividual.push(obj[i].lastname)
    inviteIndiividual.push(meeting_id)
    inviteIndiividual.push(obj[i].email)
    inviteIndiividual.push(obj[i].title || " ")
    inviteIndiividual.push(obj[i].company)
    inviteIndiividual.push(createdDate)
    inviteIndiividual.push(createdDate)
    invites.push(inviteIndiividual);
  }

  //   console.log(obj.invites);
  return invites;
}

const getAllDates = () => {
  let dateVal = new Date().toISOString().
    replace(/T/, ' ').
    replace(/\..+/, '');
  return dateVal;
}

function traverse(obj) {
  let meeting_in = {};
  for (var i in obj) {

    meeting_in[i] = obj[i];

  }
  return meeting_in;
}
function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  console.log("fromTime", t1, "toTime", t2)
  console.log(t1.getTime(), "", t2.getTime());
  var dif = t1.getTime() - t2.getTime();
  console.log("diff", dif)
  var Seconds_from_T1_to_T2 = dif / 1000;
  // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
  // return Seconds_Between_Dates;
  return Seconds_from_T1_to_T2;
}
async function createStepFunction(invites, delay, setting, id) {
  console.log("delayyyyy", delay)
  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    // const stateMachineArn = "arn:aws:states:eu-west-1:255593839762:stateMachine:emailScheduler_sustainable";
    // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev";
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
        "delay_seconds": delay,
        "obj": invites,
        "id": id,
        "setting": setting
      }),
    }).promise();
    return newstateMachineArn;

  } catch (error) {
    throw error;
  }

}

module.exports.meetingCreate = async (event, context, callback) => {

  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);

  event = JSON.parse(event.body);

  var obj = JSON.stringify(event);
  obj = JSON.parse(obj)
  let invitesOr = Object.assign([], obj.invites);
  delete obj.invites;
  delete obj.organiser;
  console.log(event)

  //   if(userDetail)
  let meetingDataParsed = traverse(obj)
  let message = "Duplicate Meeting.";

  let meetingCheck = await query(`SELECT id,outlook_meeting_id FROM meetings WHERE outlook_meeting_id = '${meetingDataParsed.OutlookID}'`);
  console.log(meetingCheck);
  if (meetingCheck[0]) {
    // return  { "code": 0, "message": "Duplicate Meeting.", "Meeting": event };
    var response = {
      "statusCode": 400,
      "headers": {
        "Content-Type": "application/json"
      },
      "body": JSON.stringify({ "code": 0, "message": message, "Meeting": meetingCheck[0] }),
    };
    return response;
  }

  let userDetail = await query(`SELECT users.id, u1.status as user_status, u1.isActive, u1.trialEnd, users.isMailVerified, users.initiativeId, settings.id as setting_id, users.orginasation_id,
    settings.emailSend, settings.hour, settings.minute, settings.exclude_meeting, settings.domain_url, subscriptions.subscriptionId, subscriptions.id as subId
    FROM users LEFT JOIN settings ON settings.user_id = users.orginasation_id 
    LEFT JOIN users u1 on users.orginasation_id = u1.id 
    LEFT JOIN subscriptions on users.orginasation_id = subscriptions.orginasation_id AND subscriptions.status = 'active'
    WHERE
    LOWER(users.email) = '${event.organiser.email.toLocaleLowerCase()}'`)
  console.log(userDetail);
  console.log("meetingDataParsed", meetingDataParsed);
  let mailTimes;
  if (userDetail[0]) {

    //checking duplicate meeting using subject,meeting start and user id
    try {
      if (meetingDataParsed.subject && meetingDataParsed.start && userDetail[0].id) {
        let meetingCheck2 = await query(`SELECT id,outlook_meeting_id FROM meetings WHERE subject = '${meetingDataParsed.subject}' AND start_date = '${meetingDataParsed.start}' AND user_id = ${userDetail[0].id}`);
        if (meetingCheck2[0]) {
          var response = {
            "statusCode": 400,
            "headers": {
              "Content-Type": "application/json"
            },
            "body": JSON.stringify({ "code": 0, "message": message, "Meeting": meetingCheck2[0] }),
          };
          return response;
        }
      }
    } catch (error) {
      console.log("Some error occurred at duplicate meeting check");
    }
    

    /* For meetings configured after trialEnd, check whether subscription exists 
       Do we need to consider TimeZoneOffset? */
    let meetingStart = new Date(meetingDataParsed.start);
    let trialEnd = new Date(userDetail[0].trialEnd);
    console.log("meeting Start Date", meetingStart);
    console.log("trial End Date", trialEnd);
    console.log("offset", new Date().getTimezoneOffset());
    let meetingSeconds = findSeconds(new Date().toISOString(), meetingDataParsed.start);
    let trialSeconds = findSeconds(new Date().toISOString(), userDetail[0].trialEnd);

    console.log("meetingSeconds", meetingSeconds);
    console.log("trialSeconds", trialSeconds);

    if(meetingSeconds > trialSeconds) {
      console. log("check using seconds - meeting after trial end")
    }
   
    
    let addHourinToSecond = userDetail[0].hour * 60 * 60;
    let addMinuteinToSecond = userDetail[0].minute * 60;


    if (userDetail[0].emailSend == 'After') {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), event.end);
      mailTimes = mailTimes + totalSeconds;
    } else {
      let totalSeconds = addHourinToSecond + addMinuteinToSecond;
      mailTimes = findSeconds(new Date().toISOString(), event.start);
      mailTimes = mailTimes - totalSeconds;
    }
    console.log("after delay calc", mailTimes);
    
    let isCountable = false;
    
    const firstDate = new Date().setHours(12, 0, 0, 0);
    const secondDate = new Date(userDetail[0].trialEnd);
    let user_status = userDetail[0].user_status;
    /* Check-1 Change status to active if the user is inside trial period */
    if (secondDate > firstDate) {
      user_status = 'active'
    }

    /* Check-2 Checking IP status and enabling countable flag */
    if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive') {
      isCountable = true
    }

    /* Check-3 If the meeting time is after trial end, check whether subscription exists, to count it as SM */
    if (meetingStart > userDetail[0].trialEnd) {
      console.log("meeting after trial period");
      /* If no active subscription is present */
      if (!userDetail[0].subscriptionId) {
        message = " No Active Subscription"
        console.log("No subscription");
        /* This meeting is not a Sustainable Meeting. So change countable to false */
        isCountable = false;
      // return;
      }
    }
    /* Check-4 Check if the IP is verified */
    if (userDetail[0].isMailVerified	== 0) {
      /* Not an SM */
      console.log("IP's email is not verified yet");
      isCountable = false;
      message = "IP's email is not verified yet "
    }

    /* Check-5 Check if IP's account is active */
    if(userDetail[0].isActive == 0) {
      console.log("IP has deactivated his account.");
      isCountable = false;
      message = "IP has deactivated his account";
    }

    try {
      let optOutVal = [];
      if (invitesOr.length) {
        optOutVal = invitesOr.filter(function (listItem) {
                return listItem.email == 'opt-out@sustainably.run';
          // return listItem.email == 'optout@yopmail.com';
          });
      }
      if (optOutVal.length > 0) {
        isCountable=false;
      }

      invitesOr = invitesOr.filter(function (listItem) {
        return listItem.email != 'opt-out@sustainably.run';
        // return listItem.email != 'optout@yopmail.com';
      });
    } catch (error) {
      console.log(err);
    }
    
      // console.log(arn)
    let meeting_info_array = [userDetail[0].id, userDetail[0].initiativeId, new Date(), new Date(), userDetail[0].orginasation_id,
    meetingDataParsed.subject, meetingDataParsed.location, meetingDataParsed.type, meetingDataParsed.start,
      meetingDataParsed.end, meetingDataParsed.AllDay, meetingDataParsed.TimeZoneOffset, meetingDataParsed.TimeZoneName, isCountable, meetingDataParsed.OutlookID];
    let meeting_info_query = `INSERT INTO meetings(user_id,initiativeId,createdAt,updatedAt,org_id,subject,location,type,
            start_date,end_date,all_Day,TimeZoneOffset,TimeZoneName,countable,outlook_meeting_id) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    try {
      let resp = await query(meeting_info_query, meeting_info_array)
      console.log(invitesOr)
      let inviters = createInvites(invitesOr, resp.insertId)

      

      // const twodays_inseconds = 60 * 60;
      const twodays_inseconds = 60 * 60 * 48 * 1000;
      let total_seconds = findSeconds(new Date().toISOString(), event.start);

      console.log("=================");
      console.log(userDetail[0].user_status);
      console.log("=================");

      //add meeting creat related data to userDetails
      userDetail[0].startTime = meetingDataParsed.start
      userDetail[0].TimeZoneOffset = meetingDataParsed.TimeZoneOffset
      userDetail[0].TimeZoneName = meetingDataParsed.TimeZoneName
      

      if (total_seconds < twodays_inseconds) {
        if (user_status != 'inactive' && userDetail[0].user_status != 'force_inactive' && isCountable == true)  {
          let arn = await createStepFunction(event.invites, mailTimes, userDetail[0], resp.insertId)
          let newstateMachineArnUpdate = await query(`update meetings SET arnName='${arn.executionArn}'  
                       where id=${resp.insertId}`)
        }
      }

      event['MeetingId'] = resp.insertId;
      let resp2 = await query(`INSERT INTO meeting_invites (first_name,last_name,meeting_id,email, title ,company,	createdAt, updatedAt) VALUES ?`, [inviters])
      //  let resp3=await query(`INSERT INTO mfc_meeting_organiser (first_name, last_name,email_address,title ,company) VALUES(?,?,?,?,?)`,meetingDataParsed.organiser)
      //console.log(resp)
    } catch (err) {
      console.log(err)
    }
    connection.release();
    // connection.end();

    let statusCode = 400;
    if (isCountable){
      statusCode = 200;
      message = "The Meeting has been created.";
    }
    // return { "code": 0, "message": "The Meeting has been created.", "Meeting": event }
    let responseBody = { "code": 0, "message": message, "Meeting": event };
    var response = {
      "statusCode": statusCode,
      "headers": {
        "Content-Type": "application/json"
      },
      "body": JSON.stringify(responseBody),
    };
    return response;
  }else{
    let responseBody = { "code": 0, "message": "No User Found", "Meeting": event };
    var response = {
      "statusCode": 400,
      "headers": {
        "Content-Type": "application/json"
      },
      "body": JSON.stringify(responseBody),
    };
    return response;
  }

  // connection.end();
  // console.log("out********");
  // console.log(resp)
};