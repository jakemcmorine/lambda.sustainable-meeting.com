'use strict';
const aws = require('aws-sdk');
const { calculate, calculateZero, rewards, updateRewards } = require('./model');
const { SQS:sqsURL } = require('./config');
const sqs = new aws.SQS({ apiVersion: '2012-11-05' });
const util = require('util');
const sendMessage = util.promisify(sqs.sendMessage).bind(sqs);
module.exports.invoice = async () => {
  try {
    // console.log(event, '**********', process.env, new Date());
    let list = await calculate();
    let group = groupByOrg(list);
    console.log('***Main ', group, typeof(group), JSON.stringify(group));
    let orgIds = [];
    for (const element in group) {
      orgIds.push(parseInt(element));
      await sendSQS(group[element]); 
    }
    if(orgIds.length) {
      console.log("orgIds for Normal Invoice", orgIds);
    }
      // await calculateRewards(orgIds, group);

    console.log('on controller', group);
    // Calculating Zero bill... 
    let listZero = await calculateZero(orgIds);
    let groupZero = groupByOrg(listZero);
    orgIds = [];
    console.log('***Zero Calculation ', groupZero, typeof(groupZero), JSON.stringify(groupZero));
    for (const element in groupZero) {
      // orgIds.push(element);
      await sendSQS(groupZero[element]);
    }
    // if(orgIds.length)
    //   await calculateRewards(orgIds, groupZero);

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};

function groupByOrg(list) {
  console.log('Group ', list);
  var group = list.reduce(function (obj, item) {
    obj[item.orginasation_id] = obj[item.orginasation_id] || [];
    obj[item.orginasation_id].push(item);
    return obj;
  }, {});
  return group;
}

async function sendSQS(item) {
  try {
    console.log('Befor Push to SQS item', item);
    let email = item[0].email;
    let body = JSON.stringify({email:email, list:item, rewards:item.rewards });
    console.log('Befor Push ** Body', body);
    var params = {
      MessageBody: body,
      QueueUrl: sqsURL,
    };
    console.log('param', params);
    let res = await sendMessage(params);
    console.log('Pushed **', item, res);
  } catch (err) {
    console.log('Error', err);
  }
}

// async function calculateRewards(ids, group){
//   console.log('\n group \n***', group);
//   let res =  await rewards(ids);
//   res = JSON.parse(JSON.stringify(res));
//   let updateIds = [];
//   for(let el in group){
//     let arr = [];
//     console.log(group[el]);
//     let length = group[el].reduce(((t, i)=> t + parseInt(i.trees)),0 );
//     console.log(length);
//     for(let i=0; i<res.length && length >1; i++){
//       if(res[i].org_id == el){
//         arr.push(res[i]);
//         updateIds.push(res[i].id);
//         length--; 
//       }
//     }
//     group[el]['rewards']=arr;
//     console.log('\n ****EL in Loop \n', el, '\n new Grp El', group[el]);
//     await sendSQS(group[el]); 
//   }
//   console.log('rewards ', res, updateIds,'\n New Group', group);
//   if(updateIds.length)
//     await updateRewards(updateIds);

//   return true;
// }