'use strict';
const { processPaymentEvent, processMandateEvent } = require('./model');

exports.eventReceived = async event => {
  try {
    console.log('WebHooks received \n', event.body, '\n End of Webhooks');
    let body = JSON.parse(event.body);
    if (body) {
      let allData = body.events;
      for(let event of allData) {
        if (event.resource_type === 'payments') {
          await processPaymentEvent(event);
        } else if (event.resource_type === 'mandates') {
          await processMandateEvent(event);
        }
          
      }
      // let promises = body.events.map(async (event) => {
      //   if (event.resource_type === 'payments')
      //     return await processPaymentEvent(event);
      //   else if (event.resource_type === 'mandates')
      //     return await processMandateEvent(event);
      // });
      // await Promise.all(promises);
      return {
        statusCode: 200,
        body: 'events processed'
      };
    } else {
      return {
        statusCode: 400,
        body: 'invalid request'
      };
    }
  } catch (err) {
    console.log('error ', err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: err
      })
    };
  }
};

