const mysql = require("mysql");
const config = {
  dev: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "dev-sustainable",
    DB_PASSWORD: "PoRwge4xZSxmJEla",
    DB_NAME: "dev-sustainable-meeting",
    AUTH_URL: "https://dev-auth-ss.emvigotechnologies.com",
    // SQS: "https://sqs.eu-west-1.amazonaws.com/255593839762/sustainable-invoice",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/sustainable-invoice",
    PRICE: 0.99,
    TAX: 20,
    // PAYURL:'https://api.sandbox.paypal.com/v1/billing/subscriptions/',
    // PAYUSER:'AY5uIzXWo09URWlwHduI62x3MqiItO0kHyhFIJFzGc77h7d9SkosbX5HoGT8Pf9O8m5JAj7Bg4C_pO6N',
    // PAYPASS:'EA3eCRmrvd6NP1qqwLEAGluaYQ4eyL6heN-QC3AvtsevWwed8_HBrr_a-3YYCvMy6NZKbMTcm2fslj6r',
    // PLANID: 'P-7WV34194NR157191DLYUTSAY',
    REMAIL: "invoice-dev@sustainably.run",
    GOCARDLESS_URL: "https://api-sandbox.gocardless.com/",
    GOCARDLESS_TOKEN: "sandbox_WyfqKHjjrnll7N0V0Nd28h0pUON_Bs5etYVSBEOU",
    SITE_UTL: "https://dev-reskin.ss.emvigotechnologies.com/",
    INVOICE_DAYS: 2,
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4"
  },
  prod: {
    DB_HOST:
      "sustainable-meeting-prod-instance-1.czsinchz0y1t.eu-west-1.rds.amazonaws.com",
    DB_USER: "sustainable-user",
    DB_PASSWORD: "e1908DnwFs7HzXZJ",
    DB_NAME: "sustainable-meeting",
    AUTH_URL: "https://auth2-meetings.sustainably.run",
    SQS: "https://sqs.eu-west-1.amazonaws.com/574877062696/sustainable-meeting",
    PRICE: 0.99,
    TAX: 20,
    REMAIL: "meetings@sustainably.run",
    GOCARDLESS_URL        : "https://api.gocardless.com/",
    GOCARDLESS_TOKEN      : "live_uaeyuv4d-46z8iVyI6yLAAtTvAiEcRYAeuQ-s_re",
    SITE_UTL: "https://meetings.sustainably.run/",
    INVOICE_DAYS: 30,
    HUBSPOT_API_KEY: "2f5a0715-c458-4862-87f0-3971d81c57d4"
  },
};

let env = config[process.env.stage];
exports.env = env;
var pool = mysql.createPool({
  connectionLimit: 1000,
  connectTimeout: 60 * 60 * 1000,
  acquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  host: env.DB_HOST,
  user: env.DB_USER,
  password: env.DB_PASSWORD,
  database: env.DB_NAME,
  debug: false,
});

module.exports.config = env;
module.exports.pool = pool;
module.exports.SQS = env.SQS;
module.exports.price = env.PRICE;
module.exports.tax = env.TAX;
// module.exports.payURL = env.PAYURL;
// module.exports.payUser = env.PAYUSER;
// module.exports.payPass = env.PAYPASS;
// module.exports.planId = env.PLANID;
module.exports.reEmail = env.REMAIL;
module.exports.goCardlessURL = env.GOCARDLESS_URL;
module.exports.goCardlessToken = env.GOCARDLESS_TOKEN;
module.exports.websiteUrl = env.SITE_UTL;
module.exports.interval = env.INVOICE_DAYS;
