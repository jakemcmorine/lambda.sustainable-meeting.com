const db = require('./config').pool;
const util = require('util');
const getConnection = util.promisify(db.getConnection).bind(db);
const handler = require('./handler');
const { config, price, tax, reEmail, interval } = require('./config');
const aws = require('aws-sdk');
const ses = new aws.SES();
const { getMandateIdOfPayment } = require('./api');
const hubspot = require("./hubspot");

const updateInvoicedStatus = async function (query, subscriptionId) {
  try {
    let sql = '';
    if (subscriptionId) {
      sql = `UPDATE tree_counts SET invoiced = 1 WHERE subscriptionId = '${subscriptionId}' AND invoiced = 0`;
    } else {
      sql = `UPDATE tree_counts
        LEFT JOIN subscriptions ON subscriptions.subscriptionId = tree_counts.subscriptionId
        SET invoiced = 1 
        WHERE subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE() AND 
        tree_counts.date BETWEEN(CURDATE() - INTERVAL ${interval} DAY) AND (CURDATE() - INTERVAL 1 DAY)
        AND tree_counts.invoiced = 0`;
    }
    console.log(" Update invoice accounted query", sql);
    let result = await query(sql);
    console.log("*** Updated invoiced status ***\n", result);
  } catch (error) {
    console.error("Error in updating invoices status", error);
  }
}
module.exports.calculate = async function () {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    
    // let sql = `SELECT
    // org.id,
    // org.firstName,
    // org.lastName,
    // org.email,
    // org.phone,
    // subscriptions.orginasation_id,
    // subscriptions.subscriptionId,
    // subscriptions.orderId,
    // subscriptions.createdAt,
    // CURDATE() AS invoiceDate,
    // DATE_FORMAT(
    //   case when ((CURDATE() - INTERVAL ${interval} DAY) > subscriptions.createdAt)
	  //   then (CURDATE() - INTERVAL ${interval} DAY)
    //   else subscriptions.createdAt END, '%Y-%m-%d') AS startDate,
    // DATE_FORMAT(CURDATE(), '%Y-%m-%d') AS endDate,
    // DATE_FORMAT(emailsent_date, '%Y-%m-%d') AS MeetingsOn,
    // COUNT(meeting_invites.id) AS trees
    // FROM
    //     subscriptions
    // LEFT JOIN users AS org
    // ON
    //     org.id = subscriptions.orginasation_id
    // LEFT JOIN meetings ON meetings.org_id = subscriptions.orginasation_id
    // LEFT JOIN meeting_invites ON meeting_invites.meeting_id = meetings.id
    // WHERE
    //     subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE() AND 
    //     emailsent_date BETWEEN(CURDATE() - INTERVAL ${interval} DAY) AND CURDATE() AND emailsent_date > trialEnd 
    //     AND emailsent_date > subscriptions.createdAt
    // GROUP BY
    //     subscriptions.orginasation_id, DATE_FORMAT(emailsent_date, '%Y-%m-%d')`;
    // console.log(sql);
    // let result = await query(sql);
    // result = JSON.parse(JSON.stringify(result));
    // console.log("*** Old query result ***\n", result);
    //------------------------------------------------------------------------------------------
    /*** NEW QUERY using the tree_counts table  START */
    let newQuery = `SELECT
    org.id,
    org.firstName,
    org.lastName,
    org.email,
    org.phone,
    subscriptions.orginasation_id,
    subscriptions.subscriptionId,
    subscriptions.orderId,
    subscriptions.createdAt,
    CURDATE() AS invoiceDate,
    DATE_FORMAT(
      case when ((CURDATE() - INTERVAL ${interval} DAY) > subscriptions.createdAt)
	    then (CURDATE() - INTERVAL ${interval} DAY)
      else subscriptions.createdAt END, '%Y-%m-%d') AS startDate,
    DATE_FORMAT(CURDATE() - INTERVAL 1 DAY, '%Y-%m-%d') AS endDate,
    DATE_FORMAT(tree_counts.date, '%Y-%m-%d') AS MeetingsOn,
    SUM(tree_counts.trees) AS trees
    FROM
        subscriptions
    LEFT JOIN users AS org ON org.id = subscriptions.orginasation_id
    LEFT JOIN tree_counts ON tree_counts.subscriptionId = subscriptions.subscriptionId
    WHERE
        subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE() AND 
        tree_counts.invoiced = 0 AND
        tree_counts.date BETWEEN(CURDATE() - INTERVAL ${interval} DAY) AND (CURDATE() - INTERVAL 1 DAY)
    GROUP BY tree_counts.org_id, tree_counts.date`;
    console.log("***New Query ***", newQuery);
    let newResult = await query(newQuery);
    newResult = JSON.parse(JSON.stringify(newResult));
    console.log("*** New query result ***\n", newResult);
    /** NEW QUERY using tree_counts table END */
    //------------------------------------------------------------------------------------------
    console.log("Calling updating tree count invoice status");
    await updateInvoicedStatus(query, null);
    console.log("After calling updating tree count invoice status");
    connection.release();
    return newResult;
  } catch (err) {
    console.log(err);
  }
};
module.exports.calculateZero = async function (orgIds) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    
    // let sql = `SELECT
    // org.id,
    // org.trialEnd,
    // org.firstName,
    // org.lastName,
    // org.email,
    // org.phone,
    // subscriptions.orginasation_id,
    // subscriptions.subscriptionId,
    // subscriptions.createdAt,
    // CURDATE() AS invoiceDate,
    // DATE_FORMAT(
    //   case when ((CURDATE() - INTERVAL ${interval} DAY) > subscriptions.createdAt)
	  //   then (CURDATE() - INTERVAL ${interval} DAY)
    //   else subscriptions.createdAt END, '%Y-%m-%d') AS startDate,
    // DATE_FORMAT(CURDATE(), '%Y-%m-%d') AS endDate,
    // DATE_FORMAT(emailsent_date, '%Y-%m-%d') AS MeetingsOn,
    // IFNULL(
    //   MAX(emailsent_date),
    //   org.trialEnd) AS maxDate,
    //   0 AS trees
    // FROM
    //   subscriptions
    // LEFT JOIN users AS org
    //   ON org.id = subscriptions.orginasation_id
    // LEFT JOIN meetings ON meetings.org_id = subscriptions.orginasation_id
    // LEFT JOIN meeting_invites ON meeting_invites.meeting_id = meetings.id
    // WHERE
    //   subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE()
    // GROUP BY
    //   subscriptions.orginasation_id
    // HAVING
    //   DATE_FORMAT(maxDate, '%Y-%m-%d') < (CURDATE() - INTERVAL ${interval} DAY) OR 
    //   DATE_FORMAT(maxDate, '%Y-%m-%d') = DATE_FORMAT(org.trialEnd, '%Y-%m-%d') OR 
    //   maxDate < subscriptions.createdAt`;
    // console.log(sql);
    // let result = await query(sql);
    // result = JSON.parse(JSON.stringify(result));
    // console.log("old query result", result);
    let newQuery = `SELECT
    org.id,
    org.trialEnd,
    org.firstName,
    org.lastName,
    org.email,
    org.phone,
    subscriptions.orginasation_id,
    subscriptions.subscriptionId,
    CURDATE() AS invoiceDate,
    subscriptions.createdAt,
    DATE_FORMAT(
      case when ((CURDATE() - INTERVAL ${interval} DAY) > subscriptions.createdAt)
	    then (CURDATE() - INTERVAL ${interval} DAY)
      else subscriptions.createdAt END, '%Y-%m-%d') AS startDate,
    DATE_FORMAT(CURDATE() - INTERVAL 1 DAY, '%Y-%m-%d') AS endDate,
    tree_counts.date AS MeetingsOn,
    IFNULL(
      MAX(tree_counts.date),
      org.trialEnd) AS maxDate,
      0 AS trees
    FROM
      subscriptions
    LEFT JOIN users AS org
      ON org.id = subscriptions.orginasation_id
    LEFT JOIN tree_counts ON tree_counts.subscriptionId = subscriptions.subscriptionId
    WHERE
      subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE()
    GROUP BY
      subscriptions.orginasation_id
    HAVING
      DATE_FORMAT(maxDate, '%Y-%m-%d') < (CURDATE() - INTERVAL ${interval} DAY) OR 
      DATE_FORMAT(maxDate, '%Y-%m-%d') = DATE_FORMAT(org.trialEnd, '%Y-%m-%d')`;
    let newResult = await query(newQuery);
    newResult = JSON.parse(JSON.stringify(newResult));
    console.log("*** Zero Query Result ***", newResult);
    /*------------------------------------------------------------------------------- */
    // try {
      let zeroOrgs = `SELECT
      org.id,
      org.trialEnd,
      org.firstName,
      org.lastName,
      org.email,
      org.phone,
      subscriptions.orginasation_id,
      subscriptions.subscriptionId,
      subscriptions.createdAt,
      CURDATE() AS invoiceDate,
      DATE_FORMAT(
        case when ((CURDATE() - INTERVAL ${interval} DAY) > subscriptions.createdAt)
        then (CURDATE() - INTERVAL ${interval} DAY)
        else subscriptions.createdAt END, '%Y-%m-%d') AS startDate,
      DATE_FORMAT(CURDATE() - INTERVAL 1 DAY, '%Y-%m-%d') AS endDate,
      0 as trees
      FROM
        subscriptions
      LEFT JOIN users AS org
        ON org.id = subscriptions.orginasation_id
      WHERE
        subscriptions.status = 'active' AND subscriptions.billingDate = CURDATE() 
        ${orgIds.length ? ` AND subscriptions.orginasation_id NOT IN (${orgIds}) ` : " "}
      GROUP BY
        subscriptions.orginasation_id`;
        
    console.log("query------------------\n", zeroOrgs);
    let result = await query(zeroOrgs);
    result = JSON.parse(JSON.stringify(result));
    console.log("New query result -------->", result);
    // } catch (error){
    //   console.log("Error in new zero invoice query", error);
    // }
    /*------------------------------------------------------------------------------- */

    connection.release();
    return result;
  } catch (err) {
    console.log(err);
  }
};

module.exports.insert = async function (data) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log(data);
    console.log('connected as id ' + connection.threadId);
    let sql = 'INSERT INTO invoices (invoiceNumber, orginasation_id, invoiceDate, periodStartDate, periodEndDate, numberOfTrees, amount, status, paymentId, payNow) VALUES ?';
    console.log(sql, data);
    let result = await query(sql, [[data]]);
    connection.release();
    console.log('Insert successf ull', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports.savePayment = async function (data) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    let sql = 'INSERT INTO payments (subscriptionId,payId, state, amount, currency, invoiceNumber, fullResponse) VALUES ?';
    console.log(sql, data);
    let result = await query(sql, [[data]]);
    connection.release();
    console.log('Payment Insert successfull', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
module.exports.cancelSubscription = async function (data) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    let sql = 'UPDATE subscriptions SET status = ? WHERE subscriptionId = ?';
    console.log('Query', sql, data);
    let result = await query(sql, data);
    connection.release();
    console.log('Subscription Updated', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
module.exports.updateInvoice = async function ([subscriptionId, amount]) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    let sql = 'SELECT orginasation_id FROM `subscriptions` WHERE subscriptionId = ?';
    //let sql = "UPDATE subscriptions SET status = ? WHERE subscriptionId = ?";
    // console.log("Query", sql, data);
    let org = await query(sql, [subscriptionId]);
    org = JSON.parse(JSON.stringify(org));
    console.log(org);
    let sqlUpdate = 'UPDATE invoices SET status = \'paid\' WHERE orginasation_id = ? and amount=? and status=\'processing\' ORDER BY id DESC LIMIT 1';
    console.log('Query', sqlUpdate, [org[0].orginasation_id, amount]);
    let result = await query(sqlUpdate, [org[0].orginasation_id, amount]);
    connection.release();
    console.log('Invoice Updated', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
module.exports.updateBillingDate = async function (subscriptionId) {
  try {
    // console.log('called', subscriptionId);
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    let sqlUpdate = `UPDATE subscriptions SET cycleStartDate = CURDATE(), billingDate = (CURDATE() + INTERVAL ${interval} DAY ) WHERE subscriptionId = ?`;

    let result = await query(sqlUpdate, [subscriptionId]);
    connection.release();
    console.log('subscription Updated', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports.rewards = async function (ids) {
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let sql = 'SELECT * FROM `rewards` WHERE org_id in (?) and status=\'0\' ';

  let result = await query(sql, [ids]);
  connection.release();
  console.log('Rewards', JSON.stringify(result));
  return result;
};
module.exports.updateRewards = async function (ids) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    let sql = 'UPDATE rewards SET status = \'1\' WHERE id IN (?)';
    console.log('Query', sql, ids);
    let result = await query(sql, [ids]);
    connection.release();
    console.log('Rewards Updated', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
module.exports.insertFreeTree = async function (message, org_id) {
  try {
    let connection = await getConnection();
    const query = util.promisify(connection.query).bind(connection);
    console.log('connected as id ' + connection.threadId);
    let sql = 'INSERT INTO rewards (org_id,message, num_trees) VALUES (?)';
    console.log('Free Tree ', sql, message, org_id);
    let result = await query(sql, [[org_id, message, 1]]);
    connection.release();
    console.log('Free Tree Reward Inserted successfully', JSON.stringify(result));
    return true;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports.processPaymentEvent = async (event) => {
  console.log('timedate start', new Date());
  console.log('PROCESSING PAYMENT', event);
  let connection = await getConnection();

  try {

  const query = util.promisify(connection.query).bind(connection);
  let paymentId = event.links.payment;

  // set status as 'paid' when payment is paid out or confirmed
  let isPaid = ['confirmed', 'paid_out'].some(action => action === event.action);
  if (isPaid) {
    console.log("Paid status -- calline green stand api");
    await updateGreenStandInvoiceStatus(paymentId, query);
    return await updatePaymentStatus('paid', paymentId, query);
  }

  // set status as 'pending' when payment is failed or cancelled
  let isFailed = ['failed', 'cancelled'].some(action => action === event.action);
      if (isFailed) {
        // cancel subscription of this initiative partner
        console.log("innnnnnnnnn")
        await updatePaymentStatus('payment_failed', paymentId, query);
        await this.sendFailedInvoiceEmail(paymentId, query);
        
        let results = await query('SELECT * FROM invoices WHERE paymentId = ?', paymentId);
        let organizationId = results[0].orginasation_id;
        results = await query('SELECT subscriptionId FROM subscriptions WHERE status = "active" AND orginasation_id = ?', [organizationId]);
        if (results[0]) {
          let mandateId = results[0].subscriptionId;
          results = await query('SELECT * FROM users WHERE id = ?', [organizationId]);
          let email = results[0].email;
          await this.updateSubscriptionStatus('cancelled', organizationId, query);
          console.log('SUBSCRIPTION CANCELLED FOR THE INITIATIVE PARTNER');
          // await this.sendCancelMail(email);
          console.log("Sending subscription cancel mail from hubspot");
          await hubspot.contacts.createOrUpdate(email, {
            properties: [
              {
                property: 'subscription_status',
                value: 'cancelled'
              }
            ]
          })

          // generate and send an invoice to the initiative partner
          let invoiceData = await this.generateInvoiceData(mandateId, query);
          console.log('INVOICE DATA' + JSON.stringify(invoiceData));
          invoiceData = invoiceData.filter(data => (parseInt(data.trees) != 0))

          console.log('INVOICE DATA' + JSON.stringify(invoiceData));
          if (invoiceData.length !== 0) {
            // invoiceData = invoiceData[0];
            invoiceData = {
              email: invoiceData[0].email,
              list: invoiceData
            };
            console.log(invoiceData);
            await this.insertInvoice(JSON.stringify(invoiceData), '', "yes");
            let emailParams = await handler.generateEmailParams(JSON.stringify(invoiceData), true);
            console.log(emailParams);
            await ses.sendEmail(emailParams).promise();
          }
        }
      }
    } catch (error) {
      console.log("Error in processing payment event from Webhook");
      throw error;
    } finally {
      console.log('timedate end', new Date());
      connection.release();
    }
  
};

module.exports.updateSubscriptionStatus = async (status, organizationId, query) => {
  let updateSubscriptionQuery = 'UPDATE subscriptions SET status = ? WHERE orginasation_id = ?';
  // let connection = await getConnection();

  // const query = util.promisify(connection.query).bind(connection);
  let result = await query(updateSubscriptionQuery, [status, organizationId]);
  /* On cancelling the subscription, set the user status to 'inactive' */
  if (status == 'cancelled') {
    const userquery = `UPDATE users SET status = 'inactive' WHERE id = ?`;
    let userResult = await query(userquery, [organizationId]);
    console.log(">>>>>>>>> Updated user status to inactive <<<<<<<<<");
  }
  return result;
};

const updatePaymentStatus = async (status, paymentId, query) => {
try {
  console.log("paiddddddddd-updatePaymentStatus");
  let payNow= 'no', paidDate = null;
  if(status == 'payment_failed') {
    payNow = 'yes';
  }
  if(status == 'paid') {
    paidDate = new Date();
  }
  let updatePaymentStatusQuery = 'UPDATE invoices SET status = ?, payNow = ?, invoicePaidDate = ? WHERE paymentId = ?';
  // let connection = await getConnection();
  // const query = util.promisify(connection.query).bind(connection);
  let result = await query(updatePaymentStatusQuery, [status, payNow, paidDate, paymentId]);
  // connection.release();
  return result;
} catch(error) {
    console.log("error update", error);
    throw error;
}
 
};

module.exports.processMandateEvent = async (event) => {
  console.log('PROCESSING MANDATES');
  let mandateId = event.links.mandate;
  console.log('mandate Id', mandateId);
  let mandateFailed = ['cancelled', 'failed', 'expired'].some(action => event.action === action);
  if (mandateFailed) {
    let connection = await getConnection();
    try {
      // cancel subscription
      const query = util.promisify(connection.query).bind(connection);
      let results = await query('SELECT status FROM subscriptions WHERE subscriptionId = ?', [mandateId]);
      if (results[0] && results[0].status != "cancelled") {
        let updateMandateStatusQuery = 'UPDATE subscriptions SET status=? WHERE subscriptionId=?';
        let queryResult = await query(updateMandateStatusQuery, ['cancelled', mandateId]);
       
        console.log('update subscription status query result', queryResult);
        let userRes = await query('select u.id, u.firstName, u.lastName, u.email, u.status from users u left join subscriptions s on s.orginasation_id = u.id where s.subscriptionId = ?', [mandateId]);
        console.log("User email get query result ****", userRes);
        /* Set IP's status to 'inactive' in DB */
        const userquery = `UPDATE users SET status = 'inactive' WHERE id = ?`;
        let userResult = await query(userquery, [userRes[0].id]);
        // await this.sendCancelMail(userRes[0].email);
        console.log("Sending subscription cancel mail from hubspot");
        await hubspot.contacts.createOrUpdate(userRes[0].email, {
          properties: [
            {
              property: 'subscription_status',
              value: 'cancelled'
            }
          ]
        })

        // generate and send an invoice
        let invoiceData = await this.generateInvoiceData(mandateId, query);
        console.log('INVOICE DATA' + JSON.stringify(invoiceData));
        invoiceData = invoiceData.filter(data => (parseInt(data.trees) != 0))
        console.log('INVOICE DATA' + JSON.stringify(invoiceData));

        if (invoiceData.length !== 0) {
          // invoiceData = invoiceData[0];
          invoiceData = {
            email: invoiceData[0].email,
            list: invoiceData
            // ]
          };
          console.log(invoiceData);
          await this.insertInvoice(JSON.stringify(invoiceData), '', "yes");
          let emailParams = await handler.generateEmailParams(JSON.stringify(invoiceData), true);
          console.log(emailParams);
          await ses.sendEmail(emailParams).promise();
        }
      }
    } catch (error) {
      console.log("Error in processing Mandate event from Webhook");
      throw error;
    } finally {
      connection.release();
    }
  }

};

exports.insertInvoice = async function (data, paymentId, payNow="no") {
  try {
    const { list, rewards } = JSON.parse(data);
    console.log('L, R', list, rewards);
    // let number = list[0].trees;
    let number = list.reduce((t, i) => t + parseInt(i.trees), 0);

    // let rewardNumber = rewards.reduce(((t, i)=> t + parseInt(i.num_trees)),0 );
    // console.log('Total Number', number, rewardNumber);
    // number -= rewardNumber;
    let startDate = list[0].startDate;
    let endDate = new Date();
    let orgId = list[0].orginasation_id;
    // if(number==0){
    //   await insertFreeTree(`Free tree For Billing cycle  ${startDate} TO ${endDate}`, orgId );
    //   number++;
    // }
    console.log('No of billed Tree', number);
    let invoiceNumber = Math.floor(100000 + Math.random() * 900000);
    let amount = (number * price);
    let taxAmount = (amount * (tax / 100));
    let invoiceStatus = 'processing';
    if(payNow == 'yes') {
      invoiceStatus = 'pending';
    }
    if(number == 0) {
        invoiceStatus = 'none';
    }
    let insertData = [invoiceNumber, orgId, endDate, startDate, endDate, number, (amount + taxAmount).toFixed(2), invoiceStatus, paymentId, payNow];
    await this.insert(insertData);
    console.log('No of trees, P, Tax', number, price, tax);
    return true;
  } catch (error) {
    console.log(error);
    throw new Error('Inserting invoice Faild ! ' + JSON.stringify(error));
  }
};

module.exports.generateInvoiceData = async (subscriptionId, query) => {
  console.log('subscription id in generateInvoiceData', subscriptionId);
  let invoiceQuery = `
  SELECT org.id,
  org.firstName,
  org.lastName,
  org.email,
  org.phone,
  CURDATE() AS invoiceDate,
  subscriptions.orginasation_id,
  subscriptions.subscriptionId,
  subscriptions.createdAt AS subscriptionDate,
  subscriptions.freeTrail,
  subscriptions.endOfFreeTrial,
  case
  when coalesce(latest_invoices.invoiceDate, '1/1/1973') > subscriptions.createdAt
  then latest_invoices.invoiceDate
  else subscriptions.createdAt end as startDate,
  DATE_FORMAT(emailsent_date, '%Y-%m-%d') As MeetingsOn,
  IFNULL(COUNT(meeting_invites.id), 0) As trees,
  meeting_invites.emailsent_date As endDate
  FROM subscriptions
  LEFT JOIN users as org on org.id = subscriptions.orginasation_id
  LEFT JOIN meetings on meetings.org_id = subscriptions.orginasation_id
  LEFT JOIN meeting_invites ON meeting_invites.meeting_id = meetings.id
  left join latest_invoices ON latest_invoices.orginasation_id = subscriptions.orginasation_id
  WHERE subscriptionId = '${subscriptionId}'
  AND trialEnd < DATE_FORMAT(emailsent_date, '%Y-%m-%d') and case
  when coalesce(latest_invoices.invoiceDate, '1/1/1973') > subscriptions.createdAt
  then latest_invoices.invoiceDate
  else subscriptions.createdAt end <= meeting_invites.emailsent_date
  GROUP By MeetingsOn`;

  let newInvoiceQuery = `SELECT org.id,
  org.firstName,
  org.lastName,
  org.email,
  org.phone,
  subscriptions.orginasation_id,
  subscriptions.subscriptionId,
  subscriptions.createdAt AS subscriptionDate,
  CURDATE() AS invoiceDate,
  subscriptions.freeTrail,
  subscriptions.endOfFreeTrial,
  case
  when coalesce(latest_invoices.invoiceDate, '1/1/1973') > subscriptions.createdAt
  then latest_invoices.invoiceDate
  else subscriptions.createdAt end as startDate,
  tree_counts.date As MeetingsOn,
  COALESCE(SUM(tree_counts.trees), 0) As trees,
  MAX(tree_counts.date) As endDate
  FROM subscriptions
  LEFT JOIN users as org on org.id = subscriptions.orginasation_id
  LEFT JOIN tree_counts ON subscriptions.subscriptionId = tree_counts.subscriptionId
  left join latest_invoices ON latest_invoices.orginasation_id = subscriptions.orginasation_id
  WHERE subscriptions.subscriptionId = '${subscriptionId}'
  AND tree_counts.invoiced = 0
  AND case
  when coalesce(latest_invoices.invoiceDate, '1/1/1973') > subscriptions.createdAt
  then DATE_FORMAT(latest_invoices.invoiceDate, '%Y-%m-%d')
  else DATE_FORMAT(subscriptions.createdAt, '%Y-%m-%d') end <= tree_counts.date
  GROUP By MeetingsOn`
  // const connection = await getConnection();
  // const query = util.promisify(connection.query).bind(connection);
  // const results = await query(invoiceQuery);
  const newResult = await query(newInvoiceQuery);
  console.log("**** Result from new query *****", newResult);
  console.log("before calling upate invoied status")
  await updateInvoicedStatus(query, subscriptionId);
  console.log("after calling upate invoied status")
  // connection.release();
  return newResult;
};

module.exports.getConversionRate = async function (currency) {
  let connection = await getConnection();
  const query = util.promisify(connection.query).bind(connection);
  let sql = 'SELECT * FROM `exchange_rates` WHERE `target` = ? ';
  let result = await query(sql, [currency]);
  connection.release();
  console.log('getConversionRate', JSON.stringify(result));
  return result;
};

exports.sendCancelMail = async (email) => {
  if (!email) {
    throw new Error('email required');
  }
  const htmlData = `<div><p>Hi,&nbsp;</p>
  <p>Your subscription with sustainable meeting has been cancelled.</p>
  <p>No further trees will be planted until a new subscription is activated.</p>
  <p>Joining Sustainable Meeting is a smart, effective yet simple way to enhance your Corporate Sustainability
    needs. It sets a differentiator from other companies and you will not only be meeting your Corporate values by
    offsetting the negative environmental impact of your meetings, but you will also be helping to provide income,
    education and food to some of the planet’s poorest communities.&nbsp;</p>
  <p><span style="font-size: 10.0pt;font-family: Arial, sans-serif;">Kind Regards</span></p>
  <p><span style="font-size: 12.0pt;font-family: Arial, sans-serif;">Sustainable Meeting</span> </p></div>`;
  const emailParams = {
    Destination: {
      ToAddresses: [
        email
      ]
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: htmlData
        },
        Text: {
          Charset: 'UTF-8',
          Data: 'Sustainable Meeting subscription cancelled'
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Cancelled subscription with Sustainable Meeting'
      }
    },
    Source: reEmail,
    ReplyToAddresses: [reEmail]
  };
  return ses.sendEmail(emailParams).promise();
};

module.exports.sendFailedInvoiceEmail = async (paymentId, query) => {
  
  try {
    
    let periodStart, periodEnd, startDate, endDate, orgId;
    const invoiceDataQuery = `SELECT * FROM invoices WHERE paymentId = ?`;
    const invoiceData = await query(invoiceDataQuery, paymentId);
    console.log("### Invoice Data ###", invoiceData);
    if (invoiceData && invoiceData[0]) {
      periodStart = invoiceData[0].periodStartDate;
      periodEnd = invoiceData[0].periodEndDate;
      orgId = invoiceData[0].orginasation_id;
      console.log("periodStart, end", periodStart, periodEnd);
      startDate = formatDate(periodStart);
      endDate = formatDate(periodEnd);
      console.log("startDate, endDate, orgId", startDate, endDate, orgId);
      /* Get subscriptionId from Gocardless for this payment */
      let subscriptionId = await getMandateIdOfPayment(paymentId);
      console.log("subscription Id from Gocardless", subscriptionId);
      // const failedInvoiceQuery = `SELECT
      // org.id,
      // org.firstName,
      // org.lastName,
      // org.email,
      // org.phone,
      // org.orginasation_id,
      // org.trialEnd,
      // '${startDate}' as startDate,
      // '${endDate}' as endDate,
      // DATE_FORMAT(emailsent_date, '%Y-%m-%d') AS MeetingsOn,
      // COUNT(meeting_invites.id) AS trees
      // FROM
      //   users AS org
      // LEFT JOIN meetings ON meetings.org_id = org.orginasation_id
      // LEFT JOIN meeting_invites ON meeting_invites.meeting_id = meetings.id
      // LEFT JOIN subscriptions on subscriptions.orginasation_id = org.orginasation_id
      // WHERE
      //   subscriptions.subscriptionId = '${subscriptionId}'
      //   AND emailsent_date BETWEEN '${startDate}' AND '${endDate}'
      //   AND emailsent_date > trialEnd AND org.orginasation_id= ${orgId}
      //   AND emailsent_date > subscriptions.createdAt
      // GROUP BY
      //   DATE_FORMAT(emailsent_date, '%Y-%m-%d')`;


      // console.log("##### before printing query #####");
      // console.log("query", failedInvoiceQuery);
      // const failedInvoiceData = await query(failedInvoiceQuery);
      // console.log("failed invoice data", failedInvoiceData);
      const failedInvoiceQuery = `SELECT
          org.id,
          org.firstName,
          org.lastName,
          org.email,
          org.phone,
          org.orginasation_id,
          org.trialEnd,
          '${startDate}' as startDate,
          '${endDate}' as endDate,
          CURDATE() AS invoiceDate,
          DATE_FORMAT(tree_counts.date, '%Y-%m-%d') AS MeetingsOn,
          SUM(tree_counts.trees) AS trees
          FROM
          subscriptions 
          LEFT JOIN tree_counts ON tree_counts.subscriptionId = subscriptions.subscriptionId
          LEFT JOIN users AS org on subscriptions.orginasation_id = org.id
          WHERE
            subscriptions.subscriptionId = '${subscriptionId}'
            AND tree_counts.date BETWEEN '${startDate}' AND '${endDate}'
            AND org.id= ${orgId}
          GROUP BY
            tree_counts.date`;
      console.log("##### NEW QUERY #####");
      console.log(failedInvoiceQuery);
      const failedInvoiceData = await query(failedInvoiceQuery);
      console.log("***********new query invoice data***********", failedInvoiceData);

      if (failedInvoiceData.length != 0) {
        let invoiceEmailData = {
          email: failedInvoiceData[0].email,
          list: failedInvoiceData
        };
        // Generate Email and send 
        let emailParams = await handler.generateEmailParams(JSON.stringify(invoiceEmailData), true);
        console.log(emailParams);
        await ses.sendEmail(emailParams).promise();
      }
      // return failedInvoiceData;
    }
    return null;
  } catch (error) {
    console.log("Error in sending failed invoice email");
  }
}
function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

const updateGreenStandInvoiceStatus = async (paymentId, query) => {
  try {
    let invoiceData = await query(`SELECT * FROM invoices WHERE paymentId = '${paymentId}'`);
    if(invoiceData && invoiceData[0]) {
      let orgId = invoiceData[0].orginasation_id;
      let startDate = formatDate(invoiceData[0].periodStartDate);
      let endDate = formatDate(invoiceData[0].periodEndDate);
      console.log("Invoice details", orgId, startDate, endDate);
      let updateQuery = `UPDATE greenstand_tree_tokens SET invoice_status = 1 
      WHERE org_id = ${orgId} AND DATE(createdAt) >=  '${startDate}' AND 
      DATE(createdAt) <= '${endDate}'`;
      console.log("Update query", updateQuery);
      let updateResult = await query(updateQuery);
      console.log("Update Query Result", updateResult);
    } else {
      console.log("No invoice found with the paymentId ", paymentId);
      return;
    }
  } catch (error) {
    console.log("Error in updating Greenstand tokens");
    console.error(error);
  }
}