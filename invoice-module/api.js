const {
  goCardlessURL,
  goCardlessToken
} = require('./config');
const axios = require('axios');


// module.exports.updatePayPalSubscription = async (subId, quantity) => {
//   try {
//     let url = `${payURL}${subId}/revise`;
//     /*  let options = {
//             auth: {
//                 username: payUser,
//                 password: payPass
//             },
//             body: {
//                 "plan_id": "P-1EK39164FU983273ELXYJLSA",
//                 "quantity": quantity
//             }
//         };
//         let res = await rq.post(url, {
//             options
//         }); */

//     let options = {
//       auth: {
//         'user': payUser,
//         'pass': payPass
//       },
//       url:url,
//       headers: {
//         'Accept': 'application/json',
//       },
//       method: 'POST',
//       json: {
//         'plan_id': planId,
//         'quantity': quantity
//       }
//     }; 
//     let res = await rq(options);
//     console.log(' *** ^^^^ Paypal udpated \n', res);
//     console.log(' *** ^^^^ Paypal udpated \n', res.body);
//     return true;
//   } catch (error) {
//     console.log(error);
//     throw error;
//   }
// };


module.exports.initiatePayment = async (subId, userCurrency, amount) => {
  try {
    let paymentId = "";
    let url = `${goCardlessURL}payments`;
    let header = {
      headers: {
        "GoCardless-Version": "2015-07-06",
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + goCardlessToken
      }
    };
    let data = {
      "payments": {
        "amount": parseInt(amount),
        "currency": userCurrency,
        "links": {
          "mandate": subId
        },
        "description": "Subscription Fee"
      }
    };
    console.log(data);
    await axios.post(url, data, header)
      .then(response => {
        console.log(' *** ^^^^ Go Cardless payment response \n', response);
        console.log(' *** ^^^^ Go Cardless payment id \n', response.data.payments.id);
        paymentId = response.data.payments.id;
      })
      .catch(err => {
        console.log("Error in GoCardless API Call");
        console.log(err.response.data);
        console.log(err.response.data.error.errors);
        throw err;
      });
    return paymentId;
  } catch (error) {
    console.log("Error Message from GoCardless API Call:", error);
    throw error;
  }
};

module.exports.getUserCurrency = async (custBankID) => {
  try {
    let bankDetails = "";
    let url = `${goCardlessURL}customer_bank_accounts/${custBankID}`;
    let header = {
      headers: {
        "GoCardless-Version": "2015-07-06",
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + goCardlessToken
      }
    };
    await axios.get(url, header)
      .then(response => {
        console.log(' *** ^^^^ Go Cardless getUserCurrency response \n', response);
        console.log(' *** ^^^^ Go Cardless getUserCurrency currency \n', response.data.customer_bank_accounts.currency);
        bankDetails = response.data.customer_bank_accounts.currency;
      })
      .catch(err => {
        console.log("Error in GoCardless API Call");
        console.log(err.response.data);
        console.log(err.response.data.error.errors);
        throw err;
      });
    return bankDetails;
  } catch (error) {
    console.log("Error Message from GoCardless API Call:", error);
    throw error;
  }
};

module.exports.getMandateIdOfPayment = async (paymentId) => {
  try {
    let mandateId = "";
    let url = `${goCardlessURL}payments/${paymentId}`;
    let header = {
      headers: {
        "GoCardless-Version": "2015-07-06",
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + goCardlessToken
      }
    };
    await axios.get(url, header)
      .then(response => {
        console.log(' *** ^^^^ Go Cardless getMandateId response ^^^^ *** \n', response.data);
        console.log(' *** ^^^^ Go Cardless MandateId from response ^^^^ ***', response.data.payments.links.mandate);
        mandateId = response.data.payments.links.mandate;
      })
      .catch(err => {
        console.log("Error in GoCardless API Call");
        console.log(err.response.data);
        console.log(err.response.data.error.errors);
        throw err;
      });
    return mandateId;
  } catch (error) {
    console.log("Error Message from GoCardless API Call:", error);
    throw error;
  }
};
// {
//   "payments": {
//     "id": "PM0014VRZDGEHV",
//     "created_at": "2020-03-16T12:15:57.787Z",
//     "charge_date": "2020-03-19",
//     "amount": 100,
//     "description": "first payment for customer UK",
//     "currency": "GBP",
//     "status": "pending_submission",
//     "amount_refunded": 0,
//     "reference": null,
//     "metadata": {},
//     "fx": {
//       "fx_currency": "EUR",
//       "fx_amount": null,
//       "exchange_rate": null,
//       "estimated_exchange_rate": "1.10865"
//     },
//     "links": {
//       "mandate": "MD00087300T3BC",
//       "creditor": "CR0000626ASCHT"
//     },
//     "retry_if_possible": false
//   }
// }