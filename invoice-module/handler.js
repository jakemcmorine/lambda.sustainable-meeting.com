"use strict";
const aws = require("aws-sdk");
const ses = new aws.SES();
const { price, tax, reEmail, websiteUrl } = require("./config");
const {
  insert,
  updateBillingDate,
  insertFreeTree,
  getConversionRate,
} = require("./model");
const { initiatePayment, getUserCurrency } = require("./api");

module.exports.generateInvoice = async (event) => {
  try {
    console.log("Event \n", event);
    for (let i = 0; i < event.Records.length; i++) {
      console.log("\n*** Body \n", event.Records[i].body);
      const paymentId = await updateSubscription(event.Records[i].body);
      const invoiceInsert = await this.insertInvoice(
        event.Records[i].body,
        paymentId
      );
      const emailParams = await this.generateEmailParams(event.Records[i].body);
      console.log(paymentId, invoiceInsert);
      if (paymentId != "") {
        const data = await ses.sendEmail(emailParams).promise();
        console.log(data);
      }
    }

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

exports.insertInvoice = async function(data, paymentId) {
  try {
    const { list, rewards } = JSON.parse(data);
    console.log("L, R", list, rewards);
    let number = list.reduce((t, i) => t + parseInt(i.trees), 0);
    // let rewardNumber = rewards.reduce(((t, i)=> t + parseInt(i.num_trees)),0 );
    // console.log('Total Number', number, rewardNumber);
    // number -= rewardNumber;
    let startDate = list[0].startDate;
    let endDate = list[0].endDate;
    let orgId = list[0].orginasation_id;
    let invoiceDate = new Date();
    // if(number==0){
    //   await insertFreeTree(`Free tree For Billing cycle  ${startDate} TO ${endDate}`, orgId );
    //   number++;
    // }
    console.log("No of billed Tree", number);
    let invoiceNumber = Math.floor(100000 + Math.random() * 900000);
    let amount = number * price;
    let taxAmount = amount * (tax / 100);
    let invoiceStatus = "processing";
    if (number == 0) {
      invoiceStatus = "none";
    }
    let insertData = [
      invoiceNumber,
      orgId,
      invoiceDate,
      startDate,
      endDate,
      number,
      (amount + taxAmount).toFixed(2),
      invoiceStatus,
      paymentId,
      "no",
    ];
    await insert(insertData);
    console.log("No of trees, P, Tax", number, price, tax);
    return true;
  } catch (error) {
    // throw new Error('Inserting invoice Faild ! ');
    console.log("Inserting invoice Failed !");
    console.error(error);
    return false;
  }
};

async function updateSubscription(data) {
  try {
    let paymentId = "";
    const { list, rewards } = JSON.parse(data);
    let number = list.reduce((t, i) => t + parseInt(i.trees), 0);
    // let rewardNumber = rewards.reduce(((t, i)=> t + parseInt(i.num_trees)),0 );
    console.log("Num", number);
    // number -= rewardNumber;
    // console.log('After Free Deduction', number);
    // number = (number === 0) ? 1: number;
    let subId = list[0].subscriptionId;
    let custBankId = list[0].orderId;
    if (number > 0) {
      let amount = number * price;
      let taxAmount = amount * (tax / 100);
      let finalAmount = (amount + taxAmount).toFixed(2) * 100;
      let currency = await getUserCurrency(custBankId);
      console.log(`updateSubscription currency ${currency}`);
      console.log(finalAmount);
      if (currency != "GBP") {
        let rateData = await getConversionRate(currency);
        if (rateData) {
          let rate = rateData[0]["rate"];
          finalAmount = finalAmount * parseFloat(rate);
        }
        if (finalAmount < 200 && currency == "AUD") finalAmount = 200;
      }
      if (finalAmount < 200 && currency == "AUD") finalAmount = 200;
      console.log(finalAmount);
      paymentId = await initiatePayment(subId, currency, finalAmount);
      console.log(`Payment Id ${paymentId} in updateSubscription`);
    }
    console.log("Subscription :", number, price);
    await updateBillingDate(subId);
    console.log("Update billing date");
    return paymentId;
  } catch (error) {
    console.log(error);
    // throw new Error('Update Subscription Failed !');
    console.log("Update Subscription Failed !");
    console.error(error);
    return;
  }
}

module.exports.generateEmailParams = async function(body, payNow = false) {
  const { email, list } = JSON.parse(body);
  console.log("Generate Email", email, list, price);
  if (!(email && list)) {
    // throw new Error('Missing parameters! Make sure to add parameters \'email\', \'list\'');
    console.log("ERROR: Email or list not found! Cannot send email!");
    return;
  }
  let htmlBody = generateHtml(list, payNow);
  return {
    Source: reEmail,
    Destination: { ToAddresses: [email] },
    ReplyToAddresses: [reEmail],
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: htmlBody,
        },
        Text: {
          Charset: "UTF-8",
          Data: `Message sent from email ${email} by`,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Sustainably Run Meetings Invoice!",
      },
    },
  };
};

function generateHtml(list, payNow = false) {
  let totalTrees = list.reduce((a, c) => parseInt(c.trees) + a, 0);
  //   let freeTrees =  rewards.reduce(((a, c)=> parseInt(c.num_trees) + a),0);
  //   totalTrees -= freeTrees;
  if (totalTrees == 0) {
    return generateZeroTreeHtml(list);
  }
  if (payNow) {
    return generatePayNowHtml(list);
  }
  let amount = totalTrees * price.toFixed(2);
  let taxAmount = (amount * (tax / 100)).toFixed(2);
  let total = (parseFloat(amount) + parseFloat(taxAmount)).toFixed(2);
  let phone = list[0].phone || "";
  let html = `<!DOCTYPE html>
  <html>
      <head>
          <title>TODO supply a title</title>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
          <style>
              body {
                  font-family: Arial, Helvetica, sans-serif; }
  
              .invoice-wrp {
                  width: 595px;
              }
  
              .header h2 {
                  font-size: 18px;
                  color: #008000;
                  margin-bottom: 20px;
              }
              .header ul {
                  margin-top: 20px;
              }
              .header ul li strong {
                  margin-right: 10px;
              }
              .header .headerLeft {
                  width: 67%;
                  display: inline-block;
                  margin-right: -3px;
                  vertical-align: top;
              }
              .header .headerRight {
                  width: 33%;
                  display: inline-block;
                  margin-right: -3px;
                  vertical-align: top;
                  text-align: right; 
              }
  
              ul {
                  list-style: none;
                  padding-inline-start: 0px;
              }
              p{
                  margin: 0;
              }
              h2,h3,h4,h5,h6{
                  margin: 0;
              }
              .invoicebox {
                  border-top: 1px solid #707070;
                  border-bottom: 1px solid #707070;
                  margin: 30px 0;
                  padding: 30px 0; 
              }
              .invoicebox h3 {
                  font-size: 16px;
                  margin-bottom: 15px;
              }
              .invoicebox p {
                  font-weight: 700;
                  margin-bottom: 3px;
              }
              .invoicebox .invoiceLeft {
                  width: 45%;
                  display: inline-block;
                  margin-right: -3px;
                  vertical-align: top;
              }
              .invoicebox .invoiceRight {
                  width: 55%;
                  display: inline-block;
                  margin-right: -3px;
                  vertical-align: top; 
              }
              .invoicebox .invoiceRight li {
                  margin-bottom: 3px; 
              }
              .invoicebox .invoiceRight li span {
                  margin-left: 30px;
              }
              .itemDetails h6 {
                  color: #000000;
                  font-weight: 400;
                  font-style: italic;
                  margin-bottom: 20px;
                  font-size: 14px;
              }
              .itemDetails h6 span {
                  font-weight: 700;
                  margin-left: 20px;
              }
              .itemDetails table {
                  border-spacing: 0;
                  border: none;
                  border-collapse: unset;
                  width: 100%;
              }
              .itemDetails table tr:first-child {
                  background-color: #E6E6E6;
              }
              .itemDetails table tr:nth-child(2) td {
                  padding-top: 20px; 
              }
              .itemDetails table th {
                  font-weight: 700;
                  text-align: left;
                  color: #000000;
                  padding: 7px 6px;
                  font-size: 14px; 
              }
              .itemDetails table th:not(:first-child) {
                  text-align: right; 
              }
              .itemDetails table th:last-child {
                  width: 100px; 
              }
              .itemDetails table td {
                  padding: 4px 6px;
                  color: #000000;
                  font-size: 14px;
              }
              .itemDetails table td:not(:first-child) {
                  text-align: right;
              }
              .itemDetails .totaldetail {
                  margin-top: 50px; 
              }
              .itemDetails .totaldetail tr:first-child {
                  background-color: #e6e6e600;
              }
              .itemDetails .totaldetail tr:last-child td {
                  border-top: 1px solid #808080;
                  border-bottom: 1px solid #808080;
                  padding: 8px 0; 
              }
              .itemDetails .totaldetail tr th {
                  border-bottom: 1px solid #808080;
              }
              .itemDetails .totaldetail tr th:not(:first-child) {
                  font-weight: 400;
              }
  
              .footer {
                  margin-top: 40px;
              }
              .footer p {
                  text-align: center;
              }
              .blank_row  {
                    height: 25px !important; /* overwrites any other rules */
                    background-color: #FFFFFF;
                    border-top: 1px solid #000 !important;
                }
          </style>
      </head>
      <body>
  
  
  
          <section class="invoice-wrp">
              <div class="header">
                  <div class="headerLeft">
                      <h2>Sustainably Run Meetings</h2>
                      <p>The Gatehouse,</p>
                      <p>Kay Street,</p>
                      <p>Summerseat</p>
                      <p>BL9 5PE</p>
                      <ul class="list-unstyled" style="list-style-type:none; padding:0;">
                          <li style="margin:0;"><strong>Telephone : </strong>0208 895 6583</li>
                          <li style="margin:0;"><strong>Email : </strong>meetings@sustainably.run
                          </li>
                      </ul>
                  </div>
                  <div class="headerRight">
                      <img src="https://sustainable-meeting-prod.s3-eu-west-1.amazonaws.com/pdfImages/srlogo.png" style="width: 200px;">
                  </div>
              </div>
  
  
              <div class="invoicebox">
                  <div class="invoiceLeft">
                      <h3>INVOICE</h3>
                      <p>${list[0].firstName}  ${list[0].lastName}</p>
                      <p>${list[0].email}</p>
                      <p>${phone}</p>
  
                  </div>
                  <div class="invoiceRight">
                      <ul class="list-unstyled" style="list-style-type:none; padding:0;">
                          <li style="margin:0;">Date of Invoice <span>${
                            new Date(list[0].invoiceDate)
                              .toISOString()
                              .split("T")[0]
                          }</span></li>
                      </ul>
                  </div>
              </div>
  
              <div class="itemDetails">
                  <div class="itemtable">
                      <table>
                          <tr>
                              <th >Item</th>
                              <th >Trees Planted</th>
                              <th >Price Per Unit</th>
                              <th >Price</th>
                          </tr>
                          ${list
                            .map(
                              (item) => `
                            <tr>
                                <td>Meetings on ${item.MeetingsOn}</td>
                                <td>&nbsp;${item.trees}</td>
                                <td> £${price}</td>
                                <td>&nbsp; £${(item.trees * price).toFixed(
                                  2
                                )}</td>
                            </tr>
                            `
                            )
                            .join("")}
                            <tr class="blank_row">
                                <td colspan="4"></td>
                            </tr>
                            
                          <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td><strong>Sub Total</strong></td>
                              <td>£${amount.toFixed(2)}</td>
                          </tr>
                          <tr style="border-bottom: 1px solid #000 !important;">
                              <td>&nbsp;</td>
                              <td>&nbsp; </td>
                              <td><strong>Tax</strong></td>
                              <td>£${taxAmount}</td>
                          </tr>
                          <tr>
                              <td></td>
                              <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
                              <th>Total</td>
                              <th>£${total}</td>
                          </tr>
                      </table>
                  </div>
                  
                  <div class="totaldetail">
                      <table>
                      </table>
                  </div>
  
              </div>
  
              <div class="footer">
                  <p>Please remit funds to 'Sustainably Run Meetings'</p>
                  <p>BACS Payment: Sort Code 77-04-05 Account number 11418860</p>
                  <p>IBAN: GB84LOYD77040511418860</p>
                  <p>Swift: LOYDGB21T12</p>
                  <p>Cheque: Post to address above</p>
  
              </div>
  
          </section>
  
  
  
      </body>
  </html>`;
  return html;
}

function generatePayNowHtml(list) {
  let totalTrees = list.reduce((a, c) => parseInt(c.trees) + a, 0);
  if (totalTrees == 0) {
    return;
  }
  let amount = totalTrees * price.toFixed(2);
  let taxAmount = (amount * (tax / 100)).toFixed(2);
  let total = (parseFloat(amount) + parseFloat(taxAmount)).toFixed(2);
  let phone = list[0].phone || "";
  let payNowUrl = websiteUrl + "invoice";
  console.log("Pay now URL", payNowUrl);
  let html = `<!DOCTYPE html>
    <html>
        <body style="font-family: Arial, Helvetica, sans-serif;">
        <table style="width: 595px;">
            <tr>
                <td style="width:100%;">
                    <div style="width:50%; float:left;">
                        <h2 style="font-size: 18px; color: #008000; margin-bottom: 15px; margin-top: 0;">Sustainably Run
                            Meetings</h2>
                    </div>
                    <div style="width:50%; float:right; text-align: right;">
                        <div>
                            <img src="https://sustainable-meeting-prod.s3-eu-west-1.amazonaws.com/pdfImages/srlogo.png" style="width: 200px;">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin:0; font-size: 14px;">The Gatehouse,</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin:0; font-size: 14px;">Kay Street,</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin:0; font-size: 14px;">Summerseat</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin:0; font-size: 14px;">BL9 5PE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin-top:15px; margin-bottom:0; font-size: 14px;"><span
                            style="font-weight: bold;">Telephone: </span>0208 895 6583</p>
                </td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #707070;">
                    <p style="margin-bottom: 40px; margin-top:0; font-size: 14px;"><span style="font-weight: bold;">E-mail:
                        </span>meetings@sustainably.run</p>
                </td>
            </tr>
            <tr>
                <td>
                    <h3 style="font-size: 16px; margin-bottom: 15px; margin-top:25px;">INVOICE</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <div
                        style="border-bottom:1px solid #707070; margin-bottom: 15px; width:100%; height:60px; margin-bottom:25px; padding-bottom: 25px;">
                        <ul style="width: 60%; float: left; margin:0; list-style-type: none; padding:0;">
                            <li style="margin:0;">
                                <p style="font-weight: 700; margin-bottom: 3px; margin-top:0; font-size:16px;">${
                                  list[0].firstName
                                }  ${list[0].lastName}
                                </p>
                            </li>
                            <li style="margin:0;">
                                <p style="margin-top:0; margin-bottom: 3px;">	
                                    <a href="#"	
                                        style="font-weight: 700; font-size:16px; color: #000000; text-decoration: none;">	
                                        ${list[0].email}</a>	
                                </p>
                            </li>
                            <li style="margin:0;">
                                <p style="font-weight: 700; margin-bottom: 3px; margin-top:0; font-size:16px;">${phone}
                                </p>
                            </li>
                        </ul>

                        <ul style="width: 40%; float: right; margin:0; list-style-type: none; padding:0;">
                            <li style="margin:0;">
                                <p style="margin:0; font-size:14px;">Date of Invoice <span
                                        style="margin-left:20px;">${
                                          new Date(list[0].invoiceDate)
                                            .toISOString()
                                            .split("T")[0]
                                        }</span></p>
                            </li>
                            <li style="margin-top: 10px;margin-left:0;">
                                <a href="${payNowUrl}"
                                    style="text-align: center; background-color: #3b7c20; border: #3b7c20 solid 1px; border-radius: 50px!important; color: #fff; font-size: 12px!important; font-weight: 500!important; padding: 6px 20px!important; min-width: 100px!important; text-decoration: none; position: relative; top: 10px; left: 0;">Pay
                                    Now</a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width:100%; border-spacing: 0px;">
                        <tr style="background-color: #E6E6E6;">
                            <th
                                style="font-weight: 700; text-align: left; color: #000000; padding: 4px 15px; font-size: 14px;">
                                Item</th>
                            <th
                                style="font-weight: 700; text-align: right; color: #000000; padding: 4px 15px; font-size: 14px;">
                                Trees Planted</th>
                            <th
                                style="font-weight: 700; text-align: right; color: #000000; padding: 4px 15px; font-size: 14px;">
                                Price Per Unit</th>
                            <th
                                style="font-weight: 700; text-align: right; color: #000000; padding: 4px 15px; font-size: 14px;">
                                Price</th>
                        </tr>
                        ${list
                          .map(
                            (item) =>
                              `<tr>
                            <td
                                style="padding: 4px 6px; color: #000000; font-size: 14px; text-align: left; padding: 4px 15px; padding-top:20px;">
                                Meetings on ${item.MeetingsOn}</td>
                            <td
                                style="padding: 4px 6px; color: #000000; font-size: 14px; text-align: right; padding: 4px 15px; padding-top:20px;">
                                ${item.trees}</td>
                            <td
                                style="padding: 4px 6px; color: #000000; font-size: 14px; text-align: right; padding: 4px 15px; padding-top:20px;">
                                £${price}</td>
                            <td
                                style="padding: 4px 6px; color: #000000; font-size: 14px; text-align: right; padding: 4px 15px; padding-top:20px">
                                £${(item.trees * price).toFixed(2)}</td>
                            
                        </tr>
                        `
                          )
                          .join("")}
                                
                    
                        <tr>
                            <td
                                style="font-weight: 700; text-align: left; padding: 4px 15px; padding-top:40px; color: #000000; font-size: 14px; border-bottom:1px solid #000;">
                                Total number of trees</td>
                            <td
                                style="border-bottom:1px solid #707070; text-align: right; padding: 4px 15px; color: #000000; padding-top:40px; font-size: 14px;">
                                ${totalTrees}</td>
                            <td style="border-bottom:1px solid #707070; padding-top:40px;"></td>
                            <td
                                style="border-bottom:1px solid #707070; text-align: right; padding: 4px 15px; color: #000000; padding-top:40px;  font-size: 14px;">
                                £${amount.toFixed(2)}</td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;"></td>
                            <td style="padding-top:20px;"></td>
                            <td
                                style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px; padding-top:20px;">
                                Sub Total</td>
                            <td
                                style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px; padding-top:20px;">
                                £${amount.toFixed(2)}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px;">Tax</td>
                            <td style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px;">
                                £${taxAmount}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px;">Total</td>
                            <td style="text-align: right; color: #000000; font-size: 14px; padding: 4px 15px;">£${total}
                            </td>
                        </tr>
                        <tr>
                            <td
                                style="border-bottom:1px solid #707070; border-top:1px solid #707070; text-align: left; padding: 8px 15px; color: #000000; font-size: 14px;">
                                Total Due</td>
                            <td style="border-bottom:1px solid #707070; border-top:1px solid #707070; padding: 8px 15px;">
                            </td>
                            <td style="border-bottom:1px solid #707070; border-top:1px solid #707070; padding: 8px 15px;">
                            </td>
                            <td
                                style="border-bottom:1px solid #707070; border-top:1px solid #707070; text-align: right; padding: 8px 15px; color: #000000; font-size: 14px;">
                                £${total}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="margin-top:35px; margin-bottom:3px; font-size:14px; color: #000000;">Please remit funds to
                        'Sustainably Run Meetings'</p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="margin:0; font-size:14px; color: #000000;">BACS Payment: Sort Code 77-04-05 Account number
                        11418860</p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="margin:0; font-size:14px; color: #000000;">IBAN: GB84LOYD77040511418860</p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="margin:0; font-size:14px; color: #000000;">Swift: LOYDGB21T12</p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="margin-bottom:20px; margin-top:0; font-size:14px; color: #000000;">Cheque: Post to address
                        above</p>
                </td>
            </tr>
        </table>
    </body>
</html>`;
  return html;
}
function generateZeroTreeHtml(list) {
  let totalTrees = 1;
  let amount = (totalTrees * price).toFixed(2);
  let taxAmount = (amount * (tax / 100)).toFixed(2);
  let total = parseFloat(amount) + parseFloat(taxAmount);
  console.log("\n \n \nTotal \n ", total, typeof amount, amount);
  let html = `<!DOCTYPE html>
  <html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            body {
                font-family: Arial, Helvetica, sans-serif; }

            .invoice-wrp {
                width: 595px;
            }

            .header h2 {
                font-size: 18px;
                color: #008000;
                margin-bottom: 20px;
            }
            .header ul {
                margin-top: 20px;
            }
            .header ul li strong {
                margin-right: 10px;
            }
            .header .headerLeft {
                width: 67%;
                display: inline-block;
                margin-right: -3px;
                vertical-align: top;
            }
            .header .headerRight {
                width: 33%;
                display: inline-block;
                margin-right: -3px;
                vertical-align: top;
                text-align: right; 
            }

            ul {
                list-style: none;
                padding-inline-start: 0px;
            }
            p{
                margin: 0;
            }
            h2,h3,h4,h5,h6{
                margin: 0;
            }
            .invoicebox {
                border-top: 1px solid #707070;
                border-bottom: 1px solid #707070;
                margin: 30px 0;
                padding: 30px 0; 
            }
            .invoicebox h3 {
                font-size: 16px;
                margin-bottom: 15px;
            }
            .invoicebox p {
                font-weight: 700;
                margin-bottom: 3px;
            }
            .invoicebox .invoiceLeft {
                width: 45%;
                display: inline-block;
                margin-right: -3px;
                vertical-align: top;
            }
            .invoicebox .invoiceRight {
                width: 55%;
                display: inline-block;
                margin-right: -3px;
                vertical-align: top; 
            }
            .invoicebox .invoiceRight li {
                margin-bottom: 3px; 
            }
            .invoicebox .invoiceRight li span {
                margin-left: 30px;
            }

            .itemDetails h6 {
                color: #000000;
                font-weight: 400;
                font-style: italic;
                margin-bottom: 20px;
                font-size: 14px;
            }
            .itemDetails h6 span {
                font-weight: 700;
                margin-left: 20px;
            }
            .itemDetails table {
                border-spacing: 0;
                border: none;
                border-collapse: unset;
                width: 100%;
            }
            .itemDetails table tr:first-child {
                background-color: #E6E6E6;
            }
            .itemDetails table tr:nth-child(2) td {
                padding-top: 20px; 
            }
            .itemDetails table th {
                font-weight: 700;
                text-align: left;
                color: #000000;
                padding: 7px 6px;
                font-size: 14px; 
            }
            .itemDetails table th:not(:first-child) {
                text-align: right; 
            }
            .itemDetails table th:last-child {
                width: 100px; 
            }
            .itemDetails table td {
                padding: 4px 6px;
                color: #000000;
                font-size: 14px;
            }
            .itemDetails table td:not(:first-child) {
                text-align: right;
            }
            .itemDetails .totaldetail {
                margin-top: 50px; 
            }
            .itemDetails .totaldetail tr:first-child {
                background-color: #e6e6e600;
            }
            .itemDetails .totaldetail tr:last-child td {
                border-top: 1px solid #808080;
                border-bottom: 1px solid #808080;
                padding: 8px 0; 
            }
            .itemDetails .totaldetail tr th {
                border-bottom: 1px solid #808080;
            }
            .itemDetails .totaldetail tr th:not(:first-child) {
                font-weight: 400;
            }

            .footer {
                margin-top: 40px;
            }
            .footer p {
                text-align: center;
            }
        </style>
    </head>
    <body>



        <section class="invoice-wrp">
            <div class="header">
                <div class="headerLeft">
                    <h2>Sustainably Run Meetings</h2>
                    <p>The Gatehouse,</p>
                    <p>Kay Street,</p>
                    <p>Summerseat</p>
                    <p>BL9 5PE</p>
                    <ul class="list-unstyled" style="list-style-type:none; padding:0;">
                        <li style="margin:0;"><strong>telephone: </strong>0208 895 6583</li>
                        <li style="margin:0;"><strong>email: </strong>meetings@sustainably.run
                        </li>
                    </ul>
                </div>
                <div class="headerRight">
                    <img src="https://sustainable-meeting-prod.s3-eu-west-1.amazonaws.com/pdfImages/srlogo.png" style="width: 200px;">
                </div>
            </div>


            <div class="invoicebox">
                <div class="invoiceLeft">
                    <h3>INVOICE</h3>
                    <p>${list[0].firstName}  ${list[0].lastName}</p>
                    <p>${list[0].email}</p>
                    <p>${list[0].phone}</p>

                </div>
                <div class="invoiceRight">
                    <ul class="list-unstyled" style="list-style-type:none; padding:0;">
                        <li style="margin:0;">Date of Invoice <span>${list[0].invoiceDate}</span></li>

                    </ul>
                </div>
            </div>

            <div class="itemDetails">
                <div class="itemtable">
                    <table>
                        <tr>
                            <th>Item</th>
                            <th>Trees Planted</th>
                            <th>Price Per Unit</th>
                            <th>Price</th>
                        </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </div>
                <div class="totaldetail">
                    <table>
                        <tr>
                            <th>Total number of trees</th>
                            <th>0</th>
                            <th></th>
                            <th>£0</th>
                        </tr>
                        <tr>
                            <th>Refundable Amount for Zero Trees planted *</th>
                            <th>${totalTrees}</th>
                            <th></th>
                            <th>£${amount}</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Sub Total</td>
                            <td>£${amount}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Tax</td>
                            <td>£${taxAmount}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td>£${total}</td>
                        </tr>
                    </table>
                </div>

            </div>
            <p>*The quantity will be reduced from next billing</p>
            <div class="footer">
                <p>Please remit funds to 'Sustainably Run Meetings'</p>
                <p>BACS Payment: Sort Code 77-04-05 Account number 11418860</p>
                <p>IBAN: GB84LOYD77040511418860</p>
                <p>Swift: LOYDGB21T12</p>
                <p>Cheque: Post to address above</p>

            </div>

        </section>



    </body>
  </html>`;
  return html;
}
