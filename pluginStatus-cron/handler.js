'use strict';
// Author: Anvar Nasar <anvar.nazar@emvigotechnologies.com>
const util = require('util');
const { pool } = require('./config');
const hubspot = require('./HubspotService');
const get_connection = util.promisify(pool.getConnection).bind(pool);

exports.updatePluginStatus = async (event) => {
  const connection = await get_connection();
  try {
    const query = util.promisify(connection.query).bind(connection);
    let emailArray = [];
    // query the email address who have not accessed the plugin for the past 28 days
    // const inactive_plugins_result = await query(`SELECT email, last_access FROM plugin_log 
    //   WHERE DATE_SUB(CURDATE(), INTERVAL 28 DAY) > last_access`);

      // try {
        const inactive_plugins_result = await query(`SELECT email, last_access FROM plugin_log 
          WHERE last_access BETWEEN (CURDATE() - INTERVAL 30 DAY) AND (CURDATE() - INTERVAL 28 DAY)`);
        console.log("new query result****", inactive_plugins_result)
      // } catch (error) {
      //   console.log("Error in new query", error);
      // }
      
    // Prepare batch data to update their plugin status in HubSpot to 'Inactive'
    const hubspotUpdateBatch = inactive_plugins_result.map(plugin => {
      const updateData = [
        {
          property: "app_plugin_status",
          value: "Inactive"
        }
      ];
      emailArray.push(JSON.stringify(plugin.email));
      return {
        email: plugin.email,
        properties: updateData
      }
    });
    await hubspot.contacts.createOrUpdateBatch(hubspotUpdateBatch);

    // Update the status of employees in DB.
    console.log("^^ Email Array ^^^", emailArray);
    const updateStatusResult = await query(`UPDATE users SET status = 'inactive' WHERE email in (${emailArray}) AND id != orginasation_id`);

    console.log("*** Updated Status to Inactive in DB ***", updateStatusResult);

    // Log the email id's of the contacts whose plugin status made 'Inactive'
    console.log(`${inactive_plugins_result.length} plugin's are inactive. emails:`)
    for (let index = 0; index < inactive_plugins_result.length; index++) {
      const plugin_last_access_log_entry = inactive_plugins_result[index];
      console.log(plugin_last_access_log_entry.email);
    }
  } catch (error) {
    console.error('FATAL ERROR: Plugin status update failed');
    console.error(error);
  } finally {
    connection.release()
  }
}

