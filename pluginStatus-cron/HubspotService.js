const Hubspot = require('hubspot');
const { env } = require('./config');

module.exports = new Hubspot({
  apiKey: env.HUBSPOT_API_KEY
});